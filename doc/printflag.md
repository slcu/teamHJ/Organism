# Description of the outputs specified by the printFlag option provided in the solver file

## Introduction

This page describes the output specified by the printFlag parameter in the solver file. The prinFlag is an index specifying the output for a simulation and is provided in addition to the number of time points to print the variables.

## Special printFlag indices

There are 5 'standard' printFlag  values implemented. These are:

<p><b>(1) or (10)</b> Standard output used for example by <tt>newman</tt> (see tools/plot/) for plotting cells in spatial context</p>
<p><b>(2)</b> Output that can be used for plotting in standard plotting tools (e.g. gnuplot) where data is stored as a matrix for each time point. </p>
<p><b>(3)</b> Printing in the format used for calculating the 'fitness' of a simulation, can e.g. be used to compare the output of the next simulation with this one.</p>
<p><b>(4)</b> Each time point is written in the init file format (compare with the -init_output flag given at runtime printing the final time in init format). </p>
<p><b>(5)</b> Printing prinFlag (1) style to STDOUT and (2) to the file <tt>organism.data</tt>.</p>


## AdHoc printFlag indices

After the standard printFlag indices described above, a number of more adhoc printing options are provided (starting from printFlag 101). In this section some useful ones are mentioned: 

<p><b>(26)</b> Standard vtu output assuming single wall compartment for wall variables and in addition printing cell variable values for statistical analysis (or gnuplot plotting) in the file tissue.gdata .</p>

For knowing some of this, the source code BaseSolver::Print() might need to be read.


## Implementing new printFlag options

To implement a new way of printing specific data during a simulation, this can be done by editing the BaseSolver::print(). Add your new prinFlag index in the list and potentially describe it in the list above.



