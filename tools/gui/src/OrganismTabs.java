import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.*;

/*
 * Creating and using TabComponents for organism gui
 */ 
public class OrganismTabs extends JTabbedPane 
    implements ActionListener {    
    
    // Simulator parameters
    private JLabel t0Label = new JLabel("Start time:");
    private JTextField t0Field = new JTextField("0",10);
    private JLabel t1Label = new JLabel("End time:");
    private JTextField t1Field = new JTextField("500",10);
    private JLabel maxLabel = new JLabel("Max time step:");
    private JTextField maxField = new JTextField("0.5",10);
    private JLabel printLabel = new JLabel("Number of print times:");
    private JTextField printField = new JTextField("100",10);
    private OrganismModelTab oModelTab_;
    
    // Selected init name
    private String selectedInit_;
    
    public OrganismTabs (OrganismModelTab oModelTab) {
	
	oModelTab_=oModelTab;
	addTab("Model",null, oModelTab_.getSplitPane(),
	       "Setting model parameters");
	
	JComponent panel2 = makeSimulatorPanel();
	addTab("Simulator", null, panel2,
	       "Setting simulator parameters");
	
	JComponent panel3 = makeInitPanel();
	addTab("Init", null, panel3,
	       "Sets initial topology and variable values");
	
	//The following line enables to use scrolling tabs.
	setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
	setVisible(true);
    }
    
    public JTextField getT0Field() {
	return t0Field;
    }
    
    public JTextField getT1Field() {
	return t1Field;
    }
    
    public JTextField getMaxField() {
	return maxField;
    }
    
    public JTextField getPrintField() {
	return printField;
    }
    
    public String getSelectedModel() {
	return oModelTab_.getSelectedModel();
    }
    
    public String getSelectedInit() {
	return selectedInit_;
    }
    
    public Vector<String> getModelParameters() {
	return oModelTab_.getParameters();
    }
    
    protected JComponent makeInitPanel() {
	JPanel panel = new JPanel();
	String[] initTitle = {"Vertical SAM  ", 
			      "Vertical SAM (WCK2d)", 
			      "Horizontal SAM", 
			      "Ablated SAM   ", 
			      //"3D SAM", 
			      "2D Lattice    ",
			      "1D Lattice    "}; 
	String[] initFile = {"vSAM", 
			     "vSAM2", 
			     "hSAM", 
			     "hSAMa", 
			     //"3DSAM", 
			     "2DLattice",
			     "1DLattice"};
	JRadioButton[] initButton = {new JRadioButton(initTitle[0]),
				     new JRadioButton(initTitle[1]),
				     new JRadioButton(initTitle[2]),
				     new JRadioButton(initTitle[3]),
				     new JRadioButton(initTitle[4]),
				     new JRadioButton(initTitle[5])};
	//													 new JRadioButton(initTitle[6])};
        initButton[0].setSelected(true);
	selectedInit_ = initFile[0];
	
	int initSize = initButton.length;
	ButtonGroup initGroup = new ButtonGroup();
	for (int i=0; i<initSize; ++i) {
	    initButton[i].setActionCommand(initFile[i]);
	    initButton[i].addActionListener(this);
	    initGroup.add(initButton[i]);
	}
	JPanel radioPanel = new JPanel(new GridLayout(0, 2));
	for (int i=0; i<initSize; ++i) {
	    JPanel tmpPanel = new JPanel();
	    tmpPanel.add(initButton[i]);
	    tmpPanel.add(new JLabel(createImageIcon("init/"+initFile[i]+".jpg")));
	    radioPanel.add(tmpPanel);
	}
	//JPanel iconPanel = new JPanel(new GridLayout(0,1));
	//for (int i=0; i<initSize; ++i) {
	//	iconPanel.add(new JLabel(createImageIcon(initFile[i]+".jpg")));
	//}
	panel.add(radioPanel);
	//panel.add(iconPanel);
	
        panel.setBorder(
			BorderFactory.createCompoundBorder(
							   BorderFactory.createTitledBorder(" Choose topology "),
							   BorderFactory.createEmptyBorder(5,5,5,5)));
	
	return panel;
    }		
    protected JPanel createButtonImage(JRadioButton button,JLabel icon) {
	JPanel panel = new JPanel();
	panel.add(button);
	panel.add(icon);
	return panel;
    }
    
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = OrganismTabs.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }	
    
    
    protected JComponent makeSimulatorPanel() {
	JPanel panel = new JPanel();
	GridBagLayout gridbag = new GridBagLayout();
	GridBagConstraints c = new GridBagConstraints();
	panel.setLayout(gridbag);
	
	JLabel[] labels = {t0Label,t1Label,maxLabel,printLabel};
	JTextField[] textFields = {t0Field,t1Field,maxField,printField};
	
	addLabelTextRows(labels,textFields,gridbag,panel);				
	
	c.gridwidth = GridBagConstraints.REMAINDER; //last
        c.anchor = GridBagConstraints.WEST;
        c.weightx = 1.0;
        //panel.add(actionLabel, c);
        panel.setBorder(
			BorderFactory.createCompoundBorder(
							   BorderFactory.createTitledBorder(" Simulator parameters "),
							   BorderFactory.createEmptyBorder(5,5,5,5)));
	return panel;
    }		
    
    private void addLabelTextRows(JLabel[] labels,
				  JTextField[] textFields,
                                  GridBagLayout gridbag,
                                  Container container) {
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.EAST;
        int numLabels = labels.length;
	
        for (int i = 0; i < numLabels; i++) {
            c.gridwidth = GridBagConstraints.RELATIVE; //next-to-last
            c.fill = GridBagConstraints.NONE;      //reset to default
            c.weightx = 0.0;                       //reset to default
            container.add(labels[i], c);
	    
            c.gridwidth = GridBagConstraints.REMAINDER;     //end row
            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 1.0;
            container.add(textFields[i], c);
        }
    }		
    // When button clicked...
    public void actionPerformed(ActionEvent e) {
	// Get selected init
	selectedInit_ = (String) e.getActionCommand();
    }		
}

