import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.*;
/*
 * Creating and using a SelectorDriver for the Organism GUI
 */ 
public class OrganismDriver extends JPanel
		implements ActionListener {
		
		private OrganismTabs oTabs_;
		private OrganismModelTab oModelTab_;
		private String rkFileName_="out/tmp.rk";
		private String parameterFileName_="out/tmp.parameter";
		private String runScript_="out/runSimulator"; 
		private String dataFile_="out/tmp.data";
		private String simulatorCerrFile_="out/tmp.cerr";
		private String newmanCerrFile_="out/newman.cerr";
		private String modelDirectory_="model/";
		private String initDirectory_="init/";
		
		public OrganismDriver (String[] modelNames,OrganismTabs oTabs,OrganismModelTab oModelTab) {
				
				oTabs_ = oTabs;
				oModelTab_ = oModelTab;
				
				JButton runButton = new JButton("Run");
				runButton.addActionListener(this);
				add(runButton);
				setVisible(true);
		}		

		// When button clicked...
		public void actionPerformed(ActionEvent e) {
				
				// Get simulator parameters and print .rk file
				String t0 = oTabs_.getT0Field().getText();
				String t1 = oTabs_.getT1Field().getText();
				String max = oTabs_.getMaxField().getText();
				String print = oTabs_.getPrintField().getText();
				String[] simulatorParameters = {t0,t1,print,max};
				printRkFile(rkFileName_,simulatorParameters);
				
				// Get model parameters and print tmp.parameter file
				//Vector<String> modelParameters = oTabs_.getModelParameters();
				Vector<String> modelParameters = oModelTab_.getParameters();
				printParameterFile(parameterFileName_,modelParameters);

				// Get model name and set model and init file names 
				String modelName = modelDirectory_;
				modelName = modelName.concat(oTabs_.getSelectedModel()+".model");
				String initName = initDirectory_;
				initName = initName.concat(oTabs_.getSelectedInit()+".init");

				// Print commands to runScript
				String command1 = "./simulator -init_output init/latest.init -parameter_input ";
				command1 = command1.concat(parameterFileName_+" "+modelName+" "+initName+" "+rkFileName_+" > "+dataFile_+" 2> "+simulatorCerrFile_);
				FileOutputStream out; // declare a file output object 
				PrintStream p; // declare a print stream object 
				String command2 = "./newman ";
				//if (modelName.equals("model/pcw.model") || 
				//	modelName.equals("model/WCK2d.model") ) {
				//command2 = command2.concat("-max 2 ");
				//} // sets newman max color display to 2 for models pcw and WCK2d  
				command2 = command2.concat(dataFile_+" 2> "+newmanCerrFile_);
				try { 
						out = new FileOutputStream(runScript_); 
						p = new PrintStream( out ); 
						//p.println("#!/bin/bash");
						//p.println (command1); 
						//p.println (command2); 
						p.print("#!/bin/bash\n"+command1+"\n"+command2+"\n");
						p.close(); 
				} catch (Exception ex) { System.err.println ("Error writing to file " + runScript_); } 
				// Convert to unix style
// 				try {				
// 						String s1 = "dos2unix out/runSimulator";
// 						System.out.println("Running " + s1);
// 						Runtime.getRuntime().exec(s1);
// 				}
// 				catch (IOException e1) {
// 						System.out.println(e1.toString());
// 				}				
// 				try {				
// 						String s1 = "chmod a+x out/runSimulator";
// 						System.out.println("Running " + s1);
// 						Runtime.getRuntime().exec(s1);
// 				}
// 				catch (IOException e1) {
// 						System.out.println(e1.toString());
// 				}				
				// Run the runScript
				try {				
						//String s1 = "./simulator -init_output latest.init tmp.model tmp.init tmp.rk > tmp.data";
						String s1 = runScript_;
						System.out.println("Running " + s1);
						Runtime.getRuntime().exec(s1);
				}
				catch (IOException e1) {
						System.out.println(e1.toString());
				}
		}
		
		protected void printRkFile(String FileName, String[] param) {
				
				FileOutputStream out; // declare a file output object 
				PrintStream p; // declare a print stream object 
				try { 
						out = new FileOutputStream(FileName); 
						p = new PrintStream( out ); 
						p.println ("RK5Adaptive"); 
						p.println (param[0] + " " + param[1]); 
						p.println ("1 " + param[2]); 
						p.println (param[3] + " 1e-03"); 
						p.close(); 
				} catch (Exception e) { System.err.println ("Error writing to file " + FileName); } 
		} 
		protected void printParameterFile(String FileName, Vector<String> param) {
				
				FileOutputStream out; // declare a file output object 
				PrintStream p; // declare a print stream object 
				int numParameter = param.size();
				String parameterString = param.elementAt(0);
				for (int i=1; i<numParameter; ++i) {
						parameterString = parameterString.concat(" "+param.elementAt(i));
				}

				try { 
						out = new FileOutputStream(FileName); 
						p = new PrintStream( out ); 
						p.println (parameterString); 
						p.close(); 
				} catch (Exception e) { System.err.println ("Error writing to file " + FileName); } 
		} 
}
