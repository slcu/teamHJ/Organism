import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/*
 * Creating and using a SelectorDriver for the Organism GUI
 */ 
public class OrganismSelectorDriver extends JPanel {    
		
		
		public OrganismSelectorDriver (String[] modelNames,OrganismModelTab oModelTab, OrganismTabs oTabs) {
				
				add(new OrganismSelector(modelNames,oModelTab));
				add(new OrganismDriver(modelNames,oTabs,oModelTab));

				setVisible(true);
		}		
}
