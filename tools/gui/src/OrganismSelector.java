import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

import java.util.*;

/*
 * Creating and using a Selector for the Organism GUI
 */ 
public class OrganismSelector extends JPanel {
		
		private JComboBox list;
		
		public OrganismSelector (String[] modelNames,OrganismModelTab oModelTab) {
				
				list = new JComboBox(modelNames);
				//list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				list.setSelectedIndex(0);
				//list.addListSelectionListener(this);
				list.addActionListener(oModelTab);
				JScrollPane listScrollPane = new JScrollPane(list);
				Dimension minimumSize = new Dimension(100, 50);
				listScrollPane.setMinimumSize(minimumSize);
				add(listScrollPane);
				
				setVisible(true);
		}		
		//Listens to the list
		public void valueChanged(ActionEvent e) {
				JComboBox list = (JComboBox)e.getSource();
				list.getSelectedIndex();
		}
}
