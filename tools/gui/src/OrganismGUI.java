///
/// OrganismGUI.java
///
/// Main window for the organism graphical user interface
///

import javax.swing.JTabbedPane;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.BoxLayout;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.io.*;


public class OrganismGUI extends JPanel {
    public OrganismGUI () {
	//super(new GridLayout(2, 1));
	this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
	
	// Get model names from scanning directory for .model files
	File directory = new File("model");
	String[] fileNames = directory.list();
	int numFiles = fileNames.length;
	int modelCount=0;
	for (int i=0; i<numFiles; ++i) {
	    if (fileNames[i].endsWith(".model") && !fileNames[i].contains("tmp")) {
		++modelCount;
	    }
	}
	String[] modelNames = new String[modelCount];
	modelCount=0;
	for (int i=0; i<numFiles; ++i) {
	    if (fileNames[i].endsWith(".model") && !fileNames[i].contains("tmp")) {
		modelNames[modelCount] = new String(fileNames[i].replace(".model",""));
		++modelCount;
	    }
	}
	if (modelCount==0) {
	    System.err.println("No models found!");
	    System.exit(-1);
	}
	
	
	OrganismModelTab oModelTab = new OrganismModelTab(modelNames);
	OrganismTabs oTabs = new OrganismTabs(oModelTab);
	add(oTabs);
	
	OrganismSelectorDriver oSelectorDriver = new OrganismSelectorDriver(modelNames,oModelTab,oTabs);
	add(oSelectorDriver);
	
    }
    
    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from
     * the event dispatch thread.
     */
    private static void createAndShowGUI() {
	//Create and set up the window.
	JFrame frame = new JFrame("Organism GUI");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	//Add content to the window.
	frame.add(new OrganismGUI(), BorderLayout.CENTER);
	
	//Display the window.
	frame.pack();
	frame.setVisible(true);
    }
    
    public static void main(String[] args) {
	//Schedule a job for the event dispatch thread:
	//creating and showing this application's GUI.
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
		    //Turn off metal's use of bold fonts
		    UIManager.put("swing.boldMetal", Boolean.FALSE);
		    createAndShowGUI();
		}
	    });
    }
}
