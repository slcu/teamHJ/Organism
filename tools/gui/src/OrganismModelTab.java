// The model tab

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;

import java.net.URL;
import java.io.*;

public class OrganismModelTab extends JPanel
    implements ActionListener {
    private JPanel parameter = new JPanel();
    private JPanel picture = new JPanel();
    private JLabel picture1;
    private JTextPane picture2;
    private JSplitPane splitPane;
    private String selectedModel_;
    private String modelDirectory_="model/";
    private String[] modelNames_;
    Vector<String> parameters_ = new Vector<String>();
    Vector<JTextField> textFields_ = new Vector<JTextField>();
    
    public OrganismModelTab(String[] modelNamesList) {
	
	modelNames_ = modelNamesList;
	if (modelNames_.length==0) {
	    System.err.println("No models found!");
	    System.exit(-1);
	}
	selectedModel_ = modelNames_[0];
	
	try {
	    parameter = getParameterPanel(modelDirectory_+selectedModel_);
	}
	catch(Exception e) {
	    System.err.println ("Error getting parameters for " + modelDirectory_+selectedModel_);
	}
	
	JScrollPane listScrollPane = new JScrollPane(parameter);
	picture1 = new JLabel();
	picture1.setFont(picture1.getFont().deriveFont(Font.ITALIC));
	picture1.setHorizontalAlignment(JLabel.LEFT);
	picture1.setVerticalAlignment(JLabel.TOP);
        picture1.setBorder(
			   BorderFactory.createCompoundBorder(
							      BorderFactory.createTitledBorder(" " + selectedModel_ + " equations "),
							      BorderFactory.createEmptyBorder(5,5,5,5)));
	
	picture.setLayout(new BoxLayout(picture,BoxLayout.Y_AXIS));
	picture.add(picture1);
	picture2 = createTextPane(modelDirectory_+selectedModel_);
        picture2.setBorder(
			   BorderFactory.createCompoundBorder(
							      BorderFactory.createTitledBorder(" Organsim model file for " + selectedModel_ + " "),
							      BorderFactory.createEmptyBorder(5,5,5,5)));
	
	picture.add(picture2);
	JScrollPane pictureScrollPane = new JScrollPane(picture);
	
	//Create a split pane with the two scroll panes in it.
	splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				   listScrollPane, pictureScrollPane);
	splitPane.setOneTouchExpandable(true);
	splitPane.setDividerLocation(250);
	
	//Provide minimum sizes for the two components in the split pane.
	Dimension minimumSize = new Dimension(200, 100);
	listScrollPane.setMinimumSize(minimumSize);
	minimumSize = new Dimension(400, 100);
	pictureScrollPane.setMinimumSize(minimumSize);
	
	//Provide a preferred size for the split pane.
	splitPane.setPreferredSize(new Dimension(700, 300));
	updateLabel(modelDirectory_+selectedModel_);
	setVisible(true);
    }
    
    public String getSelectedModel() {
	return selectedModel_;
    }
    
    public Vector<String> getParameters() {
	int numParameter = parameters_.size();
	for (int i=0; i<numParameter; ++i) {
	    parameters_.setElementAt(textFields_.elementAt(i).getText(),i); 
	}
	return parameters_;
    }
    
    private JPanel getParameterPanel(String name) throws IOException {
	JPanel panel = new JPanel();
	GridBagLayout gridbag = new GridBagLayout();
	GridBagConstraints c = new GridBagConstraints();
	panel.setLayout(gridbag);
	
	// Read parameters from name.parameter
	parameters_.clear();
	String parameterFile = name;
	parameterFile = parameterFile.concat(".parameter");
	Scanner s=null;				
	try {
	    s = new Scanner(new BufferedReader(new FileReader(parameterFile)));
	    while (s.hasNext()) {
		//System.out.println(s.next());
		parameters_.add(s.next());
	    }
	} finally {
	    if (s!=null) {
		s.close();				
	    }
	}
	// Create parameter input list
	int numParameter = parameters_.size();
	textFields_ = new Vector<JTextField>();
	JLabel[] labels = new JLabel[numParameter];
	for (int i=0; i<numParameter; ++i) {
	    labels[i] = new JLabel("p_"+i);
	    textFields_.add(new JTextField(parameters_.elementAt(i),10));
	    //Dimension maxSize = new Dimension(1, 10);
	    //textFields[i].setMaximumSize(maxSize);
	}
	
	addLabelTextRows(labels,textFields_,gridbag,panel);
	c.gridwidth = GridBagConstraints.REMAINDER; //last
        c.anchor = GridBagConstraints.WEST;
        c.weightx = 1.0;
        //panel.add(actionLabel, c);
        panel.setBorder(
			BorderFactory.createCompoundBorder(
							   BorderFactory.createTitledBorder(" " + name + " parameters "),
							   BorderFactory.createEmptyBorder(5,5,5,5)));
	
	return panel;
    }
    
    private void addLabelTextRows(JLabel[] labels,
				  Vector<JTextField> textFields,
                                  GridBagLayout gridbag,
                                  Container container) {
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.EAST;
        int numLabels = labels.length;
	
        for (int i = 0; i < numLabels; i++) {
            c.gridwidth = GridBagConstraints.RELATIVE; //next-to-last
            c.fill = GridBagConstraints.NONE;      //reset to default
            c.weightx = 0.0;                       //reset to default
            container.add(labels[i], c);
	    
            c.gridwidth = GridBagConstraints.REMAINDER;     //end row
            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 1.0;
            container.add(textFields.elementAt(i), c);
        }
    }
    
    private JTextPane createTextPane(String name) {
        JTextPane textPane = new JTextPane();
        textPane.setEditable(false);
        java.net.URL modelURL = OrganismModelTab.class.getResource(
								   name+".model");
        if (modelURL != null) {
            try {
                textPane.setPage(modelURL);
            } catch (IOException e) {
                System.err.println("Attempted to read a bad URL: " + modelURL);
            }
        } else {
            System.err.println("Could not find file: " + name + 
			       ".model for displaying model file.");
        }
	
        return textPane;
    }
    
    //Listens to the JComboBox
    public void actionPerformed(ActionEvent e) {
	JComboBox list = (JComboBox)e.getSource();
	selectedModel_ = modelNames_[list.getSelectedIndex()];
	updateLabel(modelDirectory_+selectedModel_);
	JPanel tmpPanel = new JPanel();
	tmpPanel.setLayout(new BoxLayout(tmpPanel,BoxLayout.Y_AXIS));
	
	picture1.setBorder(
			   BorderFactory.createCompoundBorder(
							      BorderFactory.createTitledBorder(" " + selectedModel_ + " equations "),
							      BorderFactory.createEmptyBorder(5,5,5,5)));
	picture2.setBorder(
			   BorderFactory.createCompoundBorder(
							      BorderFactory.createTitledBorder(" Organsim model file for " + selectedModel_ + " "),
							      BorderFactory.createEmptyBorder(5,5,5,5)));
	
	tmpPanel.add(picture1);
	tmpPanel.add(picture2);
	Dimension minimumSize = new Dimension(400, 100);
	tmpPanel.setMinimumSize(minimumSize);
	
	JScrollPane tmpScrollPane = new JScrollPane(tmpPanel);
	splitPane.setRightComponent(tmpScrollPane);
	try {
	    tmpPanel = getParameterPanel(modelDirectory_+selectedModel_);
	}
	catch(Exception ex) {
	    System.err.println ("Error getting parameters for " + modelDirectory_+selectedModel_);
	}
	minimumSize = new Dimension(200, 100);
	tmpPanel.setMinimumSize(minimumSize);
	
	tmpScrollPane = new JScrollPane(tmpPanel);
	splitPane.setLeftComponent(tmpScrollPane);
    }
    
    //Renders the selected image
    protected void updateLabel (String name) {
	ImageIcon icon = createImageIcon(name + ".jpg");
	picture1.setIcon(icon);
	if  (icon != null) {
	    picture1.setText(null);
	} else {
	    picture1.setText("Image " + name + " not found");
	}
	picture2 = createTextPane(name);			
    }
    
    //Used by SplitPaneDemo2
    //public JList getImageList() {
    //  return list;
    //}
    
    public JSplitPane getSplitPane() {
        return splitPane;
    }
    
    
    /** Returns an ImageIcon, or null if the path was invalid. */
    protected static ImageIcon createImageIcon(String path) {
	java.net.URL imgURL = OrganismModelTab.class.getResource(path);
	if (imgURL != null) {
	    return new ImageIcon(imgURL);
	} else {
	    System.err.println("Could not find model image file: " + path);
	    return null;
	}
    }
}
