### Introduction

This folder holds files for running organism via a java-based graphical user interface.

### Preparation

To run the gui you will need to compile the <tt>OrganismGUI.java</tt> and make sure the compiled files ends up in the bin folder.

The GUI uses the organism simulator and newman binaries, so these needs to be copied to (or linked from) the bin folder.

Then you can start the GUI by <tt>java OrganismGUI</tt>.

The GUI reads the model files from the bin/model folder and they become selectable, similar with init files from the bin/init folder.

To add models or init files, add the .model file with a .parameter file and .jpg to indicate model reactions and parameter order. (all should be named the same). The same for init; add .init and .jpg file into the folder for the GUI to include them in the possible selection.

