#ifndef ORGANISMDTK_OUTPUTMETHOD_H
#define ORGANISMDTK_OUTPUTMETHOD_H

#include <ostream>
#include "data.h"

class OutputMethod
{
public:
	virtual ~OutputMethod();
	virtual void writeOutput(std::ostream &out, Data *data);
	static OutputMethod *getOutputMethod(std::string name);
};

class OrganismOutputMethod : public OutputMethod
{
public:
	void writeOutput(std::ostream &out, Data *data);
};

class GnuplotOutputMethod : public OutputMethod
{
public:
	void writeOutput(std::ostream &out, Data *data);
};

class RawOutputMethod : public OutputMethod
{
public:
	void writeOutput(std::ostream &out, Data *data);
};

class InitOutputMethod : public OutputMethod
{
public:
	void writeOutput(std::ostream &out, Data *data);
};

#endif /* ORGANISMDTK_OUTPUTMETHOD_H */
