#include <cmath>
#include "shortestDistance.h"
#include "../../../src/common/myConfig.h"
#include <cstdlib>

using namespace action;

Data *ShortestDistance::process(Data *data, OutputMethod *outputMethod)
{
	size_t dimension; 

	// Check number of arguments given at command line.
	switch (myConfig::argc() - 2) {
	case 2:
		dimension = 1;
		break;
	case 4:
		dimension = 2;
		break;
	case 6:
		dimension = 3;
		break;
	default:
		std::cerr << "Error: Action averageShortestDistance requires either two, four or six arguments." << std::endl;
		exit(EXIT_FAILURE);
	}

	// Read command line arguments.
	std::vector<size_t> column(dimension);
	std::vector<double> periodicity(dimension); 
		
	for (size_t i = 0; i < dimension; ++i) {
		column[i] = (size_t) atoi(myConfig::argv(2 * i + 2).c_str());
		std::string tmp = myConfig::argv(2 * i + 2 + 1);
 		if (tmp == "no")
			periodicity[i] = 0;
		else
			periodicity[i] = atof(tmp.c_str());
	}

	Data *output = new Data;

	for (size_t n = 0; n < data->frames(); ++n) {
		Frame frame;
		std::vector<double> distances = getShortestDistance(column, periodicity, data->getFrame(n).getMatrix());

		if (distances.size() != data->getFrame(n).getMatrix().size()) {
			std::cerr << "Internal Error: ShortestDistance::process(...) - distances.size() != frame.getMatrix().size()\n";
			exit(EXIT_FAILURE);
		}

		Matrix matrix;
		for (size_t i = 0; i < distances.size(); ++i) {
			Vector vector;
// 			for (size_t j = 0; j < column.size(); ++j) 
// 				vector.push_back(data->getFrame(n).getMatrix()[i][column[j]]);
			vector.push_back(distances[i]);
			matrix.push_back(vector);
		}
		frame.setMatrix(matrix);
		frame.setTime(data->getFrame(n).getTime());
		output->addFrame(frame);
	}	
	return output;
}

void ShortestDistance::printHelp(void)
{
	std::cout << "Action: averageShortestDistance" << std::endl
			<< "Arguments: "
			<< " <x index> <x periodicity> "
			<< "[<y index> <y periodicity> "
			<< "<z index> <z periodicity>]" << std::endl
			<< std::endl
			<< "This action consider each row to be an object located in an Euclidian "
			<< "space given by arguments. It loops over each object and seeks its "
			<< "closest neighbor. The distance between each object and its closest "
			<< "neighbor is averaged over all objects. The output is a 1x2 data matrix "
			<< "with the average value and deviation. The arguments sets the indices "
			<< "to use as x, y and z coordinates. The number of dimensions can be "
			<< "arbitrary between one and three. Each coordinate index must be "
			<< "supplied with an argument for the periodicity. If no periodicity is "
 			<< "desired the value should be set to 'no'." << std::endl;	
}

Range ShortestDistance::arguments(void)
{
	Range range(2, 6);
	return range;
}

std::vector<double> ShortestDistance::getShortestDistance(const std::vector<size_t> &column, const std::vector<double> &periodicity, 
													const Matrix &data)
{
	std::vector<double> distance;
	
	for (size_t i = 0; i < data.size(); ++i) {
		double shortest = std::numeric_limits<double>::max();
		for (size_t j = 0; j < data.size(); ++j) {
			if (i == j)
				continue;
			double distance = calculateDistance(i, j, column, periodicity, data);
			if (distance < shortest)
				shortest = distance;
		}
		distance.push_back(shortest);
	}
	return distance;
}

double ShortestDistance::calculateDistance(size_t i, size_t j, const std::vector<size_t> &column, const std::vector<double> &periodicity, 
										const Matrix &data)
{
	double shortest = std::numeric_limits<double>::max();

	size_t dimensions = periodicity.size();

	std::vector<int> k(dimensions + 1, -1);			
	do {
		double distance = 0.0;
		for (size_t dim = 0; dim < dimensions; ++dim) {
			size_t index = column[dim];
			double tmp;
			if (periodicity[dim])
				tmp = (data[i][index] - (periodicity[dim] * k[dim] + data[j][index]));
			else
				tmp = (data[i][index] - data[j][index]);
			distance += (tmp * tmp);
		}
		distance = std::sqrt(distance);
		
		if (distance < shortest)
			shortest = distance;
		
		for (size_t l = 0; l < dimensions + 1; ++l) {
			++k[l];
			if (k[l] == 2)
				k[l] = -1;
			else
				break;
		}
	} while (k[periodicity.size()] == -1);
	
	return shortest;
}

std::pair<double, double> ShortestDistance::calculateStatistics(const std::vector<double> &distances)
{
	double average;
	double deviation;
	double tmpSum;
	
	// Calculate average distance.
	tmpSum = 0.0;
	for (size_t i = 0; i < distances.size(); ++i)
		tmpSum += distances[i];
	average = (tmpSum / distances.size());
	
	// Calculate deviation.
	tmpSum = 0.0;
	for (size_t i = 0; i < distances.size(); ++i)
		tmpSum += pow(distances[i] - average, 2);
	deviation = sqrt(tmpSum / distances.size());
	
	std::pair<double, double> statistics(average, deviation);
	return statistics;
}
	
