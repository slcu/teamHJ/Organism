#include <fstream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include "split.h"
#include "../file.h"
#include "../../../src/common/myConfig.h"

using namespace action;

Data *Split::process(Data *data, OutputMethod *outputMethod)
{
	std::string fileNamePrefix;
	if (myConfig::getBooleanValue("outputFile")) 
		fileNamePrefix = myConfig::getValue("outputFile", 0);
	else
		fileNamePrefix = "STDOUT";
	int w = std::ceil(std::log10(data->frames()));
	for (size_t n = 0; n < data->frames(); ++n) {
		
		Data *tmpData = new Data;
		tmpData->addFrame(data->getFrame(n));
		std::stringstream fileName;
		fileName << fileNamePrefix << ".";
		fileName << std::setfill('0') << std::setw(w) << n;
		file::writeFile(fileName.str(), tmpData, outputMethod);
		delete tmpData;
	}

	return NULL;
}

void Split::printHelp(void)
{
	std::cout << "Action: split" << std::endl
			<< "Arguments: none" << std::endl
			<<  std::endl
			<< "This action splits the input into one or more files. It uses option "
			<< "-outputFile to set the file name prefix of the output files." << std::endl
			<< std::endl;
}

Range Split::arguments(void)
{
	Range range(0,0);
	return range;
}
