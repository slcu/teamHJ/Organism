#include <cmath>
#include "average.h"

using namespace action;

Data *Average::process(Data *data, OutputMethod *outputMethod)
{
	Data *output = new Data;

	for (size_t n = 0; n < data->frames(); ++n) {
		Frame frame;
		Matrix matrix;

		Vector vector;
		for (size_t j = 0; j < data->getFrame(n).getMatrix()[0].size(); ++j) {
			std::vector<double> values;
			for (size_t i = 0; i < data->getFrame(n).getMatrix().size(); ++i)
				values.push_back(data->getFrame(n).getMatrix()[i][j]);
			std::pair<double, double> statistics = calculateStatistics(values);
			vector.push_back(statistics.first);
			vector.push_back(statistics.second);
		}
		matrix.push_back(vector);
		frame.setMatrix(matrix);
		frame.setTime(data->getFrame(n).getTime());
		output->addFrame(frame);			
	}	
	return output;
}

void Average::printHelp(void)
{
	std::cout << "Action: average" << std::endl
		  << "Arguments: none"
		  << std::endl
		  << "This action averages columns in each frane." << std::endl;
}

Range Average::arguments(void)
{
	Range range(0, 0);
	return range;
}

std::pair<double, double> Average::calculateStatistics(const std::vector<double> &distances)
{
	double average;
	double deviation;
	double tmpSum;
	
	// Calculate average distance.
	tmpSum = 0.0;
	for (size_t i = 0; i < distances.size(); ++i)
		tmpSum += distances[i];
	average = (tmpSum / distances.size());
	
	// Calculate deviation.
	tmpSum = 0.0;
	for (size_t i = 0; i < distances.size(); ++i)
		tmpSum += pow(distances[i] - average, 2);
	deviation = sqrt(tmpSum / distances.size());
	
	std::pair<double, double> statistics(average, deviation);
	return statistics;
}
	
