#ifndef ORGANISMDTK_PROJECTINTOBOUNDARYBOX_H
#define ORGANISMDTK_PROJECTINTOBOUNDARYBOX_H

#include "../action.h"

namespace action
{
	class ProjectIntoBoundaryBox : public Action
	{
	public:
		Data *process(Data *data, OutputMethod *outputMethod = NULL);
		void printHelp(void);
		Range arguments(void);
	};
}

#endif /* ORGANISMDTK_PROJECTINTOBOUNDARYBOX_H */
