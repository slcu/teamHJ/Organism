#include "projectIntoBoundaryBox.h"
#include "../../../src/common/myConfig.h"
#include <cstdlib>

using namespace action;

Data *ProjectIntoBoundaryBox::process(Data *data, OutputMethod *outputMethod)
{
	size_t dimension; 

	// Check number of arguments given at command line.
	switch (myConfig::argc() - 2) {
	case 2:
		dimension = 1;
		break;
	case 4:
		dimension = 2;
		break;
	case 6:
		dimension = 3;
		break;
	default:
		std::cerr << "Error: Action projectIntoBoundaryBox requires either two, four or six arguments." << std::endl;
		exit(EXIT_FAILURE);
	}

	// Read command line arguments.
	std::vector<size_t> column(dimension);
	std::vector<double> periodicity(dimension); 
		
	for (size_t i = 0; i < dimension; ++i) {
		column[i] = (size_t) atoi(myConfig::argv(2 * i + 2).c_str());
		std::string tmp = myConfig::argv(2 * i + 2 + 1);
 		if (tmp == "no")
			periodicity[i] = 0;
		else
			periodicity[i] = atol(tmp.c_str());
	}

	Data *output = new Data;

	for (size_t n = 0; n < data->frames(); ++n) {
		Matrix matrix = data->getFrame(n).getMatrix();
		Frame frame;		
		Matrix tmpMatrix;
		for (size_t i = 0; i < matrix.size(); ++i) {
			Vector vector;
			for (size_t j = 0; j < matrix[i].size(); ++j) {
				double element = matrix[i][j];
				for (size_t k = 0; k < column.size(); ++k) {
					if (j == column[k]) {
						while (element < 0.0 || element >= periodicity[k]) {
							if (element < 0.0)
								element += periodicity[k];
							if (element >= periodicity[k])
								element -= periodicity[k];
						} 
						break;
					}
				}
				vector.push_back(element);
			}
			tmpMatrix.push_back(vector);
		}
		frame.setTime(data->getFrame(n).getTime());
		frame.setMatrix(tmpMatrix);
		output->addFrame(frame);		
	}
	return output;
}

void ProjectIntoBoundaryBox::printHelp(void)
{
	std::cout << "Action: projectIntoBoundaryBox" << std::endl
			<< "Arguments: "
			<< " <x index> <x periodicity> "
			<< "[<y index> <y periodicity> "
			<< "<z index> <z periodicity>]" << std::endl
			<<  std::endl
			<< "This action assumes a box given by arguments with a periodic boundary "
			<< "conditiod. Each row vector will be considered as an object. If an "
			<< "object lies outside the box it will be moved inside. Example: If the "
			<< "box is given by the arguments (1, 5), the object <3 7 8> will be "
			<< "changed to <3 2 8>." << std::endl
			<< std::endl;
}

Range ProjectIntoBoundaryBox::arguments(void)
{
	Range range(2, 6);
	return range;
}
