#ifndef ORGANISMDTK_EXTRACTPEAKSFROMNEIGHBORHOOD_H
#define ORGANISMDTK_EXTRACTPEAKSFROMNEIGHBORHOOD_H

#include "../action.h"

namespace action
{
	class ExtractPeaksFromNeighborhood : public Action
	{
	public:
		Data *process(Data *data, OutputMethod *outputMethod = NULL);
		void printHelp(void);
	     action::Range arguments(void);
	};
}

#endif /* ORGANISMDTK_EXTRACTPEAKSFROMNEIGHBORHOOD_H */
