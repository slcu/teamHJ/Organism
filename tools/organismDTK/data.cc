#include "data.h"

void Frame::setMatrix(Matrix matrix)
{
	matrix_ = matrix;
}

Matrix &Frame::getMatrix(void)
{
	return matrix_;
}

size_t Frame::size(void)
{
	return matrix_.size();
}

Vector &Frame::operator[](size_t index)
{
	return matrix_[index];
}

void Frame::setTime(double time)
{
	time_ = time;
}

double Frame::getTime(void)
{
	return time_;
}

void Data::addFrame(Frame frame)
{
	data_.push_back(frame);
}

size_t Data::frames(void)
{
	return data_.size();
}

Frame &Data::getFrame(size_t index)
{
	return data_[index];
}

/*
Frame &Data::operator[](size_t index)
{
	return data_[index];
}
*/
