#ifndef ORGANISMDTK_FILE_H
#define ORGANISMDTK_FILE_H

#include <string>
#include "data.h"
#include "inputMethod.h"
#include "outputMethod.h"

namespace file
{
	void readFile(std::string fileName, Data *data, InputMethod *inputMethod);
	void writeFile(std::string fileName, Data *data, OutputMethod *outputMethod);
}

#endif /* ORGANISMDTK_FILE_H */
