#! /usr/bin/python
# -*- coding: utf-8 -*-

import sys

num_out = sys.argv[1]

chan = open(num_out,"r")
lines = chan.readlines()
chan.close()

chan = open(num_out,"w")
for l in lines:
  s = l[:-1].split(" ")
  if len(s) < 5:
    chan.write(l)
  else :
    if (float(s[0]) < 0) and (float(s[1]) < 0):
      s[3] = "0.0"
    for e in s:
      chan.write(e+" ")
    chan.write("\n")
chan.close()
