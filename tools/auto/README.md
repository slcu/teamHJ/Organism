# Introduction

auto is a (c++) script for running several (organism) runs at the same time, where parameters etc. can be changed (within files and/or flags for running  or file names for in/output). It has versatile functions and the benefit that it checks which of the jobs has been done and continues from there, i.e. it can be started on multiple processors and will run the jobs in parallel.

auto was written by Patrik Sahlin.
 
This documentation (and the example files) was compiled by Henrik Jonsson (henrik.jonsson@slcu.cam.ac.uk), who can provide help.

You run auto by 'auto auto.conf' where auto.conf is a configuration file, and a subdirectory structure (default name autodata) is generated for storing the output of the jobs.

Auto checks the `autodata` directory to figure out which parameter combinations to use next therefore, to restart an auto run the autodata directory must be either renamed, removed or a new output directory name must be specified in the config file.

# Configuration file

Each line of the configuration files must start with a key word:

`VAR varName`

will look for varName in all files provided and replace the name with specified values. It also replaces the varName in the COMMAND line (see below). There are three options to 'scan' parameters/variables:

`VAR PP1 FROM 0.1 TO 1 STEPS 10 LINEAR`

will replace any appearance of `PP1` with the values 0.1-1 scanned linear in ten steps, i.e. 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0.

```
VAR PP2 FROM 1e-3 TO 10 STEPS 9 LOG10
VAR PP3 DISCRETE 0.4 0.5 0.7 1.0 10.0
```

will replace any occurance of `PP1` with values scanned logarithmicly between 0.001 and 10 in 9 steps, and also scan `PP3` over the provided values (creating 9*5 runs in total).

`FILE filename`

the file will be copied to the subdirectory (one per run), any defined varName will be replaced and the file can be used in the COMMAND.

`SYMLINK filename`

a soft link will be made from the subdirectory such that the file can be used in the COMMAND.

`COMMAND command`

the command 'command' will be executed for all scanned variables in the created `autodata/x` subdirectory. You can provide more than one COMMAND that will then be executed sequentially for each set of variables.

`DIRECTORY name`

The default is the data will be saved in a subdirectory named 'autodata'. This will replace the subdirectory name to 'name'

# Example

Assume you want to simulate an organism model, scanning over two reaction parameters, and for three init files. Make sure you are in a directory where you have the files (including a copy/link to 'simulator' unless you have it in your PATH). Replace the parameter values in the model file with 'PARA1', 'PARA2'. 

Then a configuration file can look like:

```
VAR PARA1 FROM 1 TO 100 STEPS 100 LINEAR
VAR PARA2 FROM 1e-3 TO 1e-1 STEPS 7 LOG10
VAR PARA3 DISCRETE 1 2 3 

FILE example.model
SYMLINK simulator
SYMLINK 1.init
SYMLINK 2.init
SYMLINK 3.init
SYMLINK example.rk


COMMAND ./simulator example.model PARA3.init example.rk > ../PARA1_PARA2_PARA3.data 2> ../PARA1_PARA2_PARA3.cerr
```
