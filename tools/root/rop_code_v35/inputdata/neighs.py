#! /usr/bin/env python
# Module with one function for setting the relationships between neighboring cells.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def auxintempl(parDict, auxinTemplate):
    '''Adds details of the auxin template to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Descriptions for PIN and AUX polarity:
    # down = towards the root tip,
    # up = towards the shoot,
    # flip = change from down to up,
    # uniform = uniformly (in all directions),
    # part = uniform in some parts,
    # none = not present.
    
    # Note:
    # 7 layers: 2 stele, protophl, pericycle, endo, cortex, epi.
    # 2 layers: inner, epidemis.
    
    # SET STANDARD VALUES FOR PIN (may be changed below):
    
    # PIN in columella and QC:
    pinQc = ["PIN in QC", 'uniform']
    pinCo = ["PIN in columella", 'uniform']
    
    if '7layers' in auxinTemplate:
        # Relative strength of lateral PINs (0.0 means no lateral PIN):
        pinLat = ["lateral PIN strength", [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]
        # Index of cell in x direction where PIN weakens (0.1 mean not applic):
        pinWeak = ["PIN weakening cell index", [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]]
    
    elif '4layers' in auxinTemplate:
        pinLat = ["lateral PIN strength", [0.0, 0.0, 0.0, 0.0]]
        pinWeak = ["PIN weakening cell index", [0.1, 0.1, 0.1, 0.1]]
    
    elif '2layers' in auxinTemplate:
        pinWeak = ["PIN weakening cell index", [0.1, 0.1]]
    
    # SET SPECIFIC VALUES FOR PIN (depending on template):
    
    # With lateral PIN inward and outward in all layers:
    if '7layers1A' in auxinTemplate:
        pinLayers = ["PIN above QC", ['downlat', 'downlat', 'downlat' \
                    , 'downlat', 'downlat', 'fliplat', 'uplat']]
        pinFlip = ["PIN flip cell index", [0.1, 0.1, 0.1, 0.1, 0.1, 28, 0.1]]
        
        # Relative strength of lateral PINs (0.0 means no lateral PIN):
        indA = auxinTemplate.index('A')
        latA = (int(auxinTemplate[indA+1:]))/100.0
        latVal = [latA for ind in range(0, len(pinLayers[1]))]
        pinLat = ["lateral PIN strength", latVal]
    
    # With lateral PIN in 3 outer layers (endodermis, cortex, epidermis):
    elif ('7layers3latN' in auxinTemplate or '7layers3lat2N' in auxinTemplate \
        or '7layers' in auxinTemplate) and 'N' in auxinTemplate \
        and 'C' in auxinTemplate and 'E' in auxinTemplate:
        pinLayers = ["PIN above QC", ['down', 'down', 'down' \
                    , 'down', 'downin', 'flipin', 'upin']]
        pinFlip = ["PIN flip cell index", [0.1, 0.1, 0.1, 0.1, 0.1, 28, 0.1]]
        
        # Relative strength of lateral PINs (0.0 means no lateral PIN):
        indN = auxinTemplate.index('N')
        indC = auxinTemplate.index('C')
        indE = auxinTemplate.index('E')
        latN = (int(auxinTemplate[indN+1:indC]))/100.0
        latC = (int(auxinTemplate[indC+1:indE]))/100.0
        latE = (int(auxinTemplate[indE+1:]))/100.0
        del(indN, indC, indE)
        pinLat = ["lateral PIN strength", [0.0, 0.0, 0.0, 0.0, latN, latC, latE]]
        if latN == 0.0:
            pinLayers[1][4] = 'down'
        if latC == 0.0:
            pinLayers[1][5] = 'flip'
        if latE == 0.0:
            pinLayers[1][6] = 'up'
        del(latN, latC, latE)
    
    # With lateral PIN in 3 outer layers (endodermis, cortex, epidermis):
    elif ('4layers3latN' in auxinTemplate or '4layers3lat2N' in auxinTemplate) \
        and 'C' in auxinTemplate and 'E' in auxinTemplate:
        pinLayers = ["PIN above QC", ['down', 'downin', 'flipin', 'upin']]
        pinFlip = ["PIN flip cell index", [0.1, 0.1, 28, 0.1]]
        
        # Relative strength of lateral PINs (0.0 means no lateral PIN):
        indN = auxinTemplate.index('N')
        indC = auxinTemplate.index('C')
        indE = auxinTemplate.index('E')
        latN = (int(auxinTemplate[indN+1:indC]))/100.0
        latC = (int(auxinTemplate[indC+1:indE]))/100.0
        latE = (int(auxinTemplate[indE+1:]))/100.0
        del(indN, indC, indE)
        pinLat = ["lateral PIN strength", [0.0, latN, latC, latE]]
        del(latN, latC, latE)
    
    # single cell or cell file (epidermis) version 1:
    elif auxinTemplate in ['epid1', 'single1']:
        pinLayers = ["PIN above QC", ['up']]
        pinFlip = ["PIN flip cell index", [0.1]]
        pinLat = ["lateral PIN strength", [0.0]]
        pinWeak = ["PIN weakening cell index", [0.1]]
    
    # single cell or cell file (epidermis) with lateral PIN in epidermis:
    elif 'singleE' in auxinTemplate or 'epidermisE' in auxinTemplate:
        pinLayers = ["PIN above QC", ['upin']]
        pinFlip = ["PIN flip cell index", [0.1]]
        pinWeak = ["PIN weakening cell index", [0.1]]
        
        # Relative strength of lateral PINs (0.0 means no lateral PIN):
        indE = auxinTemplate.index('E')
        latE = (int(auxinTemplate[indE+1:]))/100.0
        del(indE)
        pinLat = ["lateral PIN strength", [latE]]
        del(latE)
    
    # SET STANDARD VALUES FOR AUX (may be changed below):
    
    # AUX in columella and QC:
    auxQc = ["AUX in QC", 'uniform']
    auxCo = ["AUX in columella", 'uniform']
    
    # SET SPECIFIC VALUES FOR AUX (depending on template):
    if '7layers' in auxinTemplate:
        
        # Number of source, none, sink cells (list of 3 numbers):
        sosiNum = ["num of source/none/sink cells", [5, 0, 2]]
        
        if auxinTemplate in ['7layers3lat1aux1m']:
            auxLayers = ["AUX above QC", ['unipart', 'unipart', 'upunipart' \
                        , 'unipart', 'none', 'none', 'none']]
            auxPart = ["AUX loss cell index", [11, 11, 11, 11, 0.1, 0.1, 0.1]]
        
        elif '7layers3lat2N' in auxinTemplate or \
            '7layers2' in auxinTemplate:
            auxLayers = ["AUX above QC", ['unipart', 'unipart', 'unipart' \
                        , 'unipart', 'none', 'none', 'uniform']]
            auxPart = ["AUX loss cell index", [11, 11, 11, 11, 0.1, 0.1, 0.1]]
        
        elif '7layers1' in auxinTemplate:
            auxLayers = ["AUX above QC", ['unipart', 'unipart', 'upunipart' \
                        , 'unipart', 'none', 'none', 'uniform']]
            auxPart = ["AUX loss cell index", [11, 11, 11, 11, 0.1, 0.1, 0.1]]
    
    elif '4layers' in auxinTemplate:
        sosiNum = ["num of source/none/sink cells", [2, 0, 2]]
        auxLayers = ["AUX above QC", ['upunipart', 'none', 'none', 'uniform']]
        auxPart = ["AUX loss cell index", [11, 0.1, 0.1, 0.1]]
        # IMPORTANT: make up in stele weaker
    
    elif auxinTemplate in ['epid1', 'single1'] \
    or 'singleE' in auxinTemplate or 'epidermisE' in auxinTemplate:
        auxLayers = ["AUX above QC", ['uniform']]
        auxPart = ["AUX loss cell index", [0.1]]
        sosiNum = ["num of source/none/sink cells", [1, 0, 1]]
    
    else:
        raise Exception('Unknown auxin template given.')
    
    # Make dictionary:
    parDict.update({'pinLayers':pinLayers, 'pinQc':pinQc, 'pinCo':pinCo \
        , 'auxLayers':auxLayers, 'auxQc':auxQc, 'auxCo':auxCo \
        , 'pinFlip':pinFlip, 'auxPart':auxPart, 'pinWeak':pinWeak \
        , 'sosiNum':sosiNum, 'pinLat':pinLat})
    
    # Checks:
    if len(parDict['pinLayers'][1]) != len(parDict['auxLayers'][1]):
        raise Exception('PIN and AUX data lists must have same lengths.')
    if len(parDict['pinLayers'][1]) == len(parDict['pinFlip'][1]) \
       == len(parDict['pinLat'][1]) == len(parDict['pinWeak'][1]):
        pass
    else:
        raise Exception('PIN data lists must have same lengths.')
    if len(parDict['auxLayers'][1]) != len(parDict['auxPart'][1]):
        raise Exception('AUX data lists must have same lengths.')               
    
    return

################################################################################

