#! /usr/bin/env python
# Module with one function for setting parameter values for model reactions.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def parameters(parDict, cellTemplate):
    '''Adds parameter values for model reactions to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''

    # Note: The values given here are rough estimations only.
    # Some are collected from various other modeling studies.
    
    # Set auxin transport-related parameter values:
    pHc = ["pH of cytoplasm", 7.2]
    pHw = ["pH of wall/apoplast", 5.3]
    pK = ["pK of auxin", 4.7]
    Nphi =  ["electric potential effect", 4.7916]
    Pa =    ["passive membrane permeability", 5.0*10.0**-6] # dm/s
    Ppin =  ["PIN-mediated permeability", 2.7*10.0**-6] # dm/s
    Paux =  ["AUX-mediated permeability", 5.5*10.0**-6] # dm/s
    
    # Set other auxin parameter values:
    Da =    ["intracell diff of auxin", 3.0*10.0**-8] # dm2/s [estim]
    Dw =    ["apoplastic diff of auxin", 4.4*10.0**-9] # dm2/s [lit/calc]
    pqc =   ["creation of auxin in the QC", 0.0] # umol/(dm3 s)
    q =     ["degradation of auxin", 1.0*10.0**-3] # 1/s [calc min]
    so =    ["creation at source", 2.0*10.0**-3] # umol/(dm3 s)
    si =    ["removal at sink", 0.5] # 1/s
    
    # Settings depending on template:
    if 'single' in cellTemplate:
        pqc[1] = 0.0 # Can not be distinguished from auxin source.
    elif 'file' in cellTemplate:
        pqc[1] = 0.0 # Can not be distinguished from auxin source.
    elif 'Jones' in cellTemplate:
        pqc[1] = 0.0 # Can not be distinguished from auxin source.
    
    # Set ROP parameter values:
    Du =    ["diffusion of ROPa",               1.0*10.0**-11] # dm2/s [lit] 
    Dv =    ["diffusion of ROPi",               1.0*10.0**-9] # dm2/s [lit]
    a =     ["creation of ROPa",                0.0] # umol/(dm3 s) [lit]
    b =     ["creation of ROPi",                0.01] # umol/(dm3 s) [lit]
    c =     ["linear ROP inactivation",         0.1] # 1/s [lit]
    k1 =    ["linear ROP activation",           0.01] # 1/s [lit]
    k2 =    ["nonlin ROP activation",           0.1] # umol2/((dm3)2 s) [lit]
    k3 =    ["nonlin ROP activation by auxin",  0.0] # for testing activ function
    k4 =    ["nonlin ROP activation by autocat",0.0] # for testing activ function
    r =     ["degradation of ROPa",             0.01] # 1/s [lit]
    s =     ["degradation of ROPi",             0.0] # 1/s [lit]
    
    # Make dictionary for parameter values:
    parDict.update({'a':a, 'r':r, 'Du':Du, 'b':b, 's':s, 'Dv':Dv, 'c':c \
        , 'k1':k1, 'k2':k2, 'pqc':pqc, 'q':q, 'Da':Da \
        , 'so':so, 'pHc':pHc, 'pHw':pHw, 'pK':pK, 'Nphi':Nphi \
        , 'Pa':Pa, 'Ppin':Ppin, 'Paux':Paux \
        , 'Dw':Dw, 'si':si, 'k3':k3, 'k4':k4})
    
    return

################################################################################

