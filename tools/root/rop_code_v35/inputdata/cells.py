#! /usr/bin/env python
# Module with one function for setting the cell template (i.e. tissue layout).
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def celltempl(parDict, cellTemplate):
    '''Adds details of the cell template to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Thickness of cell walls (in micrometer):
    wallThick = ["wall thickness", 0.2] # fixed
    
    # Diameter of stele that is not modelled (in micrometer):
    steleOffset = 1.0 # EDIT?
    
    # Cell lengths starting at root tip, increasing towards shoot:
    xLenOrig = [9, 10, 12, 11, 11, 6, 6, 4, 6, 4, 5, 11, 8, 7, 8, 7, 8, 7, \
               13, 13, 12, 14, 9, 12, 24, 21, 24, 25, 22, 37, 44, 52, 33, \
               76, 96, 124, 148, 77, 148, 158, 168, 178] # From Pontus.
    # Sum of lengths in first line is 140 and in first+second is 495.
    
    # Cell widths starting in center of root, increasing outward:
    yLenOrig = [4, 4, 4, 4, 9, 18, 18]
    
    # Set values for root cross-section model with 7 layers:
    if '7layers' in cellTemplate:
        
        # Ratio of volumes of whole root to this template:
        templRatio = ["ratio of template to root", 20.0]
        
        # Radius of hole in stele (i.e. not modelled central root part):
        steleRad = ["stele radius", steleOffset]
        
        # Lengths of cells in y direction, i.e. cell depths (in micrometer):
        # Start at root center, increase radially outwards:
        yLengths = ["cell depths (y dir)", yLenOrig]
        del(yLenOrig)
        
        # Lengths of cells in z direction, i.e. cell widths (in micrometer):
        # Start somewhere on periph, turn such that coord system is right-handed:
        zNumCells = ["cell number (z dir)", 0] # For 2.5D simu, no walls in z dir.
        zNumRoot = ["cell number periphery", 20] # Num of (epid) cells in periph.
        
        # Length of columella region (in number of cells in x direction):
        xCoNum = ["columella length", 3]
        
        # Length of quiescent center (in number of cells in x and y direction):
        xQcNum = ["quiesc center length", 1]
        yQcNum = ["quiesc center depth", 4]
        
        # Type of source and sink:
        sosiType = ["source and sink type", 'bothshoot']
        
        if cellTemplate == '7layers5':
            
            # Lengths of cells in x direction, i.e. cell lengths (in micrometer):
            # Start at root tip, increase towards shoot:
            xLengths = ["cell lengths (x dir)", xLenOrig]
            del(xLenOrig)
        
        elif cellTemplate == '7layersS1': # For Siamsa's project.
            
            # Lengths of cells in x direction, i.e. cell lengths (in micrometer):
            # Start at root tip, increase towards shoot:
            xLen = []
            # Loop over cells until sum is 500:
            for ind in range(0, len(xLenOrig)):
                if sum(xLen) <= 500:
                    xLen.append(xLenOrig[ind])
                else:
                    break
            xLengths = ["cell lengths (x dir)", xLen]
            del(xLenOrig)
        
        elif cellTemplate == '7layersS2': # For Siamsa's project.
            
            # Lengths of cells in x direction, i.e. cell lengths (in micrometer):
            # Start at root tip, increase towards shoot:
            xLen = []
            # Loop over cells until sum is roughly 500:
            for ind in range(0, len(xLenOrig)):
                if sum(xLen) <= 500 and sum(xLen) + xLenOrig[ind] <= 500:
                    xLen.append(xLenOrig[ind])
                else:
                    break
            xLengths = ["cell lengths (x dir)", xLen]
            del(xLenOrig)
        
        elif '7layers2L' in cellTemplate:
            
            # Lengths of cells in x direction:
            indL = cellTemplate.index('L')
            lenFac = (int(cellTemplate[indL+1:]))/100.0
            del(indL)
            xLen = []
            for ind in range(0, len(xLenOrig)):
                # Add shortened cell to list:
                xLen.append(int(round(lenFac*xLenOrig[ind])))
            del(xLenOrig, lenFac, ind)
            xLengths =  ["cell lengths (x dir)", xLen]
    
    # Set values for root cross-section model with 4 layers:
    elif '4layers' in cellTemplate:
        
        # Ratio of volumes of whole root to this template:
        templRatio = ["ratio of template to root", 20.0]
        
        # Radius of hole in stele (i.e. not modelled central root part):
        steleRad = ["stele radius", steleOffset]
        
        # Lengths of cells in y direction, i.e. cell depths (in micrometer):
        # Start at root center, increase radially outwards:
        yLen = [sum(yLenOrig[:4])]
        yLen.extend(yLenOrig[4:])
        del(yLenOrig)
        yLengths = ["cell depths (y dir)", yLen]
        del(yLen)
        
        # Lengths of cells in z direction, i.e. cell widths (in micrometer):
        # Start somewhere on periph, turn such that coord system is right-handed:
        zNumCells = ["cell number (z dir)", 0] # For 2.5D simu, no walls in z dir.
        zNumRoot = ["cell number periphery", 20] # Num of (epid) cells in periph.
        
        # Length of columella region (in number of cells in x direction):
        xCoNum = ["columella length", 3]
        
        # Length of quiescent center (in number of cells in x and y direction):
        xQcNum = ["quiesc center length", 1]
        yQcNum = ["quiesc center depth", 1]
        
        # Type of source and sink:
        sosiType = ["source and sink type", 'bothshoot']
        
        # Set values for root cross-section model version 1:
        if cellTemplate == '4layers1':
            
            # Lengths of cells in x direction, i.e. cell lengths (in micrometer):
            # Start at root tip, increase towards shoot:
            xLengths =  ["cell lengths (x dir)", xLenOrig]
            del(xLenOrig)
        
    # Set values for root cross-section model with 2 layers:
    elif '2layers' in cellTemplate:
        
        # Ratio of volumes of whole root to this template:
        templRatio = ["ratio of template to root", 20.0]
        
        # Radius of hole in stele (i.e. not modelled central root part):
        steleRad = ["stele radius", steleOffset]
        
        # Lengths of cells in x direction, i.e. cell lengths (in micrometer):
        # Start at root tip, increase towards shoot:
        xLengths =  ["cell lengths (x dir)", xLenOrig]
        del(xLenOrig)
        
        # Lengths of cells in y direction, i.e. cell depths (in micrometer):
        # Start at root center, increase radially outwards:
        yLen = [sum(yLenOrig[:-1]), yLenOrig[-1]]
        del(yLenOrig)
        yLengths = ["cell depths (y dir)", yLen]
        del(yLen)
        
        # Lengths of cells in z direction, i.e. cell widths (in micrometer):
        # Start somewhere on periph, turn such that coord system is right-handed:
        zNumCells = ["cell number (z dir)", 0] # For 2.5D simun, no walls in z dir.
        zNumRoot = ["cell number periphery", 20] # Num of (epid) cells in periph.
        
        # Length of quiescent center (in number of cells in x and y direction):
        xQcNum = ["quiesc center length", 1]
        yQcNum = ["quiesc center depth", 1]
        
        # Type of source and sink:
        sosiType = ["source and sink type", 'bothshoot']
        
        # Lengths of cells in y direction, i.e. cell depths (in micrometer):
        # Start at root center, increase radially outwards:
        yLen = [sum(yLenOrig[:-1]), yLenOrig[-1]]
        del(yLenOrig)
        yLengths = ["cell depths (y dir)", yLen]
        del(yLen)
        
        # Set values for simplified root cross-section model version 2:
        if cellTemplate == '2layers2':
            
            # Lengths of cells in x direction, i.e. cell lengths (in micrometer):
            # Start at root tip, increase towards shoot:
            xLengths =  ["cell lengths (x dir)", xLenOrig]
            del(xLenOrig)
            
            # Length of columella region (in number of cells in x direction):
            xCoNum = ["columella length", 3]
        
        # Set values for simplified root cross-section model version 1:
        elif cellTemplate == '2layers1':
            
            # Lengths of cells in x direction, i.e. cell lengths (in micrometer):
            # Start at root tip, increase towards shoot:
            xLen = []
            for ind in range(0, 6):
                xLen.append(sum(xLenOrig[ind*7:(ind+1)*7])) # Add lengths from 7 cells.
            del(xLenOrig, ind)
            xLengths =  ["cell lengths (x dir)", xLen]
            del(xLen)
            
            # Length of columella region (in number of cells in x direction):
            xCoNum = ["columella length", 1]
    
    # Set values for cell file model version 1:
    elif cellTemplate == 'epidermis1':
        
        # Ratio of volumes of whole root to this template:
        templRatio = ["ratio of template to root", 40.0]
        
        # Radius of hole in stele (i.e. not modelled central root part):
        steleRad = ["stele radius", steleOffset]
        
        # Lengths of cells in x direction, i.e. cell lengths (in micrometer):
        # Start at root tip, increase towards shoot:
        xLengths =  ["cell lengths (x dir)", xLenOrig]
        del(xLenOrig)
        
        # Lengths of cells in y direction, i.e. cell depths (in micrometer):
        # Start at root center, increase radially outwards:
        yLengths = ["cell depths (y dir)", [yLenOrig[-1]]]
        del(yLenOrig)
        
        # Lengths of cells in z direction, i.e. cell widths (in micrometer):
        # Start somewhere on periph, turn such that coord system is right-handed:
        zNumCells = ["cell number (z dir)", 0] # For 2.5D simu, no walls in z dir.
        zNumRoot = ["cell number periphery", 20] # Num of (epid) cells in periph.
        
        # Length of columella region (in number of cells in x direction):
        xCoNum = ["columella length", 0]
        
        # Length of quiescent center (in number of cells in x and y direction):
        xQcNum = ["quiesc center length", 0]
        yQcNum = ["quiesc center depth", 0]
        
        # Type of source and sink:
        sosiType = ["source and sink type", 'tipshoot']
    
    # Set values for various cell file models:
    elif cellTemplate in ['fileTest1', 'fileVar1', 'fileVarL1' \
                          , 'Grien', 'Lasko', 'Jones', 'fileVarL2', 'fileVarL3']:
        
        # Ratio of volumes of whole root to this template:
        templRatio = ["ratio of template to root", 40.0]
        
        # Radius of hole in stele (i.e. not modelled central root part):
        steleRad = ["stele radius", steleOffset]
        
        if cellTemplate == 'fileTest1': # Test model version 1:
            # Lengths of cells in x direction, i.e. cell lengths (in micrometer):
            # Start at root tip, increase towards shoot:
            xLen = [10 for ind in range(0, 5)]
            xLen.extend([50 for ind in range(0, 5)])
            del(ind)
        elif cellTemplate in ['fileVar1', 'fileVarL1']:
            # Derived from measured values, but "smoothed":
            xLen = [10 for ind in range(0, 18)]
            xLen.extend([20 for ind in range(0, 12)])
            xLen.extend([50 for ind in range(0, 6)])
            xLen.extend([100 for ind in range(0, 6)])
            xLen.extend([150 for ind in range(0, 2)])
        elif cellTemplate == 'fileVarL2':
            # Adapted such that 100 um cell is about 500 um away from root:
            xLen = [10 for ind in range(0, 10)]
            xLen.extend([20 for ind in range(0, 5)])
            xLen.extend([50 for ind in range(0, 5)])
            xLen.extend([100 for ind in range(0, 5)])
            xLen.extend([150 for ind in range(0, 4)])
        elif cellTemplate == 'fileVarL3': # andre
            # fileVarL2 but small 10 um cells are replaced by 20 um cells
            # Adapted such that 100 um cell is about 500 um away from root:
            xLen = [20 for ind in range(0, 5)]
            xLen.extend([20 for ind in range(0, 5)])
            xLen.extend([50 for ind in range(0, 5)])
            xLen.extend([100 for ind in range(0, 5)])
            xLen.extend([150 for ind in range(0, 4)])
        elif cellTemplate == 'Grien': # Lengths from Grieneisen_2007_ATS:
            xLen = [24 for ind in range(0, 12)]
            xLen.extend([60 for ind in range(0, 21)])
            del(ind)
        elif cellTemplate == 'Lasko': # Lengths from Laskowski_2008_RSA:
            xLen = [16 for ind in range(0, 12)]
            xLen.extend([60 for ind in range(0, 15)])
            xLen.extend([100 for ind in range(0, 15)])
            del(ind)
        elif cellTemplate == 'Jones': # Lengths from Jones_2009_ATN.
            xLen = []
            maxLen = 160 # Maximal length of one cell.
            maxSum = 2000 # Desired total length of template.
            cellLen = 10
            currSum = 0
            while currSum < maxSum: # Add cells until total size is reached.
                for i in range(0, 3):
                    if currSum < maxSum: # Add only if total size not reached yet.
                        xLen.append(cellLen)
                        currSum = sum(xLen)
                if cellLen < maxLen: # Increase length of one cell.
                    cellLen = 2*cellLen # Double length of one cell.
            del(maxLen, maxSum, cellLen, currSum)
        xLengths =  ["cell lengths (x dir)", xLen]
        del(xLen, xLenOrig)
        
        # Lengths of cells in y direction, i.e. cell depths (in micrometer):
        # Start at root center, increase radially outwards:
        yLengths = ["cell depths (y dir)", [yLenOrig[-1]]]
        del(yLenOrig)
        
        # Lengths of cells in z direction, i.e. cell widths (in micrometer):
        # Start somewhere on periph, turn such that coord system is right-handed:
        zNumCells = ["cell number (z dir)", 0] # For 2.5D simu, no walls in z dir.
        zNumRoot = ["cell number periphery", 20] # Num of (epid) cells in periph.
        
        # Length of columella region (in number of cells in x direction):
        xCoNum = ["columella length", 0]
        
        # Length of quiescent center (in number of cells in x and y direction):
        xQcNum = ["quiesc center length", 0]
        yQcNum = ["quiesc center depth", 0]
        
        # Type of source and sink:
        sosiType = ["source and sink type", 'tipshoot']
    
    # Set values for two cell file model and all versions:
    elif 'epidermisTwoX' in cellTemplate:
        
        # Lengths of cells in x direction, i.e. cell lengths (in micrometer):
        # Start at root tip, increase towards shoot:
        cellLen = int(cellTemplate[cellTemplate.index('X') + 1:])
        xLen = [cellLen, cellLen]
        del(cellLen)
        xLengths =  ["cell lengths (x dir)", xLen]
        
        # Lengths of cells in y direction, i.e. cell depths (in micrometer):
        # Start at root center, increase radially outwards:
        yLengths = ["cell depths (y dir)", [yLenOrig[-1]]]
        
        # Lengths of cells in z direction, i.e. cell widths (in micrometer):
        # Start somewhere on periph, turn such that coord system is right-handed:
        zNumCells = ["cell number (z dir)", 0] # For 2.5D simu, no walls in z dir.
        zNumRoot = ["cell number periphery", 20] # Num of (epid) cells in periph.
        
        # Length of columella region (in number of cells in x direction):
        xCoNum = ["columella length", 0]
        
        # Length of quiescent center (in number of cells in x and y direction):
        xQcNum = ["quiesc center length", 0]
        yQcNum = ["quiesc center depth", 0]
        
        # Type of source and sink:
        sosiType = ["source and sink type", 'tipshoot']
            
        # Ratio of volumes of whole root to this template: CHECK THIS
        lenRatio = float(sum(xLenOrig))/float(xLen[0])
        templRatio = ["ratio of template to root", 40.0*lenRatio]
        del(lenRatio)
        
        # Radius of hole in stele (i.e. not modelled central root part):
        steleRad = ["stele radius", steleOffset + sum(yLenOrig[:-1])]
        
        del(xLenOrig, xLen, yLenOrig)
    
    # Set values for cell file model and all versions:
    elif 'fileX' in cellTemplate or 'fileL' in cellTemplate:
        
        # Lengths of cells in x direction, i.e. cell lengths (in micrometer):
        # Start at root tip, increase towards shoot:
        if 'fileX' in cellTemplate:
            cellLen = int(cellTemplate[cellTemplate.index('X') + 1:])
        elif 'fileL' in cellTemplate:
            cellLen = int(cellTemplate[cellTemplate.index('L') + 1:])
        xLen = [cellLen for ind in range(0, 20)]
        del(cellLen)
        xLengths =  ["cell lengths (x dir)", xLen]
        
        # Lengths of cells in y direction, i.e. cell depths (in micrometer):
        # Start at root center, increase radially outwards:
        yLengths = ["cell depths (y dir)", [yLenOrig[-1]]]
        
        # Lengths of cells in z direction, i.e. cell widths (in micrometer):
        # Start somewhere on periph, turn such that coord system is right-handed:
        zNumCells = ["cell number (z dir)", 0] # For 2.5D simu, no walls in z dir.
        zNumRoot = ["cell number periphery", 20] # Num of (epid) cells in periph.
        
        # Length of columella region (in number of cells in x direction):
        xCoNum = ["columella length", 0]
        
        # Length of quiescent center (in number of cells in x and y direction):
        xQcNum = ["quiesc center length", 0]
        yQcNum = ["quiesc center depth", 0]
        
        # Type of source and sink:
        sosiType = ["source and sink type", 'tipshoot']
            
        # Ratio of volumes of whole root to this template: CHECK THIS
        lenRatio = float(sum(xLenOrig))/float(xLen[0])
        templRatio = ["ratio of template to root", 40.0*lenRatio]
        del(lenRatio)
        
        # Radius of hole in stele (i.e. not modelled central root part):
        steleRad = ["stele radius", steleOffset + sum(yLenOrig[:-1])]
        
        del(xLenOrig, xLen, yLenOrig)
    
    # Set values for single cell model and all versions:
    elif 'singleX' in cellTemplate or 'singleL' in cellTemplate:
        
        # Lengths of cells in x direction, i.e. cell lengths (in micrometer):
        # Start at root tip, increase towards shoot:
        if 'singleX' in cellTemplate:
            xLen = [int(cellTemplate[cellTemplate.index('X') + 1:])]
        elif 'singleL' in cellTemplate:
            xLen = [int(cellTemplate[cellTemplate.index('L') + 1:])]
        xLengths =  ["cell lengths (x dir)", xLen]
        
        # Lengths of cells in y direction, i.e. cell depths (in micrometer):
        # Start at root center, increase radially outwards:
        yLengths = ["cell depths (y dir)", [yLenOrig[-1]]]
        
        # Length of columella region (in number of cells in x direction):
        xCoNum = ["columella length", 0]
        
        # Length of quiescent center (in number of cells in x and y direction):
        xQcNum = ["quiesc center length", 0]
        yQcNum = ["quiesc center depth", 0]
        
        # Type of source and sink:
        sosiType = ["source and sink type", 'tipshoot'] # 'bothshoot' or 'tipshoot'.
        
        # Lengths of cells in z direction, i.e. cell widths (in micrometer):
        # Start somewhere on periph, turn such that coord system is right-handed:
        zNumCells = ["cell number (z dir)", 0] # For 2.5D simu, no walls in z dir.
        zNumRoot = ["cell number periphery", 20] # Num of (epid) cells in periph.
        
        # Ratio of volumes of whole root to this template: CHECK THIS
        lenRatio = float(sum(xLenOrig))/float(xLen[0])
        templRatio = ["ratio of template to root", 40.0*lenRatio]
        del(lenRatio)
        
        # Radius of hole in stele (i.e. not modelled central root part):
        steleRad = ["stele radius", steleOffset + sum(yLenOrig[:-1])]
        
        del(xLenOrig, xLen, yLenOrig)
        
    else:
        raise Exception('Unknown cell template given.')
    del(steleOffset)
    
    # Make dictionary for values:
    parDict.update({'steleRad':steleRad, 'wallThick':wallThick \
        , 'xLengths':xLengths, 'yLengths':yLengths, 'zNumCells':zNumCells \
        , 'zNumRoot':zNumRoot, 'xCoNum':xCoNum \
        , 'xQcNum':xQcNum, 'yQcNum':yQcNum \
        , 'sosiType':sosiType, 'templRatio':templRatio})
    
    # Check that no columella or QC is specified for sosiType tipshoot:
    if parDict['sosiType'][1] == 'tipshoot':
        if parDict['xCoNum'][1] > 0:
            raise Exception('No columella possible for source at root tip.')
        if parDict['xQcNum'][1] > 0 or parDict['yQcNum'][1] > 0:
            raise Exception('No QC possible for for source at root tip.')
    
    return

################################################################################

