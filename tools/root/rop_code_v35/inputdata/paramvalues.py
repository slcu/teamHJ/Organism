#! /usr/bin/env python
# Module with one function for setting parameter values for model reactions.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def parameters(parDict, cellTemplate, doRop):
    '''Adds parameter values for model reactions to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''

    # Note: The values given here are rough estimations only.
    # Some are collected from various other modeling studies.
    
    # Program control:
    numDigits = 2 # Number of significant digits.
    doNewDu = 0
    doNewk1 = 0
    doNewk2 = 0
    doNewr = 0
    doNews = 0
    doLarsson = 0
    if doRop == 1:
        ropModel = 'combined'
    elif doRop == 2:
        ropModel = 'separate'
    elif doRop == 3:
        ropModel = 'auxinind'
    elif doRop == 4:
        ropModel = 'autocat'
    elif doRop == 0:
        ropModel = 'none'
    if 'single' in cellTemplate or 'file' in cellTemplate \
    or 'Jones' in cellTemplate:
        doHairCell = 1
        #doHairCell = 0
    elif '7layersS' in cellTemplate:
        doHairCell = 0
    else:
        raise Exception('Specify if cell should be hair cell.')

    # Set values needed for calculation of parameters:
    pHc = 7.2 # Jones
    pHw = 5.3 # Jones
    pK = 4.7
    z = -1.0 # valence
    F = 9.6*10.0**4 # Faraday constant [C/mol]
    V = -0.12 # membrane potential [J/C]
    R = 8.3 # gas constant [J/(mol*K)]
    T = 293 # absolute temperature [K]
    Pa = 5.6*10.0**-6 # passive membrane permeability, dm/s
    Ppin = 2.8*10.0**-6 # PIN-mediated permeability, dm/s
    Paux = 5.6*10.0**-6 # AUX-mediated permeability, dm/s
    if doHairCell == 1:
        Paux = 0.6*Paux # 60% is weighted mean (H = 20%, N = 100%)
    
    # Calculate auxin fractions in the cell and wall:
    fracC = 1.0/(1.0 + 10.0**(pHc - pK))
    fracW = 1.0/(1.0 + 10.0**(pHw - pK))
    del(pHc, pHw, pK)
    
    # Calculate parameters for passive transport:
    val_Def = fracC*Pa
    val_Din = fracW*Pa

    #Def = ["passive auxin efflux", val_Def]
    #Din = ["passive auxin influx", val_Din]
    Def = ["passive auxin efflux", 1.8e-8]
    Din = ["passive auxin influx", 1.1e-6]

    del(Pa, val_Def, val_Din)
    
    # Calculate parameters for active transport:
    Nphi = (z*F*V)/(R*T)
    del(z, F, V, R, T)
    val_Tap = (1-fracC)*Nphi*Ppin
    del(fracC, Ppin)
    val_Tap = val_Tap
    val_Taa = (1-fracW)*Nphi*Paux
    del(fracW, Nphi, Paux)
    val_Taa = val_Taa
    val_Taa = val_Taa
    #Tap = ["active auxin efflux", val_Tap]
    #Taa = ["active auxin influx", val_Taa]
    Tap = ["active auxin efflux", 1.3e-5]
    Taa = ["active auxin influx", 1.3e-5]

    del(val_Tap, val_Taa)
    
    # Set auxin parameter values for diffusion and degradation:
    #Da = ["intracell diff of auxin", 3.0*10.0**-8] # dm2/s [estim]
    #Da = ["intracell diff of auxin", 10.0**-8] # dm2/s [estim]
    Da = ["intracell diff of auxin", 2.0*10.0**-8] # dm2/s [estim]
    Dw = ["apoplastic diff of auxin", 4.4*10.0**-9] # dm2/s [lit/calc]
    pqc = ["creation of auxin in the QC", 0.0*10.0**-4] # umol/(dm3 s) [calc]
    q = ["degradation of auxin", 0.0005] # 1/s [calc min?]
    
    # Set other auxin parameter values:
    so =    ["creation at source", 0.1] # umol/(dm3 s)
    si =    ["removal at sink", 0.2] # 1/s
    
    # Settings depending on template:
    if 'single' in cellTemplate:
        pqc[1] = 0.0 # Can not be distinguished from auxin source.
    elif 'file' in cellTemplate:
        pqc[1] = 0.0 # Can not be distinguished from auxin source.
    elif 'Jones' in cellTemplate:
        pqc[1] = 0.0 # Can not be distinguished from auxin source.
    
    # Set ROP parameter values:
    Du =    ["diffusion of ROPa",               1.0*10.0**-11] # dm2/s [lit] 
    Dv =    ["diffusion of ROPi",               1.0*10.0**-9] # dm2/s [lit]
    a =     ["creation of ROPa",                0.0] # umol/(dm3 s) [lit]
    b =     ["creation of ROPi",                0.01] # umol/(dm3 s) [lit]
    c =     ["linear ROP inactivation",         0.1] # 1/s [lit]
    k1 =    ["linear ROP activation",           0.01] # 1/s [lit]
    k2 =    ["nonlin ROP activation",           0.02] # umol2/((dm3)2 s) [lit]
    k3 =    ["nonlin ROP activation by auxin",  0.0] # for testing activ function
    k4 =    ["nonlin ROP activation by autocat",0.0] # for testing activ function
    r =     ["degradation of ROPa",             0.01] # 1/s [lit]
    s =     ["degradation of ROPi",             0.0] # 1/s [lit]
    if ropModel == 'combined':
        k1[1] = 0.01 # 1/s [lit]
        k2[1] = 0.02 # umol2/((dm3)2 s) [lit]
        k3[1] = 0.0 # for testing activ function
        k4[1] = 0.0 # for testing activ function
    elif ropModel == 'separate':
        k1[1] = 0.01 # 1/s [lit]
        k2[1] = 0.0 # umol2/((dm3)2 s) [lit]
        k3[1] = 0.1 # for testing activ function
        k4[1] = 0.05 # for testing activ function
    
    # Make dictionary for parameter values:
    parDict.update({'a':a, 'r':r, 'Du':Du, 'b':b, 's':s, 'Dv':Dv, 'c':c \
        , 'k1':k1, 'k2':k2, 'pqc':pqc, 'q':q, 'Da':Da \
        , 'so':so, 'Def':Def, 'Din':Din, 'Tap':Tap, 'Taa':Taa \
        , 'Dw':Dw, 'si':si, 'k3':k3, 'k4':k4})
    
    return

################################################################################

