#! /usr/bin/env python
# Module with one function that finds neighbors.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def findneigh(numComp, currComp, initMat, xCompSizeList, yCompSizeList, \
              zCompSizeList, eTol):
    '''Find neighbours and length of contact edge of current compartment.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    from toolbox import variables
    
    # Get information about current compartment:
    currXCompSize = xCompSizeList[currComp]
    currYCompSize = yCompSizeList[currComp]
    currZCompSize = zCompSizeList[currComp]
    currInit = initMat[currComp] # Line from .init for current compartment.
    
    # Get column indices for .init file:
    [colIndDict, colList] = variables.colind('.init')
    xCol = colIndDict['x']
    yCol = colIndDict['y']
    zCol = colIndDict['z']
    cCol = colIndDict['cellMarker']
    del(colIndDict, colList)
    
    # Initialize variables:
    neighList = [] # Initialize list for indices of neighbors.
    contList = [] # Init list for lengths of contact edges.
    
    # Loop over other compartments:
    for otherComp in range(0, numComp): # Loop from 0 to numComp-1.
        if otherComp != currComp:
            otherInit = initMat[otherComp] # Line from .init for curr comp.
            otherXCompSize = xCompSizeList[otherComp] # Size in x dir.
            otherYCompSize = yCompSizeList[otherComp] # Size in y dir.
            otherZCompSize = zCompSizeList[otherComp] # Size in z dir.
            
            # Calc dist betw curr and other comp in each direction:
            xRelDist = currXCompSize/2 + otherXCompSize/2 # This dist is nb.
            yRelDist = currYCompSize/2 + otherYCompSize/2 # This dist is nb.
            zRelDist = currZCompSize/2 + otherZCompSize/2 # Upper bound for nb!
            xDist = abs(otherInit[xCol] - currInit[xCol])
            yDist = abs(otherInit[yCol] - currInit[yCol])
            zDist = abs(otherInit[zCol] - currInit[zCol])
            del(otherInit)
            
            # Look for neigh in z dir (actual distance smaller than relevant):
            checkD = zRelDist - zDist <= eTol # Need this order and no abs!
            del(zRelDist)
            if xDist == 0.0 and yDist == 0.0 and checkD == True:
                check1 = (currXCompSize == otherXCompSize)
                check2 = (currYCompSize == otherYCompSize)
                if check1 == False or check2 == False:
                    raise Exception('Problem with neighbor finding.')
                del(check1, check2)
                neighList.append(otherComp)
                
                # Contact edge depends on comp size in x and y direction:
                contList.append(currXCompSize*currYCompSize)
            del(checkD)
            
            # Look for neighbor in y direction:
            checkD = abs(yDist - yRelDist) <= eTol
            del(yRelDist)
            if xDist == 0.0 and zDist == 0.0 and checkD == True:
                check1 = (currXCompSize == otherXCompSize)
                if check1 == False:
                    raise Exception('Problem with neighbor finding.')
                del(check1)
                neighList.append(otherComp)
                
                # Contact area depends on comp size in x and z direction:
                contList.append(currXCompSize*currZCompSize)
            del(checkD, otherXCompSize)
            
            # Look for neighbor in x direction:
            checkD = abs(xDist - xRelDist) <= eTol
            del(xRelDist)
            if yDist == 0.0 and zDist == 0.0 and checkD == True:
                check1 = (currYCompSize == otherYCompSize)
                check2 = (currZCompSize == otherZCompSize)
                if check1 == False or check2 == False:
                    raise Exception('Problem with neighbor finding.')
                del(check1, check2)
                neighList.append(otherComp)
                
                # Contact edge depends on comp size in y and z direction:
                contList.append(currYCompSize*currZCompSize)
            del(checkD, otherYCompSize, otherZCompSize)
            
            del(xDist, yDist, zDist)
    del(numComp, currComp, initMat, eTol, xCompSizeList, yCompSizeList, \
        zCompSizeList, otherComp, currXCompSize, currYCompSize, \
        currZCompSize, xCol, zCol)
    
    # Check list of neighbors: EDIT (adapt for 3D)
    #if currInit[cCol] == 1 and len(neighList) != 4 and currInit[yCol] >= 0.0:
    #    raise Exception('Cell compartment does not have 4 neighbours.')
    #elif currInit[cCol] == 1 and len(neighList) not in [1, 2, 3] and currInit[yCol] < 0.0:
    #    raise Exception('External auxin applic comp does not have 1 to 3 neighbours.')
    #elif currInit[cCol] == 0 and len(neighList) not in [2, 3, 4]:
    #    raise Exception('Wall compartment does not have 2, 3, or 4 neighbours.')
    #if len(neighList) != len(contList):
    #    raise Exception('Lists of neighbors and contact edges do not have same lengths.')
    #if len(neighList) == 0:
    #    raise Exception('Neighbor list is empty.')
    del(currInit, yCol)
    
    return [neighList, contList]

################################################################################

