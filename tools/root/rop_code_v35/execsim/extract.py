#! /usr/bin/env python
# Module with functions related to extraction of results.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def summass(baseName, parDict):
    '''Saves summed auxin masses in root sections and whole template from .final file.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014-15).'''
    
    import numpy
    from toolbox import variables
    
    # Settings:
    eTol = 10**-4 # Error tolerance for checks.
    
    # Get column indices for .final file:
    [colIndDict, colList] = variables.colind('.final')
    del(colList)
    
    # Get lengths of sections for summed masses:
    xCutLen = parDict['xCutLen'][1]
    
    # Get number of sections for summed masses:
    numSecs = parDict['numCuts'][1] - 1
    del(parDict)
    
    # Get .final data as array:
    finalArray = numpy.loadtxt(baseName + '.final', skiprows=1)
    
    # Extract columns with summed auxin mass, their total and the template total:
    sumMass = finalArray[:, colIndDict['sumMass']]
    sumMassAll = finalArray[:, colIndDict['sumMassAll']]
    sumMassTempl = finalArray[:, colIndDict['sumMassTempl']]
    
    # Extract x coordinate of each compartment:
    xList = finalArray[:, colIndDict['x']]
    del(finalArray, colIndDict)
    
    # Extract relevant data:
    auxinSumList = [] # Initialize list for summed masses.
    prevX = float(xList[0]) # Dummy for previous x coordinate.
    for ind in range(0, len(sumMass)): # Loop over summed masses.
        # Skip zeros at the beginning and put first non-zero value in list:
        if sumMass[ind] == 0.0:
            continue
        elif auxinSumList == []:
            auxinSumList.append(sumMass[ind])
            nextCut = float(xList[ind]) + xCutLen # Initialize position of next cut.
        currX = float(xList[ind])
        if currX != prevX:
            if currX > nextCut:
                auxinSumList.append(sumMass[ind])
                nextCut += xCutLen # Update x position for new section.
        prevX = currX # Update previous x position.
        del(currX)
    del(ind, prevX, nextCut, xList, xCutLen)
    
    # Check data:
    sumMassUnique = sorted(list(set(sumMass)))
    if 0.0 in sumMassUnique: # Must be first element because list is sorted.
        sumMassUnique = sumMassUnique[1:] # Delete 0 from list.
    for origSum in sumMassUnique:
        if origSum not in auxinSumList:
            raise Exception('Original sum value is not in processed list.')
    del(sumMassUnique, origSum, sumMass)
    
    # EDIT the following to exclude source/sink even for short templates!
    # Delete entry from tissues above last cut (e.g. source/sink):
    if len(auxinSumList) > numSecs:
        auxinSumList = auxinSumList[:numSecs]
    
    # Check data:
    if len(auxinSumList) != numSecs:
        print(len(auxinSumList), numSecs)
        raise Exception('Incorrect number of root sections.')
    
    # Get total summed mass of auxin in sections:
    totalMass = set(sumMassAll) # Get unique elements.
    del(sumMassAll)
    totalMass = totalMass.difference(set([0.0])) # Exclude zero.
    totalMass = list(totalMass)
    totalMass = totalMass[0]
    if numSecs == 3 and abs(totalMass - sum(auxinSumList)) > eTol:
        print(totalMass, sum(auxinSumList))
        raise Exception('Total mass in sections does not match sum of sections.')
    auxinSumList.append(totalMass)
    del(numSecs)
    
    # Get total summed mass of auxin in whole template:
    templMass = set(sumMassTempl) # Get unique elements.
    del(sumMassTempl)
    templMass = templMass.difference(set([0.0])) # Exclude zero.
    templMass = list(templMass)
    templMass = templMass[0]
    if templMass - totalMass < 0:
        raise Exception('Total mass cannot be larger than mass in whole template.')
    auxinSumList.append(templMass)
    del(totalMass, templMass)
    
    # Save summed masses as text file:
    fileName = baseName + '.sums'
    numpy.savetxt(fileName, auxinSumList)
    print("Saved file " + fileName)
    del(fileName, auxinSumList)
    
    return

################################################################################

def datacolumns(currName, currKeyList, saveTime):
    '''Extracts chosen data columns from .data file.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import pickle
    from toolbox import variables
    
    fileName = currName + '.data'
    
    # Get column indices:
    [colIndDict, colList] = variables.colind('.data')
    keyList = colIndDict.keys()
    del(colList)
    
    # Open .data file:
    fileObj = open(fileName, 'r')
    del(fileName)
    
    # Preallocate dictionary for columns:
    colListDict = {}
    for currKey in currKeyList: # Loop over chosen keys.
        if currKey in keyList: # Check if chosen key exists in .data file.
            colListDict[currKey] = [] # Add empty list for current column.
        else:
            raise Exception('Chosen key does not exist in .data file.')
    del(currKey, keyList)
    
    # Loop over rows (compartments):
    while 1 == 1: # Trick to iterate until break is found.
        line = fileObj.readline()
        lineElem = line.split(' ') # Get elements that were sep. by whitespaces.
        if lineElem == ['']: # End of file.
            break # Exit from while loop.
        lineElem.remove('\n') # Remove end-of-line character.
        lineElem = map(float, lineElem) # Convert strings to floats.
        if len(lineElem) > 0:
            # Check for data of last time point:
            if lineElem[colIndDict['timeind']] == float(saveTime-1):
                
                # Loop over chosen keys:
                for currKey in currKeyList:
                    
                    # Save curr entry in corresponding list in dictionary:
                    currList = colListDict[currKey]
                    currList.append(lineElem[colIndDict[currKey]])
                    colListDict[currKey] = currList
                    del(currList)
                del(currKey)
    del(line, lineElem, colIndDict, currKeyList, saveTime)
    
    # Save dictionary with lists from columns:
    pickleName = currName + '_data_cols.pkl'
    fileObj = open(pickleName, 'w')
    pickle.dump(colListDict, fileObj)
    fileObj.close()
    print('Dictionary with data from columns has been saved in file ' + pickleName)
    del(currName, colListDict, pickleName, fileObj)
    
    return

################################################################################

def allcolumns(currName, currFile):
    '''Extracts all data columns from .init or .final file.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-13).'''

    import pickle
    from toolbox import variables 

    # Check file type:
    if currFile in ['.init', '.final']:
        fileName = currName + currFile
    else:
        raise Exception('Not implemented for file type ' + currFile + '.')
    
    # Get column indices:
    [colIndDict, colList] = variables.colind(currFile)
    keyList = colIndDict.keys()
    del(colList)
    
    # Open .init or .final file:
    fileObj = open(fileName, 'r')
    del(fileName)
    
    # Get information about data set from first line in file:
    line = fileObj.readline() # Read the first line.
    lineElem = line.split(' ') # Get elements that were separated by whitespaces.
    numRows = int(lineElem[0]) # Number of compartments, i.e. rows.
    numCols = int(lineElem[1]) # Number of columns, i.e. entries per compartment.
    del(line, lineElem)
    if len(keyList) != numCols:
        print(len(keyList), numCols)
        raise Exception('Number of keys is not equal to number of columns.')
    
    # Preallocate dictionary for columns:
    colListDict = {}
    for currKey in keyList: # Loop over keys.
        colListDict[currKey] = [] # Add empty list for current column.
    del(currKey)
    
    # Loop over rows (compartments):
    for currRow in range(0, numRows):
        line = fileObj.readline()
        lineElem = line.split(' ') # Get elements that were sep. by whitespaces.
        lineElem.remove('\n') # Remove end-of-line character.
        if len(lineElem) == numCols: # True for all data lines (not first line).
            
            # Loop over keys (covers columns/entries for curr comp):
            for currKey in keyList:
                
                # Get current entry:
                currCol = colIndDict[currKey]
                currElem = lineElem[currCol]
                
                # Save current entry in corresponding list in dictionary:
                currList = colListDict[currKey]
                currList.append(currElem)
                colListDict[currKey] = currList
                del(currList, currElem, currCol)
            del(currKey)
        
        else:
            raise Exception('Wrong number of elements per line.')
        del(line, lineElem)
    del(colIndDict, numRows, numCols)
    
    # Save dictionary with lists from columns:
    pickleName = currName + '_' + currFile[1:] + '_cols.pkl'
    fileObj = open(pickleName, 'w')
    pickle.dump(colListDict, fileObj)
    fileObj.close()
    print('Dictionary with column data has been saved in ' + pickleName)
    del(colListDict, pickleName, fileObj)
    
    return

################################################################################

def relcolumns(currName, simPhase, parDict):
    '''Extracts data columns for relevant x and y values from column dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-13).'''

    import pickle
    
    # Settings:
    xyTol = 10**(-6) # Tolerance for finding same x or y values.
    relKeyListOrig = ['x', 'y', 'auxin', 'ROPactive', 'ROPinactive', 'wallMarker']
    
    # Open dictionary file with all columns:
    allPickleName = currName + '_' + simPhase + '_cols.pkl'
    fileObj = open(allPickleName, 'r')
    allColListDict = pickle.load(fileObj)
    fileObj.close()
    del(fileObj)
    
    # Extract data for each relevant y value:
    for yVal in parDict['plotY'][1]: # Loop over relevant y values.
        relKeyList = relKeyListOrig[:] # Copy list with relevant keys.
        allYColList = allColListDict['y']
        
        # Find relevant indices using the y column list:
        relIndList = []
        for currInd in range(0, len(allYColList)): # Loop over list entries.
            if abs(float(allYColList[currInd]) - yVal) < xyTol:
                relIndList.append(currInd)
        del(currInd, allYColList)
        
        # Preallocate dictionary for relevant columns:
        relColListDict = {}
        for currKey in relKeyList: # Loop over relevant keys.
            relColListDict[currKey] = [] # Add empty list for current column.
        del(currKey)
        
        # Loop over relevant keys:
        for currKey in relKeyList: # Loop over relevant keys.
            
            # Extract relevant entries from all column lists:
            oldList = allColListDict[currKey]
            newList = []
            for relInd in relIndList: # Loop over relevant indices.
                newList.append(oldList[relInd])
            relColListDict[currKey] = newList
            del(oldList, newList, currKey)
        
        # Save dictionary for current y value:
        relPickleName = allPickleName[:-4] + '_' + str(yVal) + 'y.pkl'
        fileObj = open(relPickleName, 'w')
        pickle.dump(relColListDict, fileObj)
        fileObj.close()
        print('Dictionary with relevant column data has been saved in file ' + relPickleName)
        del(relColListDict, relPickleName, fileObj, yVal)
    
    # Extract data for each relevant x value:
    for xVal in parDict['plotX'][1]: # Loop over relevant x values.
        allXColList = allColListDict['x']
        
        # Find relevant indices using the x column list:
        relIndList = []
        for currInd in range(0, len(allXColList)): # Loop over list entries.
            if abs(float(allXColList[currInd]) - xVal) < xyTol:
                relIndList.append(currInd)
        del(currInd, allXColList)
        
        # Preallocate dictionary for relevant columns:
        relColListDict = {}
        for currKey in relKeyList: # Loop over relevant keys.
            relColListDict[currKey] = [] # Add empty list for current column.
        del(currKey)
        
        # Loop over relevant keys:
        for currKey in relKeyList: # Loop over relevant keys.
            
            # Extract relevant entries from all column lists:
            oldList = allColListDict[currKey]
            newList = []
            for relInd in relIndList: # Loop over relevant indices.
                newList.append(oldList[relInd])
            relColListDict[currKey] = newList
            del(oldList, newList, currKey)
        
        # Save dictionary for current x value:
        relPickleName = allPickleName[:-4] + '_' + str(xVal) + 'x.pkl'
        del(allPickleName)
        fileObj = open(relPickleName, 'w')
        pickle.dump(relColListDict, fileObj)
        fileObj.close()
        print('Dictionary with relevant column data has been saved in file ' + relPickleName)
        del(relColListDict, relPickleName, fileObj, xVal)
    del(allColListDict)
    
    return

################################################################################

def stattext(baseName, simLabel, timeA, maxDerivList):
    '''Saves simulation statistics in text file.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-13).'''
    
    # Make text string:
    fileText = "Number of simulations:\n" + str(simLabel+1) + "\n\n" \
             + "runs per simulation:\n" + str(timeA) + "\n\n" \
             + "maximal absolute derivative per simulation:\n"
    del(simLabel, timeA)
    for maxDeriv in maxDerivList:
        fileText = fileText + str(maxDeriv) + "\n"
    del(maxDeriv, maxDerivList)
    
    # Save text string to file:
    fileName = baseName + '.stats'
    fileObj = open(fileName, 'w')
    fileObj.write(fileText)
    fileObj.close()
    print("Saved file " + fileName)
    del(fileName, fileObj, fileText)
    
    return

################################################################################

