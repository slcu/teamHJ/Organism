#! /usr/bin/env python
# Module with functions related to merging simulation results.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def mergedata(baseName, labelList, parDict):
    '''Merges .data files given in list of labels.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import os, pickle
    from toolbox import variables
    
    # Get column indices:
    [colIndDict, colList] = variables.colind('.data')
    del(colList)
    
    # Open new .data file for merged data:
    newFileObj = open(baseName + '_new.data', 'w')
    
    # Loop over given .data files:
    for currLabel in labelList:
        
        # Open current .data file:
        currFileObj = open(baseName + currLabel + '.data', 'r')
        
        # Copy contents of current file into new file:
        while 1 == 1: # Runs until break is found.
            line = currFileObj.readline()
            lineElem = line.split(' ')
            if lineElem == ['']: # End of file.
                break # Exit from while loop.
            elif lineElem != ['\n'] and \
                float(lineElem[colIndDict['timeind']]) >= parDict['saveTime'][1]:
                break 
            if lineElem == ['\n'] or \
                float(lineElem[colIndDict['timeind']]) < parDict['saveTime'][1]:
                newFileObj.write(line)
            del(line, lineElem)
        
        # Close current file:
        currFileObj.close()
        del(currFileObj)
    del(currLabel)
    
    # Close new file:
    newFileObj.close()
    del(newFileObj)
    
    # Delete old .data files:
    for currLabel in labelList:
        os.remove(baseName + currLabel + '.data')
    del(currLabel, labelList)
    
    # Rename new file into all file:
    os.rename(baseName + '_new.data', baseName + '_all.data')
    print('Merged .data files into one common all.data file.')
    
    return

################################################################################



