#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# SET LIST OF ALLOWED VARIATIONS:
varTypeList = ['so', 'si', 'pqc', 'pK', 'pHc', 'pHw' \
               , 'pHc', 'pHw', 'Pa', 'Ppin', 'Paux', 'q' \
               , 'Da', 'Dw', 'Du', 'Dv', 'c', 'k1', 'k2' \
               , 'r', 's', 'b', 'so', 'si', 'facPinAux' \
               , 'k3', 'k4', 'Def', 'Din', 'Tap', 'Taa']

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE SECTIONS FOR WHICH CONCENTRATION CURVES WILL BE PLOTTED:
plotSecY = ['firstComp'] # firstComp, lastComp, firstWall, lastWall
plotSecX = []

# CHOOSE IF .data FILE SHOULD BE DELETED TO SAVE DISK SPACE:
doKeepDataFile = 0

# CHOOSE IF CONCENTRATIONS SHOULD BE SAVED AFTER EACH RUN:
doKeepConc = 1

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import time, os, pickle, copy, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings, varsettings
from execsim import auxinsim, ropsim, ropsimpercell
from execplot import simplots

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Construct project name:
projName = os.path.basename(__file__)
projName = projName[4:-3]

# Get information from input arguments:
argList = sys.argv
[doRop, cellTemplate, auxinTemplate, varType, xyzNumComp, initCond, \
simStrategy] = variables.arguments(argList)
if varType not in varTypeList:
    raise Exception('Given varType is not in varTypeList.')
elif varType in varTypeList:
    varParName = varType
del(varType)
if 'single' not in cellTemplate:
    raise Exception('Use single cell template here.')
if cellTemplate[cellTemplate.find('single') + 6] in ['X', 'L']:
    cellTempl = cellTemplate[:7]
else:
    raise Exception('Use single cell template here.')
del(cellTemplate)
auxinTemplList = [auxinTemplate]

# SET CELL AND AUXIN TEMPLATE:
cellTemplList = []
for lengthVal in [10, 20, 50, 100, 150]: # relevant for cell file
    cellTemplList.append(cellTempl + str(lengthVal))
del(lengthVal)
appTemplate = ''

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplList, auxinTemplList, appTemplate, xyzNumComp)
raw_input('Check settings and press any key to continue:')

# Start timer:
startTime = time.time()

# Get code version number:
vNrStr = dirnavi.codeversion()

# Change directory:
os.chdir(outPath)

# Loop over cell templates to be compared:
pathList = [[], []] # Initialize list for paths to simulation directories.
for cellTemplate in cellTemplList:
    #print(cellTemplate)
    
    for auxinTemplate in auxinTemplList:
        #print(auxinTemplate)
        #raw_input()
        
        # Collect information about template into a dictionary:
        infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, \
                   auxinTemplate, appTemplate, xyzNumComp, initCond, consSys, doRop)
        
        # Get parameter values:
        origParDict = parvalues.setallpar(infoDict)
        
        # Update parDict regarding tolerance for stopping:
        simsettings.tolerance(origParDict, infoDict['cellTemplate'])
        
        # Add plot settings to parDict:
        origParDict['plotSecX'] = ["sections in x dir for plots", plotSecX]
        origParDict['plotSecY'] = ["sections in y dir for plots", plotSecY]
        
        # Set names of directories for different levels:
        origLevelList = dirnavi.dirlevel(infoDict, origParDict)
        
        # Change directory:
        dirnavi.mkchdir(origLevelList[0])
        dirnavi.mkchdir(origLevelList[1])
        dirnavi.mkchdir(origLevelList[2])
        
        # Check if pickled parDict from template exists (as example):
        pickleName = 'template/' + baseName + '_parDict.pkl'
        if os.path.exists(pickleName) == False:
            print(cellTemplate, xyzNumComp)
            raise Exception('No variable with parameters found, make template first ' \
            + '(use run_simfiles.py).')
        del(pickleName)
        
        # Get factor list for variation:
        varFacList = varsettings.factors(varParName)
        
        # Preallocate list for path names (for auxin and rop simulations):
        pathList = [[], []]
        
        # Loop over factors for chosen parameter value:
        for currVarFac in varFacList:
                
            # Copy parameter values:
            currParDict = copy.deepcopy(origParDict)
            
            # Update current dictionary with parameter values:
            currParDict[varParName][1] = currVarFac*currParDict[varParName][1]
            del(currVarFac)
            #print('\n' + varParName + ': ' + str(currParDict[varParName][1]) + '\n')
            #raw_input()
            
            # Set names of directories for different levels:
            levelList = dirnavi.dirlevel(infoDict, currParDict)
            #print(levelList)
            #raw_input()
            
            # Check for existing auxin simulation directory (levels 3 and 4):
            doSim = dirnavi.skipdir('auxin', levelList, simStrategy)
            
            # Change directory:
            dirnavi.mkchdir(levelList[3])
            dirnavi.mkchdir(levelList[4])
            
            if doSim == 1:
                # Pickle parDict (i.e. save as a variable):
                pickleName = baseName + '_parDict.pkl'
                fileObj = open(pickleName, 'w')
                pickle.dump(currParDict, fileObj)
                fileObj.close()
                print("Saved file " + pickleName)
                del(pickleName, fileObj)
                
                # Save list of parameter values as text:
                variables.partext(baseName, currParDict)
                
                # Run simulation for auxin:
                auxinsim.simulate(baseName, currParDict, levelList, doKeepDataFile, doKeepConc)
            
            elif doSim == 0:
                if os.path.exists(baseName + '.final') == True \
                    and os.path.exists(baseName + '.sums') == False \
                    and ('single' in levelList[1]) == False \
                    and ('file' in levelList[1]) == False:
                    extract.summass(baseName, currParDict)
            del(doSim)
            
            # Save path to auxin simulation directory:
            currPath = dirnavi.simpath('auxin', levelList)
            pathList[0].append(currPath)
            del(currPath)
            
            if doRop > 0:
                # Change directory:
                dirnavi.mkchdir('../' + levelList[7])
                
                # Check for existing ROP simulation directory (levels 8 and 9):
                doSim = dirnavi.skipdir('rop', levelList, simStrategy)
                
                # Change directory:
                dirnavi.mkchdir(levelList[8])
                dirnavi.mkchdir(levelList[9])
                
                if doSim == 1:
                    # Pickle parDict (i.e. save as a variable):
                    pickleName = baseName + '_parDict.pkl'
                    fileObj = open(pickleName, 'w')
                    pickle.dump(currParDict, fileObj)
                    fileObj.close()
                    print("Saved file " + pickleName)
                    del(pickleName, fileObj)

                    # Save list of parameter values as text:
                    variables.partext(baseName, currParDict)
                    
                    # Run ROP simulation per cell:
                    ropsim.simulate(baseName, currParDict, levelList, doKeepDataFile, doKeepConc)
                
                del(doSim)

                # Save path to ROP simulation directory:
                currPath = dirnavi.simpath('rop', levelList)
                pathList[1].append(currPath)
                del(currPath)

                # Change directory:
                os.chdir('../../')
            
            del(levelList, currParDict)

            # Change directory:
            os.chdir('../..')
        
        # Change directory:
        os.chdir('../../..')
        
        del(auxinTemplate, infoDict)
    del(cellTemplate)
del(plotSecX, plotSecY)

# Construct project name:
projName = projName + '_' + varParName

# Change directory:
dirnavi.mkchdir('results' + vNrStr + '/')
dirnavi.mkchdir(projName)

# Set strings for cell and auxin template depending on project:
cellTemplStr = 'varCellTempl'
auxinTemplStr = auxinTemplList[0]

# Check number of previous files of given type and set name for current run:
maxNum = dirnavi.numfiles('.txt', projName, cellTemplStr, auxinTemplStr, xyzNumComp)
countName = cellTemplStr + '_' + auxinTemplStr + appTemplate + '_' \
      + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
      + str(xyzNumComp[2]) + '_' + projName + '_' + str(maxNum+1)
del(cellTemplStr, auxinTemplStr, appTemplate, xyzNumComp, consSys, maxNum, projName)

# Save list with paths to simulation directories as text:
fileName = countName + '.txt'
fileObj = open(fileName, 'w')
for simPhase in [0, 1]:
    for currPath in pathList[simPhase]:
        if currPath != '':
            fileObj.write(currPath + '\n')
fileObj.close()
print('Saved file ' + fileName)
del(fileName, fileObj, pathList)

# Calculate run time:
endTime = time.time()
runTime = endTime - startTime
del(startTime, endTime)
[runHr, runRem] = divmod(runTime, 3600)
[runMin, runRest] = divmod(runRem, 60)
print('Run time: ' + str(int(runHr)) + ' hrs, ' + str(int(runMin)) + ' mins')
del(runTime, runHr, runRem, runMin, runRest)

