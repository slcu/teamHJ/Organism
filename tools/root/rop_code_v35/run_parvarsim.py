#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# SET LIST OF ALLOWED VARIATIONS:
varTypeList = ['so', 'si', 'pqc', 'pK', 'pHc', 'pHw' \
               , 'pHc', 'pHw', 'Pa', 'Ppin', 'Paux', 'q' \
               , 'Da', 'Dw', 'Du', 'Dv', 'c', 'k1', 'k2' \
               , 'r', 's', 'b', 'so', 'si', 'facPinAux' \
               , 'k3', 'k4', 'Def', 'Din', 'Tap', 'Taa']

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

# CHOOSE SECTIONS FOR WHICH CONCENTRATION CURVES WILL BE PLOTTED:
plotSecY = ['firstComp'] # firstComp, lastComp, firstWall, lastWall
plotSecX = []

# CHOOSE IF .data FILE SHOULD BE DELETED TO SAVE DISK SPACE:
doKeepDataFile = 0

# CHOOSE IF CONCENTRATIONS SHOULD BE SAVED AFTER EACH RUN:
doKeepConc = 1

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import time, os, pickle, copy, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings, varsettings
from execsim import auxinsim, extract, ropsim

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Construct project name:
projName = os.path.basename(__file__)
projName = projName[4:-6]

# Get information from input arguments:
argList = sys.argv
[doRop, cellTemplate, auxinTemplate, varType, xyzNumComp, initCond, \
simStrategy] = variables.arguments(argList)
if '_' in varType:
    indUn = varType.find('_')
    setParName = varType[:indUn]
    varParName = varType[indUn+1:]
    if '_' in varParName:
        raise Exception('Variation type can have maximal two parameters.')
    projName = projName + '2'
else:
    setParName = ''
    varParName = varType
    projName = projName + '1'
#print(varType, setParName, varParName)
#raw_input()
if (setParName not in varTypeList and setParName != '') or varParName not in varTypeList:
    raise Exception('Given varType is not in varTypeList.')
if varParName == setParName:
    raise Exception('Parameter for different situations can not be the varied parameter.')

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplate, auxinTemplate, appTemplate, xyzNumComp, \
      initCond, varType, simStrategy)
raw_input('Check settings and press any key to continue:')

# Start timer:
startTime = time.time()

# Get code version number:
vNrStr = dirnavi.codeversion()

# Collect information about template into a dictionary:
infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, auxinTemplate, \
                               appTemplate, xyzNumComp, initCond, consSys, doRop)
del(initCond)

# Get parameter values:
origParDict = parvalues.setallpar(infoDict)

# Set special parameter values:
#origParDict['Tap'][1] = 10.0*origParDict['Tap'][1]
#origParDict['so'][1] = 0.01
#origParDict['q'][1] = 10.0**-4
#origParDict['r'][1] = 10.0**-3
#origParDict['r'][1] = 5.0*10.0**-4
#origParDict['b'][1] = 10.0**-3
#origParDict['s'][1] = 10.0**-4
#origParDict['c'][1] = 1.0
#origParDict['k1'][1] = 5.0*origParDict['k1'][1]
#specFacList = [1.0, 2.0, 5.0, 10.0, 50.0]
#specFacList.append([float(parVal) for parVal in range(5, 31, 5)])
#specFacList = [float(parVal) for parVal in range(6, 11)]
#del(parVal)
#print(specFacList)
#raw_input('Special parameter values!')

# Set names of directories for different levels:
origLevelList = dirnavi.dirlevel(infoDict, origParDict)

# Change directory:
os.chdir(outPath)
dirnavi.mkchdir(origLevelList[0])
dirnavi.mkchdir(origLevelList[1])
dirnavi.mkchdir(origLevelList[2])

# Check if pickled parDict from template exists (as example):
pickleName = 'template/' + baseName + '_parDict.pkl'
if os.path.exists(pickleName) == False:
    raise Exception('No variable with parameters found, make template first ' \
    + '(use run_simfiles.py).')
del(pickleName)

# Update parDict regarding tolerance for stopping:
simsettings.tolerance(origParDict, infoDict['cellTemplate'])

# Add plot settings to parDict:
origParDict['plotSecX'] = ["sections in x dir for plots", plotSecX]
origParDict['plotSecY'] = ["sections in y dir for plots", plotSecY]
del(plotSecX, plotSecY)

# Get factor lists for set and for variation:
if setParName == '':
    setFacList = [1.0]
else:
    setFacList = varsettings.factors(setParName)
    #setFacList = [0.5, 1.0, 2.0]
    #setFacList = [2.0, 5.0, 10.0]
    #setFacList = [0.1, 0.3, 3.0, 10.0]
    #setFacList = [float(parVal) for parVal in range(1, 6)]
    #del(parVal)
    # for so in single cell when testing file (default 0.01):
    #setFacList = [4.0, 5.0, 8.0, 9.0, 10.0]
    #setFacList = specFacList
varFacList = varsettings.factors(varParName)
#varFacList = [0.5, 1.0, 2.0]
#varFacList = [2.0, 3.0, 4.0]
#varFacList = [0.1*float(parVal) for parVal in range(1, 11)]
#del(parVal)
#print(varFacList)
#varFacList = specFacList
#raw_input('Special parameter variations!')
print(varParName, varFacList)
raw_input('Check list with factors and press any key to continue:')

# Loop over settings for which chosen parameter is varied:
for currSetFac in setFacList:
    
    # Copy original parameter values:
    setParDict = copy.deepcopy(origParDict)
    
    # Update parameter for current variation set:
    if setParName != '':
        setParDict[setParName][1] = currSetFac*setParDict[setParName][1]
        print('\n' + setParName + ': ' + str(setParDict[setParName][1]) + '\n')
        #raw_input()
    
    # Preallocate list for path names (for auxin and rop simulations):
    pathList = [[], []]
    
    # Loop over factors for chosen parameter value:
    for currVarFac in varFacList:
            
        # Copy parameter values:
        currParDict = copy.deepcopy(setParDict)
        
        # Update current dictionary with parameter values:
        currParDict[varParName][1] = currVarFac*currParDict[varParName][1]
        del(currVarFac)
        #print('\n' + varParName + ': ' + str(currParDict[varParName][1]) + '\n')
        #raw_input()
        
        # Set names of directories for different levels:
        levelList = dirnavi.dirlevel(infoDict, currParDict)
        #print(levelList)
        #raw_input()
        
        # Check for existing auxin simulation directory (levels 3 and 4):
        doSim = dirnavi.skipdir('auxin', levelList, simStrategy)
        
        # Change directory:
        dirnavi.mkchdir(levelList[3])
        dirnavi.mkchdir(levelList[4])
        
        if doSim == 1:
            # Pickle currParDict (i.e. save as a variable):
            pickleName = baseName + '_parDict.pkl'
            fileObj = open(pickleName, 'w')
            pickle.dump(currParDict, fileObj)
            fileObj.close()
            print("Saved file " + pickleName)
            del(pickleName, fileObj)
            
            # Save list of parameter values as text:
            variables.partext(baseName, currParDict)
            
            # Run simulation for auxin:
            auxinsim.simulate(baseName, currParDict, levelList, \
                              doKeepDataFile, doKeepConc)
            print('Finished auxin simulation.')
        
        #elif doSim == 0:
        #    if os.path.exists(baseName + '.sums') == False:
        #        print(os.getcwd())
        #        raise Exception('File .sums is missing for auxin.')
        del(doSim)
        
        # Save path to auxin simulation directory:
        currPath = dirnavi.simpath('auxin', levelList)
        pathList[0].append(currPath)
        del(currPath)
        
        if doRop > 0:
            # Change directory:
            dirnavi.mkchdir('../' + levelList[7])
            
            # Check for existing ROP simulation directory (levels 8 and 9):
            doSim = dirnavi.skipdir('rop', levelList, simStrategy)
            
            # Change directory:
            dirnavi.mkchdir(levelList[8])
            dirnavi.mkchdir(levelList[9])
            
            if doSim == 1:
                # Pickle parDict (i.e. save as a variable):
                pickleName = baseName + '_parDict.pkl'
                fileObj = open(pickleName, 'w')
                pickle.dump(currParDict, fileObj)
                fileObj.close()
                print("Saved file " + pickleName)
                del(pickleName, fileObj)

                # Save list of parameter values as text:
                variables.partext(baseName, currParDict)

                # Run ROP simulation per cell:
                ropsim.simulate(baseName, currParDict, levelList, \
                                doKeepDataFile, doKeepConc)
                print('Finished ROP simulation.')
                
            #elif doSim == 0:
            #    if os.path.exists(baseName + '.sums') == False:
            #        print(os.getcwd())
            #        raise Exception('File .sums is missing for ROP.')
            del(doSim)
            
            # Save path to ROP simulation directory:
            currPath = dirnavi.simpath('rop', levelList)
            pathList[1].append(currPath)
            del(currPath)

            # Change directory:
            os.chdir('../../')
        
        del(levelList, currParDict)
        
        # Change directory:
        os.chdir('../..')
    
    del(setParDict)
    
    # Construct project name:
    currProjName = projName + '_' + varType
    
    # Change directory:
    dirnavi.mkchdir('../../../results' + vNrStr + '/')
    dirnavi.mkchdir(currProjName)
    
    # Check number of previous files of given type:
    maxNum = dirnavi.numfiles('.txt', varType, cellTemplate, \
                              auxinTemplate, xyzNumComp)
    
    # Set name for current run of current project:
    countName = cellTemplate + '_' + auxinTemplate + appTemplate + '_' \
        + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
        + str(xyzNumComp[2]) + '_' + currProjName + '_' + str(maxNum+1)
    del(currProjName, maxNum)
    
    # Save list with paths to simulation directories as text:
    fileName = countName + '.txt'
    fileObj = open(fileName, 'w')
    for simPhase in [0, 1]:
        for currPath in pathList[simPhase]:
            if currPath != '':
                fileObj.write(currPath + '\n')
    fileObj.close()
    print('Saved file ' + fileName)
    del(countName, fileName, fileObj, pathList)
    
    # Change directory:
    os.chdir('../../' + origLevelList[0] + origLevelList[1] + origLevelList[2])
    #if setParName != '':
    #    raw_input()

del(varType, vNrStr, infoDict, origParDict, projName, origLevelList)
del(cellTemplate, auxinTemplate, appTemplate, xyzNumComp, consSys)
del(setParName, varParName, setFacList, varFacList)

# Calculate run time:
endTime = time.time()
runTime = endTime - startTime
del(startTime, endTime)
[runHr, runRem] = divmod(runTime, 3600)
[runMin, runRest] = divmod(runRem, 60)
print('Run time: ' + str(int(runHr)) + ' hrs, ' + str(int(runMin)) + ' mins')
del(runTime, runHr, runRem, runMin, runRest)

