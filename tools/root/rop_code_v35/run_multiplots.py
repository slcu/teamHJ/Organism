#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# CHOOSE WHICH RUN OF CURRENT PROJECT SHOULD BE USED:
resNumDef = 'last'

# SET LIST OF ALLOWED VARIATIONS:
projCheckList = ['stopcrit', 'vartempl', 'parvar', 'parvar1', 'parvar2' \
               , 'initcond', 'manual', 'discret']

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import os, pickle, copy, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings
from plotfiles import plotsettings, forplots, timecurves
from execplot import simplots
from latexfiles import resultstex, forlatex

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Get information from input arguments:
argList = sys.argv
[doRop, cellTemplate, auxinTemplate, projName, xyzNumComp, initCond, \
simStrategy] = variables.arguments(argList)
del(simStrategy)
if projName[:projName.find('_')] not in projCheckList and projName not in projCheckList:
    raise Exception('Given project name is not in allowed list.')
del(projCheckList)

# ASK USER WHICH RUNS TO USE:
print('Which run schould be used?')
resNum = raw_input('Press enter for "last" or type number or type range (with "-"): ')
if resNum == '':
    resNumList = [resNumDef]
elif '-' in resNum:
    begNum = resNum[:resNum.find('-')]
    endNum = resNum[resNum.find('-')+1:]
    resNumList = range(int(begNum), int(endNum)+1)
    del(begNum, endNum)
else:
    resNumList = [int(resNum)]
del(resNumDef)

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplate, auxinTemplate, appTemplate, xyzNumComp, \
      initCond, projName, resNumList)
raw_input('Check settings and press any key to continue:')

# Set list of plots to be made:
if 'single' in cellTemplate:
    plotList = ['final_curve', 'time_curve', 'stats_per_cell']
elif 'file' in cellTemplate:
    plotList = ['final_curve', 'time_curve', 'stats_per_cell'] #, 'max_deriv'] #, 'final_newman']
    plotList = ['final_curve', 'time_curve', 'stats_per_cell'] #, 'max_deriv'] #, 'final_newman']
elif '7layers' in cellTemplate:
    plotList = ['final_curve']
if doRop == 0:
    substList = ['auxin']
elif doRop > 0:
    substList = ['auxin', 'ropa', 'ropi', 'roptotal', 'ropratio']

# Get code version number:
vNrStr = dirnavi.codeversion()

# Loop over result file numbers:
for resNum in resNumList:
    
    # Change directory:
    os.chdir(outPath + 'results' + vNrStr + '/')
    os.chdir(projName)
    
    # In case of variation over two parameters, find set and var parameters:
    if 'parvar2' in projName:
        pass
    
    # Check how many runs of current project have been done:
    if 'discret' in projName:
        maxNum = dirnavi.numfiles('.txt', projName, cellTemplate, \
                                  auxinTemplate, [])
    elif 'vartempl_length' in projName:
        maxNum = dirnavi.numfiles('.txt', projName, 'varCellTempl', \
                                  auxinTemplate, xyzNumComp)
    elif 'vartempl_lateral' in projName:
        maxNum = dirnavi.numfiles('.txt', projName, cellTemplate, \
                                  'varAuxinTempl', xyzNumComp)
    elif 'vartempl_constcomp' in projName:
        maxNum = dirnavi.numfiles('.txt', projName, 'varCellTempl', \
                                  auxinTemplate, [])
    elif 'manual' in projName:
        maxNum = dirnavi.numfiles('.txt', projName, cellTemplate, \
                                  auxinTemplate, [])
    else:
        maxNum = dirnavi.numfiles('.txt', projName, cellTemplate, \
                                  auxinTemplate, xyzNumComp)
    
    # Set name for chosen run of current project:
    if resNum == 'last':
        resNum = maxNum
    elif resNum > maxNum:
        resNum = maxNum
        print('Chosen number is too high, will take last result.')
        raw_input()
    if 'discret' in projName or 'manual' in projName:
        countName = cellTemplate + '_' + auxinTemplate + appTemplate \
            + '_' + projName + '_' + str(maxNum)
    elif 'parvar' in projName \
    or 'stopcrit' in projName \
    or 'initcond' in projName:
        countName = cellTemplate + '_' + auxinTemplate + appTemplate + '_' \
            + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
            + str(xyzNumComp[2]) + '_' + projName + '_' + str(resNum)
    elif 'vartempl_lateral' in projName:
        countName = cellTemplate + '_varAuxinTempl' + appTemplate + '_' \
            + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
            + str(xyzNumComp[2]) + '_'  + projName + '_' + str(resNum)
    elif 'vartempl_length' in projName:
        countName = 'varCellTempl_' + auxinTemplate + appTemplate + '_' \
            + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
            + str(xyzNumComp[2]) + '_'  + projName + '_' + str(resNum)
    elif 'vartempl_constcomp' in projName:
        countName = 'varCellTempl_' + auxinTemplate + appTemplate + '_' \
            + projName + '_' + str(resNum)
    #elif 'manual' in projName:
    #    countName = cellTemplate + '_' + auxinTemplate + '_' \
    #                + projName + str(resNum)
    
    # Load list with paths to simulation directories:
    fileName = countName + '.txt'
    fileObj = open(fileName, 'r')
    origPathList = fileObj.readlines()
    fileObj.close()
    print("Loaded file " + fileName)
    del(fileName, fileObj)
    
    # Format list with paths to simulation directories:
    pathList = [[], []] # Initialize list in correct format.
    for currPath in origPathList:
        currPath = currPath.replace('\n', '')
        if 'Asim' in currPath:
            pathList[0].append(currPath)
        elif 'Rsim' in currPath:
            pathList[1].append(currPath)
        else:
            raise Exception('Path can not be identified as auxin or ROP.')
    del(origPathList)
    
    # Change directory:
    os.chdir('../../')
    
    # Loop over paths to auxin and corresponding ROP simulations:
    for pInd in range(0, len(pathList[0])):
        # Set path names for simulation and plot directories:
        auxinPath = pathList[0][pInd]
        returnAuxinPath = '../../../../../'
        if doRop == 0:
            ropPath = ''
            returnRopPath = ''
            currSimPath = auxinPath
            currReturnPath = returnAuxinPath
        elif doRop > 0:
            if pathList[1] == []:
                raise Exception('ROP path list is empty, simulate first with ROP.')
            ropPath = pathList[1][pInd]
            returnRopPath = '../../../../../../../'
            currSimPath = ropPath
            currReturnPath = returnRopPath
        #print(auxinPath)
        #print(ropPath)
        #raw_input('Auxin and ROP paths')
        
        # Change directory:
        os.chdir(currSimPath)
        
        # Unpickle parDict:
        pickleName = baseName + '_parDict.pkl'
        fileObj = open(pickleName, 'r')
        parDict = pickle.load(fileObj)
        fileObj.close()
        print("Loaded file " + pickleName)
        del(pickleName, fileObj)
        
        # Change directory:
        os.chdir(currReturnPath)
        
        # Change directory:
        os.chdir(auxinPath)
        
        # Add settings for visualization:
        plotsettings.visual(parDict)
        
        # Get x and y values of sections for which conc curves will be plotted:
        pickleName = '../../template/' + baseName + '_comp.pkl'
        forplots.getsections(baseName, parDict, pickleName, 'auxin')
        del(pickleName)
        
        # Plot auxin concentration curves over time:
        if 'time_curve' in plotList:
            timecurves.gnuplot(baseName, parDict, 'auxin', '.eps')
            if doRop > 0:
                os.chdir(returnAuxinPath + ropPath)
                timecurves.gnuplot(baseName, parDict, 'ropa', '.eps')
                timecurves.gnuplot(baseName, parDict, 'ropi', '.eps')
                os.chdir(returnRopPath + auxinPath)
        
        # Change directory:
        os.chdir(returnAuxinPath)
        
        # Plot final simulation results:
        simplots.finalplots(baseName, auxinPath, ropPath, plotList)
        
        # Make first part of file for presentation:
        if pInd == 0:
            simPath = auxinPath[:auxinPath.find('/')]
            text = resultstex.latexbeg(simPath, parDict, cellTemplate, \
                                       auxinTemplate, appTemplate)
            del(simPath)
        
        # Set part of title for current variation:
        if 'default' == projName or 'manual' == projName:
            partTitle = projName
        elif 'discret' == projName:
            # Find value of discretization:
            partTitle = str(parDict['xNumComp'][1]) + 'x ' \
                      + str(parDict['yNumComp'][1]) + 'y ' \
                      + str(parDict['zNumComp'][1]) + 'z'
        elif 'parvar' in projName \
        or 'stopcrit' in projName:
            # Find name and value of varied parameter:
            parName = projName[projName.find('_') + 1:]
            partTitle = parName + ' = ' + str(parDict[parName][1])
        elif 'initcond' in projName:
            # Find value of initial condition:
            parName = projName[projName.find('_') + 1:]
            if parName == 'auxin':
                partTitle = 'auxin = ' + str(parDict['initAuxin'][1])
            elif parName in ['roptotal', 'ropafrac']:
                partTitle = 'ropa = ' + str(parDict['initRopa'][1]) \
                          + ', ropi = ' + str(parDict['initRopi'][1])
        elif 'vartempl' in projName:
            # Find name of cell and auxin template:
            partTitle = auxinPath[auxinPath.find('/')+1:]
            partTitle = partTitle[:partTitle.find('/')]
            unInd = partTitle.find('_')
            partTitle = partTitle[:unInd] + ' ' + partTitle[unInd+1:]
            del(unInd)
        
        # Edit path names for simulation and plot directories:
        auxinPlotPath = '../../' + auxinPath.replace('sim_', 'plot_')
        ropPlotPath = '../../' + ropPath.replace('sim_', 'plot_')
        currPlotPath = '../../' + currSimPath.replace('sim_', 'plot_')
        del(currSimPath)
        
        # Make section for presentation with curve plots for all substances:
        if 'final_curve' in plotList:
            addText = resultstex.latexpervar(baseName, currPlotPath, partTitle, \
                      parDict['plotY'][1], parDict['zoomList'][1])
            text = text + addText
            del(addText)
        
        # Make section for presentation with derivatives:
        if 'max_deriv' in plotList:
            addText = resultstex.latexderiv(baseName, currPlotPath, partTitle, \
                      substList)
            text = text + addText
            del(addText)
        
        # Make section for presentation with auxin concentration curves over time:
        if 'time_curve' in plotList:
            addText = resultstex.latextime(auxinPlotPath, partTitle, ['auxin'])
            text = text + addText
            del(addText)
            if doRop > 0:
                addText = resultstex.latextime(ropPlotPath, partTitle, ['ropa'])
                text = text + addText
                addText = resultstex.latextime(ropPlotPath, partTitle, ['ropi'])
                text = text + addText
                del(addText)
        del(auxinPlotPath, ropPlotPath)
        del(partTitle, currPlotPath)
    
    del(parDict)
    
    # Change directory:
    os.chdir('results' + vNrStr + '/' + projName)
    
    # Make end of presentation file:
    addText = resultstex.latexend()
    text = text + addText
    del(addText)
    
    # Save latex file and compile it:
    forlatex.latexfile(countName + '_plots', text)
    #raw_input('Copy figures if needed!')
    
    # Delete auxiliary files from LaTeX:
    forlatex.dellatex(countName + '_plots')

    # Delete comparison plots:
    fileList = os.listdir(os.getcwd())
    for fileName in fileList:
        currNamePart = projName + '_' + str(resNum) + '_'
        if currNamePart in fileName and '.eps' in fileName:
            os.remove(fileName)
        del(currNamePart, fileName)
    del(resNum, fileList)
    
del(resNumList, projName)


