#! /usr/bin/env python
# Module with functions related to plotting of simulation results.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def finalplots(baseName, auxinPath, ropPath, plotList):
    '''Plot final result of simulation in various forms.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-15).'''
    
    import os, pickle
    from toolbox import dirnavi
    from plotfiles import forplots, maxabsderiv, newman, plotsettings
    from plotfiles import curvespervar, curvespersubst
    
    # Check for simulation phase:
    if ropPath == '':
        doRop = 0
    else:
        doRop = 1
    
    # Change directory:
    os.chdir(auxinPath)
    
    # Set directory names:
    auxinSimDir = auxinPath[auxinPath.find('Asim_'):]
    auxinPlotDir = auxinSimDir.replace('sim', 'plot')
    
    # Check if .sums file exists, i.e. if simulation is done:
    if os.path.exists(baseName + '.sums') == False:
        raise Exception('Current auxin simulation is not finished yet.')       

    # Unpickle parDict:
    pickleName = baseName + '_parDict.pkl'
    fileObj = open(pickleName, 'r')
    parDict = pickle.load(fileObj)
    fileObj.close()
    print("Loaded file " + pickleName)
    del(pickleName, fileObj)

    # Add settings for visualization:
    plotsettings.visual(parDict)
    
    # Get x and y values of sections for which conc curves will be plotted:
    pickleName = '../../template/' + baseName + '_comp.pkl'
    forplots.getsections(baseName, parDict, pickleName, 'auxin')
    del(pickleName)
    
    # Change directory:
    dirnavi.mkchdir('../' + auxinPlotDir)
    del(auxinPlotDir)
    
    # Plot final concentrations of all substances:
    if 'final_curve' in plotList:
        # Loop over zoom values:
        for zoom in parDict['zoomList'][1]:
            if 'single' in auxinPath \
            or 'file' in auxinPath:
                curvespervar.gnuplot(baseName, auxinSimDir, parDict, \
                                     zoom, 'final', '.eps')
            else:
                curvespersubst.gnuplot(baseName, auxinSimDir, parDict, \
                                       zoom, 'final', '.eps')
    del(auxinSimDir, auxinPath)
    
    # Plot maximal absolute derivative for auxin over time:
    if 'max_deriv' in plotList:
        maxabsderiv.gnuplot(baseName, 'A', parDict, '.eps')

    # Plot final auxin concentration with Newman:
    if 'final_newman' in plotList:
        newman.plot(baseName, 'A', parDict, '.ps') # .ps or .tiff
    del(parDict)
    
    # Change directory:
    os.chdir('../../../../..')

    if doRop == 1:
        # Change directory:
        os.chdir(ropPath)
        
        # Set directory names:
        ropSimDir = ropPath[ropPath.find('Rsim_'):]
        ropPlotDir = str.replace(ropSimDir, 'sim', 'plot')
        
        # Unpickle parDict:
        pickleName = baseName + '_parDict.pkl'
        fileObj = open(pickleName, 'r')
        parDict = pickle.load(fileObj)
        fileObj.close()
        print("Loaded file " + pickleName)
        del(pickleName, fileObj)
        
        # Add settings for visualization:
        plotsettings.visual(parDict)
        
        # Get x and y values of sections for which conc curves will be plotted:
        pickleName = '../../../../template/' + baseName + '_comp.pkl'
        forplots.getsections(baseName, parDict, pickleName, 'rop')
        del(pickleName)
        
        # Change directory:
        dirnavi.mkchdir('../' + ropPlotDir)
        
        # Plot final concentrations of all substances:
        if 'final_curve' in plotList and doRop == 1:
            # Loop over zoom values:
            for zoom in parDict['zoomList'][1]:
                if 'single' in ropPath \
                or 'file' in ropPath:
                    curvespervar.gnuplot(baseName, ropSimDir, parDict, \
                                         zoom, 'final', '.eps')
                else:
                    curvespersubst.gnuplot(baseName, ropSimDir, parDict, \
                                         zoom, 'final', '.eps')
        del(ropPath)
        
        # Plot maximal absolute derivative for ropa over time:
        if 'max_deriv' in plotList:
            maxabsderiv.gnuplot(baseName, 'R', parDict, '.eps')

        # Plot final ropa concentration with Newman:
        if 'final_newman' in plotList:
            newman.plot(baseName, 'R', parDict, '.ps') # .ps or .tiff
        del(parDict)
        
        # Change directory:
        os.chdir('../../../../../../..')
    
    return

################################################################################

def frameplots(baseName, simPhase, plotList, infoDict, parDict):
    '''Plot intermediate results from simulation in various forms.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    import os, shutil, pickle, subprocess
    from toolbox import dirnavi
    from execsim import extract
    from plotfiles import forplots, curvespersubst #, maxabsderiv, newman
    from execplot import extractdata
    
    # Change directory:
    levelList = dirnavi.dirlevel(infoDict, parDict)
    if simPhase == 'A':
        dirnavi.mkchdir('../' + levelList[6])
    elif simPhase == 'R':
        dirnavi.mkchdir('../' + levelList[11])
    
    # Obtain data for each time point from .data file and last time value:
    lastTimeVal = extractdata.blocks(baseName, simPhase, parDict, levelList)
    
    # Find number of digits of last time value:
    numDigLast = len(str(lastTimeVal))
    
    # Get time step between blocks:
    timeStep = int(parDict['timeA'][1]/parDict['saveTime'][1])
    
    # Loop over time points:
    timeVal = 0 # Initialize time point.
    while 1 == 1: # Loop over time points until break is found.
        # Skip first time point (i.e. inital conditions):
        if timeVal == 0:
            # Update time point:
            timeVal += timeStep
            continue
        
        # Check if timeVal is within available range:
        if timeVal == lastTimeVal:
            break
        
        # Make string from time value and pad with zeros:
        numDigCurr = len(str(timeVal))
        if numDigCurr == numDigLast:
            timeValStr = str(timeVal)
        elif numDigCurr < numDigLast:
            # Make string with missing leading zeros:
            timeValStr = [0 for dig in range(0, numDigLast - numDigCurr)]
            timeValStr = ''.join(map(str, timeValStr))
            timeValStr = timeValStr + str(timeVal)
        elif numDigCurr > numDigLast:
            raise Exception('Current and last number of digits are inconsistent.')
        del(numDigCurr)
        
        # Loop over plot types:
        for plotType in plotList:
            
            # Loop over zoom values:
            for zoom in parDict['zoomList'][1]:
                
                if plotType == 'curve_subst':
                    if simPhase == 'A':
                        # Make current plot for current time point and current zoom:
                        curvespersubst.gnuplot(baseName, levelList[4], \
                                               parDict, zoom, timeVal, '.png')
                        
                        # Rename plot files such that time value is padded with zeros:
                        oldName = baseName + '_curves_auxin_' + str(zoom) + 'dm_' \
                                  + str(timeVal) + '.png'
                        newName = baseName + '_curves_auxin_' + str(zoom) + 'dm_' \
                                  + timeValStr + '.png'
                        os.rename(oldName, newName)
                        del(oldName, newName)
                    
                    elif simPhase == 'R':
                        # Make current plot for current time point and current zoom:
                        curvespersubst.gnuplot(baseName, levelList[9], \
                                               parDict, zoom, timeVal, '.png')
                        
                        # Rename plot files such that time value is padded with zeros:
                        oldName = baseName + '_curves_ropa_' + str(zoom) + 'dm_' \
                                  + str(timeVal) + '.png'
                        newName = baseName + '_curves_ropa_' + str(zoom) + 'dm_' \
                                  + timeValStr + '.png'
                        os.rename(oldName, newName)
                        del(oldName, newName)
                del(zoom)
            del(plotType)
        
        # Delete pickled data array:
        os.remove(baseName + '_data_array_' + str(timeVal) + '.pkl')
        
        # Update time point:
        timeVal += timeStep
    
    del(timeVal, lastTimeVal, numDigLast)
    
    # Loop over plot types:
    for plotType in plotList:
        
        # Loop over zoom values:
        for zoom in parDict['zoomList'][1]:
            
            if simPhase == 'A':
                # Make current movie from frames for current zoom:
                if plotType == 'curve_subst':
                    frameName = baseName + '_curves_auxin_' + str(zoom) + 'dm_*.png'
                    aviName = baseName + '_curves_auxin_' + str(zoom) + 'dm.avi'
            elif simPhase == 'R':
                # Make current movie from frames for current zoom:
                if plotType == 'curve_subst':
                    frameName = baseName + '_curves_ropa_' + str(zoom) + 'dm_*.png'
                    aviName = baseName + '_curves_ropa_' + str(zoom) + 'dm.avi'
            subprocess.call(['mencoder', 'mf://' + frameName, '-mf', 'fps=5' \
            , '-ovc', 'xvid', '-xvidencopts', 'bitrate=1200' \
            , '-o', aviName])
            print('Saved movie ' + aviName)
            del(zoom, aviName)

            # Delete frames:
            delName = frameName[:frameName.rfind('_')]
            del(frameName)
            for currFile in os.listdir(os.getcwd()):
                if delName in currFile and '.png' in currFile:
                    os.remove(currFile)
                del(currFile)
        
        del(plotType)
    
    del(plotList)
    
    # Change directory:
    if simPhase == 'A':
        os.chdir('../' + levelList[4])
    elif simPhase == 'R':
        os.chdir('../' + levelList[9])
    del(levelList)
    
    return

################################################################################


