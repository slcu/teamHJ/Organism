#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def plot(baseName, simPhase, parDict, fileType):
    '''Make plot of result from one simulation using Newman.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import pickle, subprocess, os, math
    from toolbox import variables
    
    # Get values:
    sTol = parDict['sTol' + simPhase][1]
    sCount = parDict['sCount' + simPhase][1]
    
    # Get column indices for .final file:
    [colIndDict, colList] = variables.colind('.final')
    del(colList)
    vCol = colIndDict['V']
    del(colIndDict)
    
    # Open .final file:
    finalFileName = '../' + simPhase + 'sim_' + str(sTol) \
                 + '_' + str(sCount) + '/' + baseName + '.final'
    del(sTol, sCount)
    finalFileObj = open(finalFileName, 'r')
    del(finalFileName)
    
    # Make .nmf file (Newman final):
    plotFileName = baseName + '.nmf'
    plotFileObj = open(plotFileName, 'w')
    plotFileObj.write('1\n') # First line with number of time points.
    
    # Get information about cell template from first line in .final file:
    lineObj = finalFileObj.readline() # Read the first line.
    lineElem = lineObj.split(' ') # Get elements that were separated by whitespaces.
    numComp = int(lineElem[0]) # Number of compartments, i.e. rows.
    numCol = int(lineElem[1]) # Numbe of columns.
    del(lineObj, lineElem)
    plotFileObj.write(str(numComp) + ' ' + str(numCol) + ' 0 \n') # 0 is time index.
    
    # Loop over compartments (remaining lines in .final file):
    for currComp in range(0, numComp):
        lineObj = finalFileObj.readline() # Read next line in .final file.
        lineElem = lineObj.split(' ') # Get elements (were sep by whitesp).
        del(lineObj)
        if len(lineElem)-1 != numCol: # lineElem has end-of-line character.
            raise Exception('Line has wrong length.')
        volVal = float(lineElem[vCol]) # Volume of current compartment.
        radVal = (0.75*volVal/math.pi)**(1.0/3.0) # Calculate corresp radius.
        lineElem[vCol] = str(radVal)
        del(volVal, radVal)
        lineObj = ' '.join(lineElem)
        plotFileObj.write(lineObj)
        del(lineObj, lineElem)
    del(currComp, numComp, numCol, vCol)
    plotFileObj.write('\n')

    # Close files:
    finalFileObj.close()
    del(finalFileObj)
    plotFileObj.close()
    print("Saved file " + plotFileName)
    del(plotFileObj)
    
    # Execute Newman plotting:
    subprocess.call(['nPlot', '-d', '3', '-output', fileType[1:], \
                     plotFileName[:-4], plotFileName])
    print('Saved result of Newman for file ' + plotFileName)
    
    # Delete .plot file:
    os.remove(plotFileName)
    del(plotFileName)
    
    return

