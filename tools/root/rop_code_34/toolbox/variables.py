#! /usr/bin/env python
#  Module with functions related to storage of variables.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def inputargs(argList):
    '''Checks given arguments and returns information from them.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Check number of arguments:
    if len(argList) not in [5, 8, 11]:
        raise Exception('Provide 4 or 7 or 10 arguments: doRop (0 or 1), cellTemplate, auxinTemplate, varType (optionally discretization (3 integers) and initial values (3 floats)).')
    
    # Extract information about ROP simulation and templates:
    doRop = int(argList[1])
    if (doRop in [0, 1, 2, 3, 4]) == False:
        raise Exception('Given value for doRop is not valid.')
    cellTemplate = argList[2]
    auxinTemplate = argList[3]
    
    # Extract information about variations:
    varType = argList[4]
    
    # Extract information about discretization:
    if len(argList) in [8, 11]:
        xyzNumComp = map(int, [argList[5], argList[6], argList[7]])
    else:
        xyzNumComp = []
    
    # Extract information about initial conditions:
    if len(argList) == 11:
        initCond = map(float, [argList[8], argList[9], argList[10]])
    else:
        initCond = []
    del(argList)
    
    return [doRop, cellTemplate, auxinTemplate, varType, xyzNumComp, initCond]

################################################################################

def templinfo(baseName, vNrStr, cellTemplate, auxinTemplate, appTemplate, \
    xyzNumComp, initCond, consSys, doRop):
    '''Returns dictionary with information about template given in arguments.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Save list of arguments as dictionary with new name:
    infoDict = locals()
    
    return infoDict

################################################################################

def colind(fileType):
    '''List column indices for the .init/.final or .data or .model files.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    # List with column names:
    commonList = ['x', 'y', 'z', 'V', 'auxin', 'ROPactive', 'ROPinactive' \
        , 'auxinIn', 'auxinOut', 'auxinProd', 'cellMarker', 'wallMarker' \
        , 'cellLabel', 'sumMass', 'sumMassAll', 'sumMassTempl']
    if fileType in ['.init', '.final', '.model']:
        colList = commonList
    elif fileType == '.data':
        colList = ['timeind', 'timeval', 'complabel', 'numneigh'] + commonList
        commonList = [entry + '_deriv' for entry in commonList]
        colList.extend(commonList)
    else:
        raise Exception('Not implemented for file type ' + fileType + '.')
    del(commonList, fileType)
    
    # Make dictionary with column numbers:
    colIndDict = {}
    for colInd in range(0, len(colList)):
        # Add current column to dictionary:
        colIndDict.update({colList[colInd]:colInd})
    del(colInd)
    
    return colIndDict, colList

################################################################################

def checkregion(xPos, yPos, xBeg, xEnd, yBeg, yEnd, eTol = 10**-6):
    '''Check if a given point is in a given region.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Check (xPos >= xBeg) avoiding numerical problems:
    checkX1 = (xPos - xBeg >= 0) or (xBeg - xPos <= eTol)
    
    # Check (xPos <= xEnd) avoiding numerical problems:
    checkX2 = (xEnd - xPos >= 0) or (xPos - xEnd <= eTol)
    
    # Check (yPos >= yBeg) avoiding numerical problems:
    checkY1 = (yPos - yBeg >= 0) or (yBeg - yPos <= eTol)
    
    # Check (yPos <= yEnd) avoiding numerical problems:
    checkY2 = (yEnd - yPos >= 0) or (yPos - yEnd <= eTol)
    
    del(xBeg, xEnd, yBeg, yEnd, xPos, yPos)
    
    # Check if position is within region, i.e. all checks are True:
    if checkX1 and checkX2 and checkY1 and checkY2:
        check = True
    else:
        check = False
    del(checkX1, checkX2, checkY1, checkY2)
    
    return check

################################################################################

def findrelloc(xCurr, xNeigh, yCurr, yNeigh):
    '''Find relative location of one point with regard to another.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Default for the case that neither x nor y are the same:
    neighDir = 'none'
    
    # Comparison of y values when x values are the same:
    if xCurr == xNeigh:
        if yCurr < yNeigh: # Neighbour towards stele.
            neighDir = 'epid'
        elif yCurr > yNeigh: # Neighbour towards epidermis.
            neighDir = 'stele'
    
    # Comparison of x values when y values are the same:
    if yCurr == yNeigh:
        if xCurr < xNeigh: # Neighbour towards shoot.
            neighDir = 'shoot'
        elif xCurr > xNeigh: # Neighbour towards root.
            neighDir = 'root'
    
    return neighDir

################################################################################

def partext(baseName, parDict):
    '''Save list of parameter values as text.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    # Collect parameters in text form:
    text = ''
    for parName in parDict.keys():
        parVal = str(parDict[parName][1])
        text = text + parName + '\t' + parVal + '\n'
    del(parName, parVal, parDict)
    
    # Save text string to file:
    fileName = baseName + '.par'
    fileObj = open(fileName, 'w')
    fileObj.write(text)
    fileObj.close()
    print('Saved file ' + fileName)
    del(fileName, fileObj, text)
    
    return

################################################################################


