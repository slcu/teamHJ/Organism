#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def gnuplot(baseName, pathList, projName, countName, fileType):
    '''Make plots of total substance for comparison of different situations.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import subprocess, os, numpy
    from toolbox import dirnavi, variables
    
    # Program control:
    scaleType = 'lin' # Choose 'lin' or 'log'.
    
    # Get code version number:
    vNrStr = dirnavi.codeversion()

    # Get column indices for .final file:
    [colIndDict, colList] = variables.colind('.final')
    del(colList)

    # Change directory:
    os.chdir('../../')

    # Initialize list for totals and legend entries:
    totalList = []
    legStrList = []

    # Loop over paths to auxin simulations:
    numVar = len(pathList[0])
    for pInd in range(0, numVar):
        currPath = pathList[0][pInd]

        # Make legend entry:
        legEntry = currPath[currPath.find('/')+1:]
        legEntry = legEntry[:legEntry.find('/')]
        legStrList.append(legEntry)
        del(legEntry)
        
        # Change directory:
        os.chdir(currPath)
        del(currPath)
        
        # Get .final data as array:
        finalArray = numpy.loadtxt(baseName + '.final', skiprows=1)
        
        # Extract columns with total auxin mass in whole template:
        sumMassTotal = finalArray[:, colIndDict['sumMassTempl']]
        del(finalArray)
        totalVal = list(set(sumMassTotal))
        del(sumMassTotal)
        if len(totalVal) == 1:
            totalVal = totalVal[0]
        else:
            raise Exception('Problem with extraction of total value.')
        totalList.append(totalVal)
        del(totalVal)

        # Change directory:
        os.chdir('../../../../..')

    del(colIndDict, pInd)

    # Check data:
    if len(totalList) != numVar:
        raise Exception('List with average auxin has wrong length.')
    if len(legStrList) != numVar:
        raise Exception('Legend list has wrong length.')

    # Change directory:
    os.chdir('results' + vNrStr + '/')
    os.chdir(projName)
    del(vNrStr)

    # Save list as text in .plot file:
    plotName = countName + '_total_final_auxin.plot'
    numpy.savetxt(plotName, totalList)
    print("Saved file " + plotName)
    del(totalList)

    # Settings for plot:
    plotName = countName + '_total_final_auxin.plot'
    if fileType == '.eps':
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 26 cm, 8 cm'
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 20 cm, 6 cm'
    elif fileType == '.png':
        terminalStr = 'set term pngcairo enhanced color ' \
                      'size 26 cm, 5 cm'
    if scaleType == 'lin':
        scaleText = '\n### Plot with linear scale ###\n'
    elif scaleType == 'log':
        scaleText = '\n### Plot with logarithmic scale ###\n\n' \
                    + 'set logscale y\n'
    else:
        raise Exception("Set scaleType to 'lin' or 'log'.")
    del(scaleType)

    # Set x range:
    xAxisBeg = -0.5
    xAxisEnd = numVar - 0.5
    del(numVar)

    # Make .gnplt file:
    text = '''# File for plotting total mass of auxin in whole template using gnuplot.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

# Specify input data:
dataFileIn = "''' + plotName + '"\n\n' + terminalStr + '''\n
dataFileOut = "''' + plotName[:-5] + fileType + '''"
set output dataFileOut # Set name of output file.

# Settings:
set key horizontal outside center top # Settings for legend.
set size ratio 0.2 #  Set ratio of plot height to width.
set ytics out mirror # Put y tic marks on both sides.
set xl "variation"
set yl "total auxin mass [pg]"
set xrange [''' + str(xAxisBeg) + ':' + str(xAxisEnd) + ''']
'''
    del(fileType, terminalStr)
    
    # Set scaling of plot:
    text = text + scaleText
    del(scaleText)

    # Plot:
    text = text + '''
# Total mass:
plot dataFileIn u 1 title "" w lp lt 1 
pause 1
'''
    del(xAxisBeg, xAxisEnd, legStrList)

    # Save .gnplt file:
    gnpltName = plotName[:-5] + '.gnplt'
    fileObj = open(gnpltName, 'w')
    fileObj.write(text)
    fileObj.close()
    print('Saved file ' + gnpltName)
    del(text, fileObj)
    
    # Execute .gnplt file:
    subprocess.call(['gnuplot', gnpltName])
    print('Saved result of ' + gnpltName)
    
    # Delete .plot and .gnplt files:
    os.remove(plotName)
    os.remove(gnpltName)
    del(plotName, gnpltName)

    return
