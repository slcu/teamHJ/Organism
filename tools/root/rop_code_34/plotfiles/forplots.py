#! /usr/bin/env python
# Module with small functions related to plotting.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def firstlines(initFileObj, neighFileObj):
    '''Gets info about cell template from first lines in .init and .neigh files.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013-14).'''
    
    # From .init file:
    line = initFileObj.readline() # Read the first line.
    lineElem = line.split(' ') # Get elements that were separated by whitespaces.
    if len(lineElem) == 2: # Line should have 2 entries.
        numComp = int(lineElem[0]) # Number of compartments, i.e. rows.
    else:
        raise Exception('First line in .init file has wrong format.')
    del(line, lineElem)
    
    # From .neigh file:
    line = neighFileObj.readline() # Read the first line.
    lineElem = line.split(' ') # Get elements that were separated by whitespaces.
    if len(lineElem) == 2: # Line should have 2 entries.
        if numComp == int(lineElem[0]): # Should agree with that from .init.
            print('Number of compartments: ' + str(numComp))
        else:
            raise Exception('Number of compartments differs in .init and .neigh.')
    else:
        raise Exception('First line in .neigh file has wrong format.')
    del(line, lineElem)
    
    return numComp

################################################################################

def ranges(parDict):
    '''Returns x and y ranges for plotting.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    # Parameter values:
    steleRad = parDict['steleRad'][1]
    wallThick = parDict['wallThick'][1]
    xLen = parDict['xLengths'][1]
    yLen = parDict['yLengths'][1]
    del(parDict)
    
    # Calculate x range:
    xBeg = -wallThick/2
    xLenTotal = sum(xLen) + len(xLen)*wallThick
    xEnd = xLenTotal + wallThick/2
    del(xLen, xLenTotal)
    
    # Calculate y range:
    yBeg, yEnd = steleRad, steleRad # Offset due to stele in centre of root.
    yBeg += -wallThick/2
    del(steleRad)
    yLenTotal = sum(yLen) + len(yLen)*wallThick
    yEnd += yLenTotal + wallThick/2
    del(wallThick, yLenTotal, yLen)
    
    return xBeg, xEnd, yBeg, yEnd

################################################################################

def colind(colList, offSet):
    '''Returns dictionary with column indices for the given list.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    # Make dictionary with column numbers:
    colIndDict = {}
    for colInd in range(0, len(colList)):
        # Add current column to dictionary:
        colIndDict[colList[colInd]] = colInd + offSet
    del(colInd, colList, offSet)
    
    return colIndDict

################################################################################

def getsections(baseName, parDict, pickleName, subst):
    '''Adds x and y values for which tissue sections should be plotted to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013-14).'''
    
    import pickle, os
    from toolbox import variables
    
    # Get parameter values:
    xNumComp = parDict['xNumComp'][1]
    yNumComp = parDict['yNumComp'][1]
    plotSecX = parDict['plotSecX'][1]
    plotSecY = parDict['plotSecY'][1]
    
    # Check if chosen sections are unique:
    if xNumComp == 1:
        if ('firstComp' in plotSecX) and ('lastComp' in plotSecX):
            raise Exception('Only one comp in x direction, adjust plotSecX.')
    if yNumComp == 1:
        if ('firstComp' in plotSecY) and ('lastComp' in plotSecY):
            raise Exception('Only one comp in y direction, adjust plotSecY.')
    
    # Unpickle lists of compartment sizes and positions:
    fileObj = open(pickleName, 'r')
    [xCompSizeList, yCompSizeList, zCompSizeList, xPosList, yPosList, \
        zPosList] = pickle.load(fileObj)
    fileObj.close()
    del(pickleName, fileObj)
    
    # Determine y values for plots of sections through the tissue:
    plotY = [] # Initialize list for y values for section plots.
    if 'firstWall' in plotSecY: # and subst == 'auxin':
        plotY.append(yPosList[0])
    if 'lastWall' in plotSecY: # and subst == 'auxin':
        plotY.append(yPosList[-1])
    if 'firstComp' in plotSecY:
        ind = 1 # Index of first compartment in first cell.
        if len(yPosList) == 1:
            plotY.append(yPosList[0])
        while ind < len(yPosList):
            plotY.append(yPosList[ind])
            ind += yNumComp + 1 # Go to index of first comp in next cell.
        del(ind)
    if 'lastComp' in plotSecY:
        ind = yNumComp # Index of last compartment in first cell.
        while ind < len(yPosList):
            plotY.append(yPosList[ind])
            ind += yNumComp + 1 # Go to index of last comp in next cell.
        del(ind)
    del(plotSecY, yNumComp)
    plotY.sort()
    plotY.reverse()
    parDict['plotY'] = ["sections in y for plots", plotY]
    del(plotY)
    
    # Determine x values for plots of sections through the tissue:
    plotX = [] # Initialize list for x values for section plots.
    if 'firstWall' in plotSecX:
        plotX.append(xPosList[0])
    if 'lastWall' in plotSecX:
        plotX.append(xPosList[-1])
    if 'firstComp' in plotSecX:
        ind = 1 # Index of first compartment in first cell.
        while ind < len(xPosList):
            plotX.append(xPosList[ind])
            ind += xNumComp + 1 # Go to index of first comp in next cell.
        del(ind)
    if 'lastComp' in plotSecX:
        ind = xNumComp # Index of last compartment in first cell.
        while ind < len(xPosList):
            plotX.append(xPosList[ind])
            ind += xNumComp + 1 # Go to index of last comp in next cell.
        del(ind)
    del(plotSecX, xNumComp)
    plotX.sort()
    parDict['plotX'] = ["sections in x for plots", plotX]
    del(plotX)
    
    return

################################################################################

def layernames(parDict, nameType):
    '''Returns names of tissue layers for plot legends.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Parameters:
    plotY = parDict['plotY'][1]
    numLayers = len(parDict['yLengths'][1])
    
    # Set names of layers depending on number of layers:
    if numLayers == len(plotY):
        if len(plotY) == 7:
            if nameType == 'short':
                descLegendList = ['epid', 'cort', 'endo', 'peri', \
                                  'prot', 'steleo', 'stelei']
            elif nameType == 'long':
                descLegendList = ['epidermis', 'cortex', 'endodermis', \
                'pericycle', 'protophloem', 'stele (outer)', 'stele (inner)']
        elif len(plotY) == 2:
            if nameType == 'short':
                descLegendList = ['epid', 'inner']
            elif nameType == 'long':
                descLegendList = ['epidermis', 'inner tissues']
        elif len(plotY) == 1:
            if nameType == 'short':
                descLegendList = ['epid']
            elif nameType == 'long':
                descLegendList = ['epidermis']
        else:
            raise Exception('Layer names not implemented for given situation.')
    elif numLayers == 1:
        if len(plotY) == 3:
            descLegendList = ['outer wall', 'epidermis', 'inner wall']
        else:
            raise Exception('Layer names not implemented for given situation.')
    del(nameType)
    
    # Set correct order of layer names:
    ascPlotY = sorted(plotY) # Sort in ascending order.
    descPlotY = sorted(plotY, reverse=True) # Sort in descending order.
    if plotY == ascPlotY:
        # Reverse order of entries in list for legend:
        legendList = []
        for ind in range(1, len(descLegendList)+1):
            legendList.append(descLegendList[-ind])
    elif plotY == descPlotY:
        # Keep list for legend:
        legendList = descLegendList
    else:
        raise Exception('Problem with sorted layer names.')
    del(ascPlotY, descPlotY, plotY, descLegendList)
    
    return legendList

################################################################################

def walls(coordDir, endCoord, parDict):
    '''Returns coordinates of walls in chosen direction.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013-14).'''
    
    # Loop over cells in chosen direction:
    wallList = [0.0]
    for currLen in parDict[coordDir + 'Lengths'][1]:
        nextCoord = wallList[-1] + currLen + parDict['wallThick'][1]
        if nextCoord <= endCoord:
            wallList = wallList + [nextCoord]
        else:
            break # Leave this loop.
    del(currLen, nextCoord, coordDir)
    
    return wallList

################################################################################

def walltext(xyWallList):
    '''Returns text for plotting of walls as lines.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013-14).'''
    
    # Plotting of cell walls as lines:
    wallText = ''
    for xyWall in xyWallList:
        wallStyle = 'lt 3 lc rgb "grey"'
        wallText = wallText + 'set arrow from ' \
                 + str(xyWall) + ',graph 0 to ' \
                 + str(xyWall) + ',graph 1 nohead ' + wallStyle + '\n'
        del(wallStyle)
    del(xyWall, xyWallList)
    
    return wallText

################################################################################

