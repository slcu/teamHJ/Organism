#! /usr/bin/env python
# Module with functions related to settings for plotting.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def visual(parDict):
    '''Adds settings for visualization of template/results to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Get parameter values:
    xCellLen = parDict['xLengths'][1]
    wallThick = parDict['wallThick'][1]
    
    # Calculate total length of cell template:
    xLenTotal = sum(xCellLen) + len(xCellLen)*wallThick
    del(xCellLen, wallThick)
    
    # Set list with zoom values for plots:
    zoomList = [xLenTotal] # Always include total length.
    #zoomList = [xLenTotal, xLenTotal/5.0] # Always include total length.
    del(xLenTotal)
    zoomList.sort() # Sort in increasing order.
    zoomList.reverse() # Reverse order, i.e. sort in decreasing order.
    
    # Set list with time points for movies:
    timeList = [1, 3, 6, 9]
    
    # Add lists to dictionary:
    parDict['zoomList'] = ["zoom values for plotting", zoomList]
    parDict['timeList'] = ["time values for plotting", timeList]
    
    # Sections for which concentration curves will be plotted:
    plotSecY = ['firstComp'] # firstComp, lastComp, firstWall, lastWall
    #plotSecY = ['firstWall', 'firstComp', 'lastWall'] # firstComp, lastComp, firstWall, lastWall
    plotSecX = []
    
    # Add to dictionary:
    parDict['plotSecX'] = ["sections in x dir for plots", plotSecX]
    parDict['plotSecY'] = ["sections in y dir for plots", plotSecY]
    del(plotSecX, plotSecY)
    
    return

################################################################################
