#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def gnuplot(baseName, pathList, projName, countName, fileType):
    '''Make plots of mean concentration for comparison of different situations.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import subprocess, os
    import numpy as np
    from toolbox import dirnavi, variables
    
    # Program control:
    scaleType = 'lin' # Choose 'lin' or 'log'.
    
    # Set x value above which mean will be calculated from auxin:
    #xThres = 5*10.0**-3
    xThres = 0.0
    
    # Get code version number:
    vNrStr = dirnavi.codeversion()
    
    # Get column indices for .final file:
    [colIndDict, colList] = variables.colind('.final')
    del(colList)
    
    # Change directory:
    os.chdir('../../')
    
    # Initialize list for means:
    meanList = []
    
    # Loop over paths to auxin simulations:
    numVar = len(pathList[0])
    for pInd in range(0, numVar):
        currPath = pathList[0][pInd]
        
        # Change directory:
        os.chdir(currPath)
        
        # Get .final data as array:
        finalArray = np.loadtxt(baseName + '.final', skiprows=1)
        
        # Extract columns with auxin concentration, x coord and markers:
        auxinConc = finalArray[:, colIndDict['auxin']]
        xCoord = finalArray[:, colIndDict['x']]
        cellMarker = finalArray[:, colIndDict['cellMarker']]
        sourceMarker = finalArray[:, colIndDict['auxinIn']]
        sinkMarker = finalArray[:, colIndDict['auxinOut']]
        del(finalArray)
        cellMarker = cellMarker.tolist()
        sourceMarker = sourceMarker.tolist()
        sinkMarker = sinkMarker.tolist()
        
        # Extract auxin concentration only from normal cell compartments:
        relAuxinConc = []
        for ind in range(0, len(cellMarker)):
            if cellMarker[ind] == 1 and sourceMarker[ind] == 0 \
            and sinkMarker[ind] == 0:
                if 'single' in currPath or '7layersS' in currPath:
                    relAuxinConc.append(auxinConc[ind])
                elif 'file' in currPath or 'epidermis' in currPath:
                    if xCoord[ind] > xThres:
                        relAuxinConc.append(auxinConc[ind])
                else:
                    raise Exception('Case not implemented yet.')
        del(currPath, auxinConc, cellMarker, sourceMarker, sinkMarker)
        
        # Calculate and save mean auxin concentration:
        meanVal = np.mean(relAuxinConc)
        del(relAuxinConc)
        meanList.append(meanVal)
        del(meanVal)

        # Change directory:
        os.chdir('../../../../..')

    del(colIndDict, pInd)
    
    # Check data:
    if len(meanList) != numVar:
        raise Exception('List with average auxin has wrong length.')
    
    # Change directory:
    os.chdir('results' + vNrStr + '/')
    os.chdir(projName)
    del(vNrStr)

    # Save list as text in .plot file:
    plotName = countName + '_mean_final_auxin.plot'
    np.savetxt(plotName, meanList)
    print("Saved file " + plotName)
    del(meanList)

    # Settings for plot:
    if fileType == '.eps':
        #terminalStr = 'set term post enhanced color ' \
        #              'portrait size 26 cm, 8 cm'
        #terminalStr = 'set term post enhanced color ' \
        #              'portrait size 20 cm, 6 cm'
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 20 cm, 8 cm linewidth 1.2'
    elif fileType == '.png':
        terminalStr = 'set term pngcairo enhanced color ' \
                      'size 26 cm, 5 cm'
    if scaleType == 'lin':
        scaleText = '\n### Plot with linear scale ###\n'
    elif scaleType == 'log':
        scaleText = '\n### Plot with logarithmic scale ###\n\n' \
                    + 'set logscale y\n'
    else:
        raise Exception("Set scaleType to 'lin' or 'log'.")
    del(scaleType)

    # Set x and y range:
    xAxisBeg = -0.5
    xAxisEnd = numVar - 0.5
    del(numVar)
    yAxisBeg = 0.001
    yAxisEnd = 0.1
    
    # Make .gnplt file:
    text = '''# File for plotting mean concentration of auxin in whole template using gnuplot.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

# Specify input data:
dataFileIn = "''' + plotName + '"\n\n' + terminalStr + '''\n
dataFileOut = "''' + plotName[:-5] + fileType + '''"
set output dataFileOut # Set name of output file.

# Settings:
set key horizontal outside center top # Settings for legend.
set size ratio 0.25 #  Set ratio of plot height to width.
set ytics out mirror # Put y tic marks on both sides.
set grid ytics
set xl "variation"
set yl "mean auxin conc [umol/dm^3]"
set xrange [''' + str(xAxisBeg) + ':' + str(xAxisEnd) + ''']
#set yrange [''' + str(yAxisBeg) + ':' + str(yAxisEnd) + ''']
'''
    del(fileType, terminalStr)
    
    # Set scaling of plot:
    text = text + scaleText
    del(scaleText)

    # Plot:
    text = text + '''
# Mean mass:
plot dataFileIn u 1 title "" w lp lt 1 
pause 1
'''
    del(xAxisBeg, xAxisEnd, yAxisBeg, yAxisEnd)

    # Save .gnplt file:
    gnpltName = plotName[:-5] + '.gnplt'
    fileObj = open(gnpltName, 'w')
    fileObj.write(text)
    fileObj.close()
    print('Saved file ' + gnpltName)
    del(text, fileObj)
    
    # Execute .gnplt file:
    subprocess.call(['gnuplot', gnpltName])
    print('Saved result of ' + gnpltName)
    
    # Delete .plot and .gnplt files:
    #os.remove(plotName)
    #os.remove(gnpltName)
    del(plotName, gnpltName)

    return
