#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# CHOOSE WHICH RUN OF ALL PROJECTS SHOULD BE USED:
resNum = 'last'

# CHOOSE PROJECT TYPE:
projType = 'parvar'

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE CELL AND AUXIN TEMPLATE:
cellTemplate = '7layers5'
auxinTemplate = '7layers1N50C10E10'
#cellTemplate = 'epidermis1'
#auxinTemplate = 'epidermis1'

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

# CHOOSE DISCRETIZATION:
# Number of compartments in x, y, z direction per cell.
xyzNumComp = [2, 1, 1]

################################################################################

# Print user input for checking:
print(baseName, consSys, projType, resNum)
raw_input('Check settings and press any key to continue:')

# Import necessary modules:
import os, pickle, copy
import numpy as np
from toolbox import dirnavi

# Get code version number:
vNrStr = dirnavi.codeversion()

# Change directory:
os.chdir('../../results' + vNrStr + '/')

# Get list of relevant directories:
listDir = os.listdir(os.getcwd())
if 'default' in listDir:
    relListDir = ['default']
else:
    relListDir = []
for currDir in listDir:
    # Add curr directory if it contains the project type and exactly one underscore:
    if projType in currDir and len(currDir.split('_')) == 2:
        relListDir.append(currDir)
del(currDir, listDir)

# Initialize collection (list of lists) for all effects:
effectColl = [['variation', '1st level effect', '2nd level effect' \
            , '1st slope effect', '2nd slope effect']]

# Loop over relevant directories:
for currProjName in relListDir:
    #print(currProjName)
    currVarName = currProjName[currProjName.find('_')+1:]
    #print(currVarName)
    
    # Change directory:
    os.chdir(currProjName)
    
    # Check how many runs of current project have been done:
    maxNum = dirnavi.numfiles('.txt', currProjName)
    
    # Set name for chosen run of current project:
    if resNum == 'last':
        currResNum = maxNum
    elif resNum > maxNum:
        currResNum = maxNum
        print('Chosen number is too high, will take last result.')
    else:
        currResNum = resNum
    if 'default' == currProjName:
        countName = cellTemplate + '_' + auxinTemplate + appTemplate + '_' \
            + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
            + str(xyzNumComp[2]) + '_default_' + str(currResNum)
    elif 'parvar' in currProjName:
        countName = cellTemplate + '_' + auxinTemplate + appTemplate + '_' \
            + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
            + str(xyzNumComp[2]) + '_' + currProjName + '_' + str(currResNum)
    elif 'vartempl_lateral' in currProjName:
        countName = cellTemplate + '_auxinTemplates' + appTemplate + '_' \
            + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
            + str(xyzNumComp[2]) + '_'  + currProjName + '_' + str(currResNum)
    del(currResNum)
    
    # Check if table with effect of variations on gradient level and slope exists:
    tabName = countName + '_tabpervar_auxin.plot'
    if os.path.exists(tabName) == False:
        # Change directory and jump to next iteration:
        os.chdir('..')
        continue
    elif os.path.exists(tabName):
        
        # Load table as array:
        tabPerVar = np.loadtxt(tabName)
        tabSize = np.shape(tabPerVar)
        
        if 'parvar' in currProjName:
            # Make sure that parameter values are in increasing order:
            if list(tabPerVar[:, 0]) != sorted(tabPerVar[:, 0]):
                raise Exception('Parameter values are not increasing.')
        
        # Add columns with levels of two gradient segments:
        newCol = np.array((tabPerVar[:, 1] + tabPerVar[:, 2])/2.0)
        newCol = newCol.reshape((tabSize[0], 1))
        tabPerVar = np.append(tabPerVar, newCol, 1) 
        del(newCol)
        newCol = np.array((tabPerVar[:, 2] + tabPerVar[:, 3])/2.0)
        newCol = newCol.reshape((tabSize[0], 1)) 
        tabPerVar = np.append(tabPerVar, newCol, 1) 
        del(newCol)
        
        # Add columns with slopes of two gradient segments:
        newCol = np.array(tabPerVar[:, 2] - tabPerVar[:, 1])
        newCol = newCol.reshape((tabSize[0], 1))
        tabPerVar = np.append(tabPerVar, newCol, 1) 
        del(newCol)
        newCol = np.array(tabPerVar[:, 3] - tabPerVar[:, 2])
        newCol = newCol.reshape((tabSize[0], 1)) 
        tabPerVar = np.append(tabPerVar, newCol, 1) 
        del(newCol)
        #print(tabPerVar)
        
        # Initialize lists for effects of current variation:
        levelEffect = []
        slopeEffect = []
        
        # Loop over level and slope:
        for lasShift in [0, 2]:
            
            # Loop over two gradient segments:
            for gsShift in [0, 1]:
                
                # Set appropriate column index for table:
                currCol = tabSize[1] + lasShift + gsShift
                
                # Initialize lists for effects of current variation and segment:
                currLevelEffect = []
                currSlopeEffect = []
                
                # Loop over pairs of rows in table, e.g. parameter values:
                for rInd in range(0, tabSize[0] - 1):
                    
                    # Check current effect for current gradient segment:
                    if tabPerVar[rInd+1, currCol] > tabPerVar[rInd, currCol]:
                        if lasShift == 0:
                            currLevelEffect.append('higher')
                        elif lasShift == 2:
                            currSlopeEffect.append('steeper')
                    elif tabPerVar[rInd+1, currCol] < tabPerVar[rInd, currCol]:
                        if lasShift == 0:
                            currLevelEffect.append('lower')
                        elif lasShift == 2:
                            currSlopeEffect.append('flatter')
                
                # Check if effects of curr var and segment are always the same:
                currLevelEffect = list(set(currLevelEffect))
                if len(currLevelEffect) > 1:
                    currLevelEffect = ['unclear']
                currSlopeEffect = list(set(currSlopeEffect))
                if len(currSlopeEffect) > 1:
                    currSlopeEffect = ['unclear']
                
                # Add effects for current variation and segment to lists:
                if lasShift == 0:
                    levelEffect.extend(currLevelEffect)
                    if currSlopeEffect != []:
                        raise Exception('Something wrong with finding effects.')
                elif lasShift == 2:
                    slopeEffect.extend(currSlopeEffect)
                    if currLevelEffect != []:
                        raise Exception('Something wrong with finding effects.')
                del(currLevelEffect, currSlopeEffect)
                del(currCol)
            
            del(gsShift)
        del(lasShift)
        
        # Collect information on effect of current variation:
        currEffect = [currVarName]
        currEffect.extend(levelEffect)
        currEffect.extend(slopeEffect)
        effectColl.append(currEffect)
        del(currEffect)
        
    # Change directory:
    os.chdir('..')
    
print(effectColl)

# CONTINUE HERE

del(projType)


