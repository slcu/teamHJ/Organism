#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.
xCompSize = 0.5 # Compartment size in x direction for varType constcomp (int).

# CHOOSE SECTIONS FOR WHICH CONCENTRATION CURVES WILL BE PLOTTED:
plotSecY = ['firstComp'] # firstComp, lastComp, firstWall, lastWall
plotSecX = []

# CHOOSE IF PREVIOUSLY STARTED SIMULATION SHOULD BE CONTINUED OR SKIPPED:   
#simStrategy = 'continue'
simStrategy = 'skip'

# CHOOSE IF .data FILE SHOULD BE DELETED TO SAVE DISK SPACE:
doKeepDataFile = 0

# CHOOSE IF CONCENTRATIONS SHOULD BE SAVED AFTER EACH RUN:
doKeepConc = 1

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import time, os, pickle, copy, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings, varsettings
from execsim import auxinsim, ropsim, ropsimpercell
from execplot import simplots

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Construct project name:
projName = os.path.basename(__file__)
projName = projName[4:-3]

# Get default discretization and initial conditons:
[xyzNumCompDef, initCondDef] = simsettings.defaults()

# Get information from input arguments:
argList = sys.argv
[doRop, cellTemplate, auxinTemplate, varType, xyzNumComp, initCond] = variables.inputargs(argList)
if varType in ['length', 'constcomp']:
    if 'single' not in cellTemplate:
        raise Exception('Use single cell template here.')
    if cellTemplate[cellTemplate.find('single') + 6] in ['X', 'L']:
        cellTempl = cellTemplate[:7]
    else:
        raise Exception('Use single cell template here.')
    del(cellTemplate)
    auxinTemplList = [auxinTemplate]
elif varType == 'lateral':
    if auxinTemplate not in ['singleE', 'fileE', 'epidermisE']:
        raise Exception("Provide one of the following auxin templates: " \
        + "'single', 'file', 'epidermis'.")
    cellTemplList = [cellTemplate]
if xyzNumComp == []:
    xyzNumComp = map(int, xyzNumCompDef)
del(xyzNumCompDef)
if initCond == []:
    initCond = map(float, initCondDef)
del(initCondDef)

# SET CELL AND AUXIN TEMPLATE:
if varType in ['length', 'constcomp']:
    cellTemplList = []
    for lengthVal in range(20, 160, 10):
    #for lengthVal in [10, 20, 50, 100, 150]:
    #for lengthVal in [20, 30, 40, 50, 60]: # relevant for mutants
        cellTemplList.append(cellTempl + str(lengthVal))
    del(lengthVal)
elif varType == 'lateral':
    if 'single' in cellTemplate:
        latStrList = map(str, range(0, 60, 10))
    elif 'file' in cellTemplate or 'epidermis' in cellTemplate:
        latStrList = map(str, range(0, 30, 10))
    elif '7layersS' in cellTemplate:
        latStrList = map(str, [0, 1, 5, 10])
    else:
        raise Exception('Implement list of lateral PIN strengths for chosen template.')
    auxinTemplList = []
    for latStr in latStrList:
        auxinTemplList.append(auxinTemplate + latStr)
elif varType == 'optim':
    cellTemplList = ['2layers1', '2layers2'] 
    auxinTemplList = ['2layers1lat1']
elif varType in ['lateralN', 'lateralNCE']:
    cellTemplList = ['7layers5']
    auxinTemplPart = '7layers1'
    if varType == 'lateralN':
        #endoList = range(0, 50, 10)
        endoList = range(0, 20, 10)
        cortEpidList = [0]
    elif varType == 'lateralNCE':
        endoList = [50]
        cortEpidList = range(0, 30, 10)
    auxinTemplList = []
    for endoVal in endoList:
        auxinPart = auxinTemplPart + 'N' + str(endoVal)
        del(endoVal)
        for cortEpidVal in cortEpidList:
            auxinStr = auxinPart + 'C' + str(cortEpidVal) + 'E' + str(cortEpidVal)
            del(cortEpidVal)
            auxinTemplList.append(auxinStr)
            del(auxinStr)
        del(auxinPart)
    del(auxinTemplPart, endoList, cortEpidList)
elif varType == 'mutant':
    cellTemplList = ['7layers5']
    auxinTemplList = ['7layers3lat1', '7layers3lat1aux1m' \
                       , '7layers3lat1pin2m', '7layers3lat1gnom']
elif varType == 'weak':
    cellTemplList = ['7layers5']
    auxinTemplList = ['7layers3lat1', '7layers3lat1weak1']
elif varType == 'ES':
    cellTemplList = ['7layers2L100', '7layers2L45', '7layers2L82']
    auxinTemplList = ['7layers1N35C10E10']
else:
    raise Exception('Unknown variation for comparison of templates given.')
appTemplate = ''

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplList, auxinTemplList, appTemplate, xyzNumComp)
raw_input('Check settings and press any key to continue:')

# Start timer:
startTime = time.time()

# Get code version number:
vNrStr = dirnavi.codeversion()

# Change directory:
os.chdir(outPath)

# Loop over cell templates to be compared:
pathList = [[], []] # Initialize list for paths to simulation directories.
for cellTemplate in cellTemplList:
    #print(cellTemplate)
    
    for auxinTemplate in auxinTemplList:
        #print(auxinTemplate)
        #raw_input()
        
        # Set discretization in special case:
        if varType == 'constcomp':
            xLen = int(cellTemplate[cellTemplate.find('single') + 7:])
            xNumComp = int(xLen/xCompSize)
            xyzNumComp[0] = xNumComp # Update discretization in x direction.
            del(xLen, xNumComp)
        
        # Collect information about template into a dictionary:
        infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, \
                   auxinTemplate, appTemplate, xyzNumComp, initCond, consSys)
        
        # Get parameter values:
        parDict = parvalues.setallpar(infoDict)
        
        # Update parDict regarding tolerance for stopping:
        simsettings.tolerance(parDict, infoDict['cellTemplate'])
        
        # Add plot settings to parDict:
        parDict['plotSecX'] = ["sections in x dir for plots", plotSecX]
        parDict['plotSecY'] = ["sections in y dir for plots", plotSecY]
        
        # Set names of directories for different levels:
        levelList = dirnavi.dirlevel(infoDict, parDict)
        del(infoDict)
        
        # Change directory:
        dirnavi.mkchdir(levelList[0])
        dirnavi.mkchdir(levelList[1])
        dirnavi.mkchdir(levelList[2])
        
        # Check if pickled parDict from template exists (as example):
        pickleName = 'template/' + baseName + '_parDict.pkl'
        if os.path.exists(pickleName) == False:
            print(cellTemplate, xyzNumComp)
            raise Exception('No variable with parameters found, make template first.')
        
        # Check for existing auxin simulation directory (levels 3 and 4):
        doSim = dirnavi.skipdir('auxin', levelList, simStrategy)
        
        # Change directory:
        dirnavi.mkchdir(levelList[3])
        dirnavi.mkchdir(levelList[4])
        
        if doSim == 1:
            # Pickle parDict (i.e. save as a variable):
            pickleName = baseName + '_parDict.pkl'
            fileObj = open(pickleName, 'w')
            pickle.dump(parDict, fileObj)
            fileObj.close()
            print("Saved file " + pickleName)
            del(pickleName, fileObj)
            
            # Save list of parameter values as text:
            variables.partext(baseName, parDict)
            
            # Run simulation for auxin:
            auxinsim.simulate(baseName, parDict, levelList, doKeepDataFile, doKeepConc)
        
        del(doSim)
        
        # Save path to auxin simulation directory:
        currPath = dirnavi.simpath('auxin', levelList)
        pathList[0].append(currPath)
        del(currPath)
        
        if doRop == 1:
            # Change directory:
            dirnavi.mkchdir('../' + levelList[7])
            
            # Check for existing ROP simulation directory (levels 8 and 9):
            doSim = dirnavi.skipdir('rop', levelList, simStrategy)
            
            # Change directory:
            dirnavi.mkchdir(levelList[8])
            dirnavi.mkchdir(levelList[9])
            
            if doSim == 1:
                # Pickle parDict (i.e. save as a variable):
                pickleName = baseName + '_parDict.pkl'
                fileObj = open(pickleName, 'w')
                pickle.dump(parDict, fileObj)
                fileObj.close()
                print("Saved file " + pickleName)
                del(pickleName, fileObj)

                # Save list of parameter values as text:
                variables.partext(baseName, parDict)
                
                if 'single' in levelList[1]:
                    # Run ROP simulation for complete template:
                    ropsim.simulate(baseName, parDict, levelList, doKeepDataFile, doKeepConc)
                else:
                    # Run ROP simulation per cell:
                    ropsimpercell.simulate(baseName, parDict, levelList, doKeepDataFile)
            
            del(doSim)

            # Save path to ROP simulation directory:
            currPath = dirnavi.simpath('rop', levelList)
            pathList[1].append(currPath)
            del(currPath)

            # Change directory:
            os.chdir('../../')
        
        del(levelList, parDict)
        
        # Change directory:
        os.chdir('../../../../..')
        
        del(auxinTemplate)
    del(cellTemplate)
del(plotSecX, plotSecY, xCompSize)

# Construct project name:
projName = projName + '_' + varType

# Change directory:
dirnavi.mkchdir('results' + vNrStr + '/')
dirnavi.mkchdir(projName)

# Set strings for cell and auxin template depending on project:
if varType in ['length', 'constcomp']:
    cellTemplStr = 'varCellTempl'
    auxinTemplStr = auxinTemplList[0]
elif varType == 'lateral':
    cellTemplStr = cellTemplList[0]
    auxinTemplStr = 'varAuxinTempl'
else:
    raise Exception('Implement cellTemplStr and auxinTemplStr for chosen variation.')

# Check number of previous files of given type and set name for current run:
if varType == 'constcomp':
    maxNum = dirnavi.numfiles('.txt', projName, cellTemplStr, auxinTemplStr, [])
    countName = cellTemplStr + '_' + auxinTemplStr + appTemplate + '_' \
          + projName + '_' + str(maxNum+1)
else:
    maxNum = dirnavi.numfiles('.txt', projName, cellTemplStr, auxinTemplStr, xyzNumComp)
    countName = cellTemplStr + '_' + auxinTemplStr + appTemplate + '_' \
          + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
          + str(xyzNumComp[2]) + '_' + projName + '_' + str(maxNum+1)
del(cellTemplStr, auxinTemplStr, appTemplate, xyzNumComp, consSys, maxNum, projName)

# Save list with paths to simulation directories as text:
fileName = countName + '.txt'
fileObj = open(fileName, 'w')
for simPhase in [0, 1]:
    for currPath in pathList[simPhase]:
        if currPath != '':
            fileObj.write(currPath + '\n')
fileObj.close()
print('Saved file ' + fileName)
del(fileName, fileObj, pathList)

# Calculate run time:
endTime = time.time()
runTime = endTime - startTime
del(startTime, endTime)
print('Run time was ' + str(runTime/3600) + ' hours')


