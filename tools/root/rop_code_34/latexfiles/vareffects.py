#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def tabpervar(baseName, origParDict, origLevelList, pathList, projName, countName):
    '''Make table with changes in level and slope of gradient for different situations.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import subprocess, os, numpy, pickle
    from toolbox import dirnavi
    
    # Get code version number:
    vNrStr = dirnavi.codeversion()
    
    # Change directory:
    os.chdir('../../')
    
    # Initialize array for gradient values (one row per simulation):
    numSecs = origParDict['numCuts'][1] - 1
    numVar = len(pathList[0])
    compArray = numpy.zeros(shape = (numVar, numSecs + 1))
    
    # Loop over paths to auxin simulations:
    for pInd in range(0, numVar):
        currPath = pathList[0][pInd]
        
        # Change directory:
        os.chdir(currPath)
        
        # Unpickle parDict:
        pickleName = baseName + '_parDict.pkl'
        fileObj = open(pickleName, 'r')
        parDict = pickle.load(fileObj)
        fileObj.close()
        print("Loaded file " + pickleName)
        del(pickleName, fileObj)
        
        # Make legend entry:
        if 'default' == projName:
            raise Exception('Table cannot be made for default only.')
        elif 'parvar' in projName:
            # Find name and value of varied parameter:
            parName = projName[projName.find('_') + 1:]
            compArray[pInd, 0] = str(parDict[parName][1])
            del(parName)
        else:
            # Use running number (because array can not take string):
            compArray[pInd, 0] = pInd
        del(currPath, parDict)
        
        # Get .sum data as array:
        auxinSumList = numpy.loadtxt(baseName + '.sums')
        
        # Add gradient to array:
        compArray[pInd, 1:4] = auxinSumList[0:numSecs]
        del(auxinSumList)
        
        # Change directory:
        os.chdir('../../../../..')
        
    del(pInd, origLevelList, pathList, numSecs, numVar)
    
    # Change directory:
    os.chdir('results' + vNrStr + '/' + projName)
    del(vNrStr, projName)
    
    # Save array as text in .plot file:
    plotName = countName + '_tabpervar_auxin.plot'
    numpy.savetxt(plotName, compArray)
    print("Saved file " + plotName)
    del(compArray, countName)
    
    return

