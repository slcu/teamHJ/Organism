#! /usr/bin/env python
# Script that provides the latex template for template of ROP model.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013).
#

################################################################################

def latextempl(templName, parDict):
    '''Generates the .tex file to display template for ROP model.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013).'''
    
    # Settings:
    rDig = 3 # Digits for rounding.
    
    # Get template specifics:
    parNameTempl = ['pinLoc', 'auxLoc', 'sourceLoc', 'sinkLoc', 'prodLoc']
    
    if templName.count('_1ny') == 1 and templName.count('_1nx') == 1:
        widthStr = '0.3 \\textwidth'
    else:
        widthStr = '\\textwidth'
    
    text = """\documentclass[11pt, a4paper, landscape]{article}

%%%%%% Graphics %%%%%%%
\usepackage[dvips]{epsfig} % old, should be replaced
\usepackage[font=small, width=\\textwidth]{caption}[2008/04/01]
\usepackage[labelformat=simple]{subcaption}
\\renewcommand{\\thesubfigure}{(\\alph{subfigure})} % put round brackets around subfigure label
\graphicspath{{}}

%%%%%% Layout %%%%%%%

% new page layout:
\\renewcommand{\\topfraction}{1} % 100% of page top can be a float
\\renewcommand{\\bottomfraction}{1} % 100% of page bottom...
\\renewcommand{\\textfraction}{0} % only 0% of page must be text
%\\renewcommand{\\baselinestretch}{1.2} % line spacing
\linespread{1.2}
\setlength{\headheight}{14pt}

% page margins:
%\usepackage[margin = 1.6 cm]{geometry}
\usepackage[inner = 1 cm, outer = 1 cm, top = 2 cm, bottom = 2 cm]{geometry}

%fancy header and footer:
\usepackage{fancyhdr} % for pagestyle
\pagestyle{fancy}
\\fancyhead{} % delete header
\lhead{Bettina}
\\rhead{\\today}

\\fancyfoot{} % delete footer
\\rfoot{\\thepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\\begin{document}

\section{Tissue template for ROP simulations}
"""
    
    text = text + """
Paremeter values for the tissue template:

\\vspace{0.2 cm}

\\begin{tabular}{l|l}
    par & val \\\\
    \hline
    """
    
    for parName in parNameTempl:
        if parName in ['pinLoc', 'auxLoc']:
            numE = 6
        elif parName in ['sourceLoc', 'sinkLoc', 'prodLoc']:
            numE = 5
        numB = len(parDict[parName][1])/numE
        text = text + "$" + parName + "$ & "
        currPar = parDict[parName][1]
        for ind1 in range(0, numB):
            text = text + "$["
            currParPart = currPar[ind1*numE:(ind1+1)*numE]
            for ind2 in range(len(currParPart)):
                text = text + str(currParPart[ind2]) + ", "
            del(ind2, currParPart)
            text = text[:-2] + "]$ "
            if ind1 % 2 == 1 and ind1 != numB - 1:
                text = text + "\\\\ $ $ & "
        del(currPar, ind1)
        if parName != parNameTempl[-1]:
            text = text + """\\\\
    """
    text = text + """
\end{tabular}

\\vspace{0.7 cm}
"""
    
    text = text + """
\\noindent
The following pictures show the marker values, i.e. the tissue template.

\\vspace{1cm}

\centering
"""
    # Loop over different markers to be plotted:
    markerList = ['ind', 'V', 'auxinIn', 'auxinOut', 'auxinProd', 'pinMarker' \
                  , 'auxMarker', 'cellLabel']
    for marker in markerList:
        for zoom in parDict['zoomList'][1]:
            text = text + """
    \\begin{minipage}{""" + widthStr + """}
        \subsection{Plot of template for """ + marker + """:}
        \includegraphics[height = \\textwidth, angle = -90]{""" + templName + '_' + \
    str(zoom) + 'dm_' + marker + """.eps}
    \end{minipage}
    \\vspace{1cm}"""
    
    del(marker, markerList)
    
    text = text + "\n\n\end{document}\n\n"
    
    return text


