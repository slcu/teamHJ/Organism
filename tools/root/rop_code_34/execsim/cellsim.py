#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def simpercell(baseName, parDict, levelList, doKeepDataFile):
    '''Simulate each cell separately (only for cell autonomous substances).
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013-15).'''
    
    import shutil, os
    from execsim import percell, ropsimloop
    
    # Calculate number of compartments per cell:
    numCpC = parDict['xNumComp'][1]*parDict['yNumComp'][1]*parDict['zNumComp'][1]
    
    # Calculate number of cells (excl source/sink) and comps per cell:
    if parDict['sosiType'][1] == 'tipshoot':
        numSosi = 2
    elif parDict['sosiType'][1] == 'bothshoot':
        numSosi = 1
    xNumCells = len(parDict['xLengths'][1])
    yNumCells = len(parDict['yLengths'][1])
    if parDict['zNumCells'][1] == 0: # I.e. 2.5 dimensions.
        numCells = (xNumCells - numSosi)*yNumCells
    else:
        numCells = (xNumCells - numSosi)*yNumCells \
                   *parDict['zNumCells'][1]
    del(numSosi, xNumCells)
    
    # Copy original .init file as template for combined .final file:
    shutil.copy2(baseName + '.init', baseName \
                 + '_all_' + str(numCells + 1) + '.final')
    
    # Copy pickled parameter file:
    shutil.copy2(baseName + '_parDict.pkl', baseName + '_cell_parDict.pkl')
    
    # Simulate ROPs in each cell separately:
    cellName = baseName + '_cell'
    
    # Loop over cells (reversed order):
    epidLabelList = [numCells + 1] # Label for previous epidermal cell.
    for cellLabel in range(numCells, 0, -1):
        # Use modulo (%) to find epidermis (last layer in y direction):
        if cellLabel%yNumCells == 0: # Epidermal cell.
            
            # Make .init file for current cell:
            [numRows, numCols, relLineIndList] = percell.cellinit\
                                (baseName, cellName, cellLabel, numCpC)
            
            # Make .neigh file for current cell:
            percell.cellneigh(baseName, cellName, numRows, \
                                      relLineIndList, numCpC)
            
            
            # Run rop simulation:
            simNeeded = True
            simLabel, tolCount = 0, 0
            while simNeeded == True:
                [simLabel, tolCount, simNeeded] = ropsimloop.onerun(cellName, \
                simLabel, tolCount, parDict, levelList, doKeepDataFile)
            del(simLabel, tolCount)
            
            # Add cell label to .stats file:
            os.rename(cellName + '.stats', \
                      cellName + '_' + str(cellLabel) + '.stats')
            
            # Delete .sums file:
            os.remove(cellName + '.sums')
            
            # Merge .final file of current cell with general .final file:
            percell.cellfinal(baseName, cellName, cellLabel, \
                epidLabelList[-1], numRows, numCols, numCpC, relLineIndList)
            
            # Delete .neigh and .data files for current cell:
            os.remove(cellName + '.neigh')
            
            # Update label for previous epidermal cell:
            if cellLabel == numCells:
                epidLabelList = [cellLabel] # Replace dummy.
            else:
                epidLabelList.append(cellLabel) # Add to list.
    
    del(parDict, yNumCells, numCells)
    
    # Rename last .final file:
    os.rename(baseName + '_all_' + str(epidLabelList[-1]) + '.final', \
              baseName + '.final')
    del(cellLabel)
    
    # Collect statistics into one .stats file, delete cell .stats files:
    percell.collstats(baseName, cellName, epidLabelList)
    del(epidLabelList)
    
    # Rename last pickled statistics:
    os.rename(cellName + '_stats.pkl', baseName + '_stats.pkl')
    del(cellName)
    
    # Delete pickled parameter file:
    os.remove(baseName + '_cell_parDict.pkl')
    
    return

