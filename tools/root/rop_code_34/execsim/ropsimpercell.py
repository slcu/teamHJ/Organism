#! /usr/bin/env python
# Module with one function for simulation of auxin part in the root model.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def simulate(baseName, parDict, levelList, doKeepDataFile, doKeepConc):
    '''Runs the code for the various simulation of the ROP model in organism.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    import os, pickle, shutil, copy, subprocess
    import numpy as np
    from toolbox import dirnavi
    from simfiles import shellbinfiles, modelfile
    from execsim import checksim, merge, cellsim, cellsim
    
    # Parameter values:
    timeR = parDict['timeR'][1]
    sTolR = parDict['sTolR'][1]
    sCountR = parDict['sCountR'][1]
    saveTime = parDict['saveTime'][1]
    
    # Get lists of parameter names:
    parNameDict = dirnavi.getparnames()
    parNameAuxin = parNameDict['auxin']
    parNameRop = parNameDict['rop']
    
    # Make dummy file for curr rop-related param if needed:
    os.chdir('../../')
    dirnavi.pardummy(parNameRop)
    os.chdir(levelList[8] + levelList[9])
    
    # Get list of files in simulation directory:
    fileList = os.listdir(os.getcwd())
    
    # Check if simulations are needed:
    if doKeepDataFile == 1:
        if baseName + '.sums' in fileList and baseName + '.data' in fileList:
            print('ROP simulations per cell have been done and completed, .data exists.')
            simNeeded = False
        else:
            print('ROP .data file does not exist.')
            simNeeded = True
    
    elif doKeepDataFile == 0:
        if baseName + '.sums' in fileList:
            print('ROP simulations per cell have been done and completed.')
            simNeeded = False
        elif baseName + '.final' in fileList \
        and baseName + '_cell_stats.pkl' in fileList:
            print('Some ROP simulation per cell had been started, will be done again.')
            simNeeded = True
        elif baseName + '.final' not in fileList \
        and baseName + '_stats.pkl' not in fileList:
            print('No ROP simulation per cell has been done, will be started.')
            simNeeded = True
        else:
            raise Exception('Unexpected situation, check simulation folder.')
    
    if simNeeded == True:
        # Copy simulation files from template folder:
        shutil.copy2('../../../../template/osimR.sh', 'osimR.sh')
        print('Copied osimR.sh to rop simulation directory.')
        
        # Copy simulation files from auxin simulation folder:
        fileName = '../../../' + levelList[4] + baseName
        shutil.copy2(fileName + '.final', baseName + '.init')
        shutil.copy2(fileName + '.neigh', baseName + '.neigh')
        print('Copied .final/.init and .neigh to rop simulation directory.')
        shutil.copy2(fileName + '_comp.pkl', baseName + '_comp.pkl')
        print('Copied _comp.pkl file to rop simulation directory.')
        del(fileName)
        
        # Make .model file:
        modelfile.model(baseName + '_cell', parDict, 'R')
    
    # Run rop simulation per cell:
    if simNeeded == True:
        cellsim.simpercell(baseName, parDict, levelList, doKeepDataFile)
    
    # Save empty .sums file:
    fileName = baseName + '.sums'
    fileObj = open(fileName, 'w')
    fileObj.write('')
    fileObj.close()
    print('Saved empty file ' + fileName)
    del(fileName, fileObj)
    
    return

################################################################################

