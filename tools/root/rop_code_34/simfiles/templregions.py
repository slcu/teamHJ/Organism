#! /usr/bin/env python
# Module with functions related to regions in template, i.e. PIN or production.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def columella(parDict):
    '''Adds beginning and end of columella to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    # Get parameter values:
    wallThick = parDict['wallThick'][1]
    xLen = parDict['xLengths'][1]
    sosiType = parDict['sosiType'][1]
    xCoNum = parDict['xCoNum'][1]
    
    # Calculate columella beginning in x direction:
    if sosiType == 'bothshoot':
        xCoBeg = 0.0
    elif sosiType == 'tipshoot':
        xCoBeg = parDict['xSizeSo'][1] + wallThick
    parDict.update({'xCoBeg':["columella beg in x dir", xCoBeg]})
    
    # Calculate columella end in x direction:
    xCoEnd = xCoBeg + float(sum(xLen[:xCoNum])) + xCoNum*wallThick
    del(xCoBeg, xLen, xCoNum, wallThick)
    parDict['xCoEnd'] = ["columella end in x dir", xCoEnd]
    del(xCoEnd)
    
    return

################################################################################

def quiescent(parDict):
    '''Adds quiescent center location to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Get parameter values:
    steleRad = parDict['steleRad'][1]
    wallThick = parDict['wallThick'][1]
    xCoNum = parDict['xCoNum'][1]
    xQcNum = parDict['xQcNum'][1]
    yQcNum = parDict['yQcNum'][1]
    xLen = parDict['xLengths'][1]
    yLen = parDict['yLengths'][1]
    
    # Calculate beg and end of QC in x direction (next after columella):
    xQcBeg = parDict['xCoEnd'][1]
    xQcEnd = xQcBeg + sum(xLen[xCoNum:xCoNum+xQcNum]) + xQcNum*wallThick
    del(xLen, xCoNum, xQcNum)
    
    # Calculate beginning and end of quiescent center in y direction:
    yQcBeg = steleRad
    yQcEnd = steleRad + sum(yLen[:yQcNum]) + yQcNum*wallThick
    del(steleRad, yLen, wallThick, yQcNum)
    
    # Collect coordinate data in list:
    prodLoc = [xQcBeg, xQcEnd, yQcBeg, yQcEnd, 1.0]
    del(xQcBeg, xQcEnd, yQcBeg, yQcEnd)
    parDict['prodLoc'] = ["auxin prod", prodLoc]
    
    return

################################################################################

def sourcesink(parDict):
    '''Add source and sink cell locations to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013-14).'''
    
    # Get type of template:
    sosiType = parDict['sosiType'][1]
    xSizeSo = parDict['xSizeSo'][1]
    xSizeSi = parDict['xSizeSi'][1]
    if sosiType == 'bothshoot' and xSizeSo != xSizeSi:
        raise Exception('Source size must equal sink size here.')
    
    # Get parameter values:
    steleRad = parDict['steleRad'][1]
    wallThick = parDict['wallThick'][1]
    xLen = parDict['xLengths'][1]
    yLen = parDict['yLengths'][1]
    
    # Add source and sink cells to list of cell lengths in x direction:
    if sosiType == 'tipshoot':
        # Source cell at root tip and sink cell towards shoot:
        xLen = [xSizeSo] + xLen + [xSizeSi]
    elif sosiType == 'bothshoot':
        # Source and sink cell towards shoot:
        xLen = xLen + [xSizeSo]
    del(xSizeSo, xSizeSi)
    parDict['xLengths'][1] = xLen # Update dictionary.
    
    # Calculate x coordinate for beg and end of source and sink region(s):
    if sosiType in ['tipshoot', 'bothshoot']:
        # Sink beg in x dir is border between last cell (=si) and prev wall:
        xSiBeg = wallThick/2.0 + sum(xLen[:-1]) + len(xLen[:-1])*wallThick
        # Sink end in x dir is center of last wall:
        xSiEnd = sum(xLen) + len(xLen)*wallThick
    if sosiType == 'tipshoot':
        # Source beginning in x dir is center of first wall:
        xSoBeg = 0.0
        # Source end in x dir is border betw first cell (=so) and next wall:
        xSoEnd = wallThick/2.0 + xLen[0]
    elif sosiType == 'bothshoot':
        # Source beg and end in x dir are same as sink beg and end in x dir:
        xSoBeg = xSiBeg
        xSoEnd = xSiEnd
    del(xLen)
    
    # Calculate y coordinate for beg and end of source and sink region(s):
    numSo = parDict['sosiNum'][1][0]
    numNone = parDict['sosiNum'][1][1]
    numSi = parDict['sosiNum'][1][2]
    if sosiType == 'bothshoot' and numSo + numNone + numSi != len(yLen):
        print(numSo, numNone, numSi, yLen)
        raise Exception('Incorrect specification of source and sink.')
    elif sosiType == 'tipshoot' and numSo != numSi != len(yLen):
        print(numSo, numSi, yLen)
        raise Exception('Incorrect specification of source and sink.')
    del(numNone)
    
    # Source beg in y dir is center of first wall after stele:
    ySoBeg = steleRad
    # Source end in y dir is center of wall after numSo cells:
    ySoEnd = steleRad + sum(yLen[:numSo]) + len(yLen[:numSo])*wallThick
    del(numSo)
    # Sink beg in y dir is center of wall before numSi last cells:
    ySiBeg = steleRad + sum(yLen[:-numSi]) + len(yLen[:-numSi])*wallThick
    del(numSi)
    # Sink end in y dir is center of last wall:
    ySiEnd = steleRad + sum(yLen) + len(yLen)*wallThick
    del(sosiType, wallThick, yLen, steleRad)
    
    # Set default lists for source and sink localization:
    # Format: xBeg, xEnd, yBeg, yEnd, stre.
    parDict['sourceLoc'] = ["auxin source", [xSoBeg, xSoEnd, ySoBeg, ySoEnd, 1.0]]
    del(xSoBeg, xSoEnd, ySoBeg, ySoEnd)
    parDict['sinkLoc'] = ["auxin sink", [xSiBeg, xSiEnd, ySiBeg, ySiEnd, 1.0]]
    del(xSiBeg, xSiEnd, ySiBeg, ySiEnd)
    
    return

################################################################################

