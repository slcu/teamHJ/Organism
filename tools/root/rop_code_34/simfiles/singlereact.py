#! /usr/bin/env python
# Module with functions relating to the single-species reactions in the .model file.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def auxinrct(simPhase, specIndDict, parDict):
    '''Returns text for auxin species definition and reactions.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    # Number of auxin reactions:
    val_pqc = str(parDict['pqc'][1])
    val_q = str(parDict['q'][1])
    if simPhase == 'A':
        numRct = 6 # Maximal 7 reactions.
        if val_pqc == '0.0':
            numRct -= 1 # Do not include production of auxin in QC.
        if val_q == '0.0':
            numRct -= 1 # Do not include degradation of auxin.
    elif simPhase == 'R':
        numRct = 0
    
    # Auxin species:
    ind_auxin = str(specIndDict['auxin'])
    text = "auxin " + ind_auxin + " " + str(numRct) + "               " \
         + "# species index and number of reactions \n"
    del(ind_auxin, numRct)
    
    # Auxin reactions:
    if simPhase == 'A':
        
        # Production at source:
        ind_so = str(specIndDict['auxinIn'])
        val_so = str(parDict['so'][1])
        text = text + "creationOne 1 1 1       " \
             + "# 1 parameter, 1 type of variable interaction, 1 interaction \n" \
             + val_so + "                  " \
             + "# production rate at source (so) \n" \
             + ind_so + "                      " \
             + "# only at auxinSource \n"
        del(ind_so, val_so)
        
        # Degradation at sink:
        ind_si = str(specIndDict['auxinOut'])
        val_si = str(parDict['si'][1])
        text = text + "degradationTwo 1 1 1 \n" \
             + val_si + "          " \
             + "# degradation rate at sink (si) \n" \
             + ind_si + "                      " \
             + "# only at auxinSink \n"
        del(ind_si, val_si)
        
        # Production/synthesis in QC:
        ind_prod = str(specIndDict['auxinProd'])
        if val_pqc != '0.0':
            text = text + "creationOne 1 1 1 \n" \
                 + val_pqc + "            " \
                 + "# production rate in the QC (pqc) \n" \
                 + ind_prod + "                       " \
                 + "# only in auxin production compartments \n"
        del(val_pqc, ind_prod)
        
        # General degradation:
        ind_cell = str(specIndDict['cellMarker'])
        if val_q != '0.0':
            text = text + "degradationTwo 1 1 1 \n" \
                 + val_q + "            " \
                 + "# degradation rate (q) \n" \
                 + ind_cell + "                     " \
                 + "# only in cell compartments \n"
        del(val_q)
        
        # Parameter values for transport across cell membrane:
        #pHc = parDict['pHc'][1]
        #pHw = parDict['pHw'][1]
        #pK = parDict['pK'][1]
        #fracC = 1/(1 + 10.0**(pHc - pK))
        #fracW = 1/(1 + 10.0**(pHw - pK))
        #del(pHc, pHw, pK)
        #Pa = parDict['Pa'][1]
        #val_Def = str(fracC*Pa)
        #val_Din = str(fracW*Pa)
        #del(Pa)
        val_Def = str(parDict['Def'][1])
        val_Din = str(parDict['Din'][1])
        #Nphi = parDict['Nphi'][1]
        #Ppin = parDict['Ppin'][1]
        #Paux = parDict['Paux'][1]
        #val_Tap = str((1-fracC)*Nphi*Ppin)
        #del(fracC, Ppin)
        #val_Taa = str((1-fracW)*Nphi*Paux)
        #del(fracW, Nphi, Paux)
        val_Tap = str(parDict['Tap'][1])
        val_Taa = str(parDict['Taa'][1])
        #print(val_Def, val_Din, val_Tap, val_Taa)
        #raw_input()
        
        # Transport across cell membrane:
        ind_V = str(specIndDict['V'])
        ind_wall = str(specIndDict['wallMarker'])
        val_Dw = str(parDict['Dw'][1])
        text = text + "cellWallAuxinTransport 5 2 3 2      " \
             + "# 5 param, 2 types of var interactions, with 3 resp 2 interactions \n" \
             + val_Def + " " + val_Tap + " " + val_Din + " " + val_Taa + " " + val_Dw + "      " \
             + "# parameters Def, Tap, Din, Taa, Dw \n" \
             + ind_V + " " + ind_cell + " " + ind_wall + "                 " \
             + "# first level of indices: vol ind, cell marker ind, wall marker ind \n" \
             + "0 1                     " \
             + "# second level of indices: ind for PIN and AUX in .neigh file (after area) \n"
        del(ind_wall, val_Tap, val_Din, val_Def, val_Taa, val_Dw)
        
        # Diffusion:
        val_Da = str(parDict['Da'][1])
        text = text + "diffusionRestricted 2 2 2 1     " \
             + "# 2 param, 2 types of var interactions, with 2 resp 1 interaction \n" \
             + val_Da + " " + val_Da + "              " \
             + "# intracellular diffusion rate (D_a) \n" \
             + ind_cell + " " + ind_cell + "                " \
             + "# first level of indices: cell marker ind (i.e. only intracellular diff) \n" \
             + ind_V + "                       " \
             + "# second level of indices: vol ind \n"
        del(ind_V, ind_cell, val_Da)
    
    text = text + "\n" # Empty line after auxin block.
    
    return text

################################################################################

def roparct(simPhase, specIndDict, parDict):
    '''Returns text for active ROP species definition and reactions.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    # Number of active ROP reactions:
    if simPhase == 'A':
        numRct = 0
    elif simPhase == 'R':
        numRct = 3
        val_a = parDict['a'][1]
        val_r = parDict['r'][1]
        if val_a == 0.0:
            numRct -= 1 # Do not include production of ropa.
        if val_r == 0.0:
            numRct -= 1 # Do not include degradation of ropa.
    
    # Active ROP species:
    ind_ropa = str(specIndDict['ROPactive'])
    text = "ROPactive " + ind_ropa + " " + str(numRct) + "          " \
         + "# species index and number of reactions \n"
    del(ind_ropa, numRct)
    
    # Active ROP reactions:
    if simPhase == 'R':
        
        # Production in cells:
        ind_cell = str(specIndDict['cellMarker'])
        if val_a != 0.0:
            val_a = str(val_a)
            text = text + "creationOne 1 1 1 \n" \
                 + val_a + "              " \
                 + "# a=0.0 \n" \
                 + ind_cell + "                   " \
                 + "# only in cells \n"
        del(val_a)
        
        # Degradation in cells:
        if val_r != 0.0:
            val_r = str(val_r)
            text = text + "degradationTwo 1 1 1 \n" \
                 + val_r + "                    " \
                 + "# r=0.01 \n" \
                 + ind_cell + "                     " \
                 + "# only in cells \n"
        del(val_r)
        
        # Diffusion in cells:
        ind_V = str(specIndDict['V'])
        val_Du = str(parDict['Du'][1])
        text = text + "diffusionRestricted 2 2 2 1 \n" \
             + val_Du + " " + val_Du + "               " \
             + "# D_u= 0.05 0.05 (orig 0.1) \n" \
             + ind_cell + " " + ind_cell + "                    " \
             + "# only intracellular \n" \
             + ind_V + "                       " \
             + "# take compartment volume into account \n"
        del(ind_cell, ind_V, val_Du)
    
    text = text + "\n" # Empty line after active ROP block.
    
    return text

################################################################################

def ropirct(simPhase, specIndDict, parDict):
    '''Returns text for inactive ROP species definition and reactions.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    # Number of inactive ROP reactions:
    if simPhase == 'A':
        numRct = 0
    elif simPhase == 'R':
        numRct = 3
        val_b = parDict['b'][1]
        val_s = parDict['s'][1]
        if val_b == 0.0:
            numRct -= 1 # Do not include production of ropi.
        if val_s == 0.0:
            numRct -= 1 # Do not include degradation of ropi.
    
    # Inactive ROP species:
    ind_ropi = str(specIndDict['ROPinactive'])
    text = "ROPinactive " + ind_ropi + " " + str(numRct) + "        " \
         + "# species index and number of reactions \n"
    del(ind_ropi, numRct)
    
    # Inactive ROP reactions:
    if simPhase == 'R':
        ind_cell = str(specIndDict['cellMarker'])
        if val_b != 0.0:
            val_b = str(val_b)
            text = text + "creationOne 1 1 1 \n" \
                 + val_b + "                " \
                 + "# b=0.01 \n" \
                 + ind_cell + "         " \
                 + "# only in cells \n"
        del(val_b)
        if val_s != 0.0:
            val_s = str(val_s)
            text = text + "degradationTwo 1 1 1 \n" \
                 + val_s + "                   " \
                 + "# s=0.0 \n" \
                 + ind_cell + "                    " \
                 + "# only in cells \n"
        del(val_s)
        ind_V = str(specIndDict['V'])
        val_Dv = str(parDict['Dv'][1])
        text = text + "diffusionRestricted 2 2 2 1 \n" \
             + val_Dv + " " + val_Dv + "                    " \
             + "# D_v= 5.0 5.0 (orig 10) \n" \
             + ind_cell + " " + ind_cell + "                    " \
             + "# only intracellular \n" \
             + ind_V + "                       " \
             + "# take compartment volume into account \n"
        del(val_Dv, ind_cell)
    
    text = text + "\n" # Empty line after inactive ROP block.
    
    return text

################################################################################

