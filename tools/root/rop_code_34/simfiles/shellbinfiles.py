#! /usr/bin/env python
# Module with functions for making shell script and solver file for Organism.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def osim(simPhase):
    '''Make and save shell script for running Organism.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    from simfiles import simsettings
    
    # Settings:
    fileName = 'osim' + simPhase + '.sh'
    
    # Get path to Organism simulator:
    orgPath = simsettings.orgpath()
    
    # Contents of file:
    text = '''#! /bin/bash
# Script for running organism.
#
# Input: Model file, initial conditions file, solver file.
# Output: Data file, final conditions file.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-13).
#

modelName=$1''' + simPhase + '''.model
initName=$1.init
binName=$1.bin
dataName=$1.data
finalName=$1.final

''' + orgPath + ''' ${modelName} ${initName} ${binName} > ${dataName} -init_output ${finalName}
'''
    del(simPhase, orgPath)
    
    # Save file:
    fileObj = open(fileName, 'w')
    fileObj.write(text)
    fileObj.close()
    print('Saved file ' + fileName)
    del(text, fileName, fileObj)
    
    return

################################################################################

def obin(baseName, startTime, endTime, saveTime):
    '''Make and save solver file (rk5.bin) for Organism simulations.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    # Settings:
    fileName = baseName + '.bin'
    plotFlag = '2' # 1 for newman, 2 for gnuplot
    
    # Contents of file:
    text = """RK5Adaptive
""" + str(startTime) + ' ' + str(endTime) + """
""" + plotFlag + " " + str(saveTime + 1) + """ # 2 is flag for gnuplot (1 for newman)
1.0 1e-5 # max deltaTime and allowed error btw 4th and 5th RK
"""
    del(plotFlag)
    
    # Save file:
    fileObj = open(fileName, 'w')
    fileObj.write(text)
    fileObj.close()
    print("Saved file " + fileName)
    del(text, fileName, fileObj)
    
    return

################################################################################

