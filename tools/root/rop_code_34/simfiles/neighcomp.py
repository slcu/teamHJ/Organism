#! /usr/bin/env python
#  Module with functions related to compartment properties in .neigh file.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def pinauxstrength(paComp, paStre, interType, paLoc, \
                   currInit, neighInit):
    '''Get strength of PIN or AUX if within PIN or AUX region.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    from toolbox import variables
    
    # Specifics of PIN and AUX lists:
    numL = 6 # Number of entries per list.
    
    # Check length of PIN or AUX localization list:
    if len(paLoc)%numL != 0: # Check if modulo divison yields zero.
        raise Exception('Wrong length of PIN or AUX list.')
    
    # Get column indices for .init file:
    [colIndDict, colList] = variables.colind('.init')
    xCol = colIndDict['x']
    yCol = colIndDict['y']
    del(colIndDict, colList)
    
    # Loop over regions:
    for ind in range(0, len(paLoc)/numL):
        xPaBeg = paLoc[ind*numL]
        xPaEnd = paLoc[ind*numL+1]
        yPaBeg = paLoc[ind*numL+2]
        yPaEnd = paLoc[ind*numL+3]
        paDir = paLoc[ind*numL+4]
        
        if interType == 'cell2wall':
            # Check if cell, i.e. current comp, is in PIN region:
            checkReg = variables.checkregion(currInit[xCol], currInit[yCol], \
                                 xPaBeg, xPaEnd, yPaBeg, yPaEnd)
        elif interType == 'wall2cell':
            # Check if cell, i.e. neigh comp, is in PIN region:
            checkReg = variables.checkregion(neighInit[xCol], neighInit[yCol], \
                                 xPaBeg, xPaEnd, yPaBeg, yPaEnd)
        else:
            raise Exception('Chosen type of interface is not implemented.')
        del(xPaBeg, xPaEnd, yPaBeg, yPaEnd)
        
        if checkReg:
            # Determine relative location of neighbour:
            neighDir = variables.findrelloc(currInit[xCol], neighInit[xCol], \
                                 currInit[yCol], neighInit[yCol])
            
            # Place PIN at correct interface with correct strength:
            paSignal = checkpinaux(neighDir, paDir, interType)
            if paSignal == 'yes':
                paComp = 1
                paStre = paLoc[ind*numL+5]
            del(paSignal, neighDir)
        del(checkReg, paDir, ind)
    del(paLoc, currInit, neighInit, xCol, yCol, interType, numL)
    
    return [paComp, paStre]

################################################################################

def checkpinaux(neighDir, paDir, interType):
    '''Check if PIN or AUX should be placed at current interface.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Default signal:
    paSignal = 'no'
    
    # PIN/AUX depend on relative location and type of interface:
    if neighDir == 'shoot': # Neighbour towards shoot.
        if interType == 'cell2wall' and paDir in ['up', 'uniform']:
            paSignal = 'yes'
        elif interType == 'wall2cell' and paDir in ['down', 'uniform']:
            paSignal = 'yes'
    elif neighDir == 'root': # Neighbour towards root.
        if interType == 'cell2wall' and paDir in ['down', 'uniform']:
            paSignal = 'yes'
        elif interType == 'wall2cell' and paDir in ['up', 'uniform']:
            paSignal = 'yes'
    elif neighDir == 'stele': # Neighbour towards stele.
        if interType == 'cell2wall' and paDir in ['in', 'uniform']:
            paSignal = 'yes'
        elif interType == 'wall2cell' and paDir in ['out', 'uniform']:
            paSignal = 'yes'
    elif neighDir == 'epid': # Neighbour towards epidermis.
        if interType == 'cell2wall' and paDir in ['out', 'uniform']:
            paSignal = 'yes'
        elif interType == 'wall2cell' and paDir in ['in', 'uniform']:
            paSignal = 'yes'
    del(neighDir, paDir, interType)
    
    return paSignal

################################################################################

