#include <vector>
#include <iostream>
#include "../../src/organism.h"
//#include "../solvers/baseSolver.h"
#include "../../src/common/myConfig.h"
#include "../../src/common/mySignal.h"
//#include "../../src/common/myRandom.h"

int main(int argc,char *argv[]) 
{
  myConfig::registerOption ( "input_format", 1 );
  myConfig::registerOption ( "output_format", 1 );
  //myConfig::registerOption("init_output", 1);
  //myConfig::registerOption("debug_output", 1);
  myConfig::registerOption("model_input", 1);
  myConfig::registerOption("neigh_input", 1);
  myConfig::registerOption("verbose", 1);
  myConfig::registerOption("help", 0);
  
  std::string configFile(getenv("HOME"));
  configFile.append("/.organism");
  myConfig::initConfig(argc, argv, configFile);
  
  if (myConfig::getBooleanValue("help")) {
    std::cerr << std::endl 
	      << "Usage: " << argv[0] << " initFile > outfile" << std::endl 
	      << std::endl;
    std::cerr << "Flags to specify input and/or output formats:" << std::endl;
    std::cerr << "-input_format format - Sets format in input file." << std::endl
	      << "Available input formats are:" << std::endl
	      << "organism (default), " << std::endl
	      << "ply (interchangable graph format)." << std::endl;
    std::cerr << "-output_format format - Sets format for output of"
	      << " state in specified file format. " << std::endl
	      << "Available output formats are:" << std::endl
	      << "organism (default), " << std::endl
	      << "ply (interchangable graph format)." << std::endl;      
    std::cerr << "Possible additional flags are:" << std::endl;
    std::cerr << "-model_input file - Model file (organism format) to define how to "
	      << "calculate neighborhood from reaction defined in file." << std::endl;
    std::cerr << "-neigh_input file - Read neighborhood from file"
	      << " (overriding those read in the model file)." << std::endl;
    std::cerr << "-verbose flag - Set flag for verbose (flag=1) or "
	      << "silent (0) output mode to stderr." << std::endl; 
    exit(EXIT_FAILURE);
  } else if (myConfig::argc() != 2) {
    std::cerr << "Wrong number of arguments given to " << argv[0] << std::endl
	      << "Type '" << argv[0] << " -help' for usage." << std::endl;
    exit(EXIT_FAILURE);
  }
  std::string modelFile = myConfig::argv(1);
  std::string initFile = myConfig::argv(2);
  std::string simPara = myConfig::argv(3);
	
  int verboseFlag=0;
  std::string verboseString;
  verboseString = myConfig::getValue("verbose", 0);
  if( !verboseString.empty() ) {
    verboseFlag = atoi( verboseString.c_str() );
    if( verboseFlag != 0 || verboseFlag !=1 ) {
      verboseFlag=0;
      std::cerr << "Flag given to -verbose not recognized (0, 1 allowed)."
		<< " Setting it to zero (silent)." << std::endl;
    }
  }
  
  // Create organism and read initial configuration
  Organism O;
  std::string initFile = myConfig::argv ( 1 );
  std::string inputFormat;
  inputFormat = myConfig::getValue ( "input_format",0 );
  if ( inputFormat.empty() || inputFormat.compare ( "organism" ) ==0 )
    {
      if ( verboseFlag )
        {
	  std::cerr << "Reading init from file " << initFile << " assuming organism format." << std::endl;
        }
      O.readInit ( initFile.c_str(),verboseFlag );
      //CHECK HERE(?)FOR MODEL OR NEIGH FILE TO CALCULATE/READ NEIGHBORHOOD
      //IF NOT PROVIDED, ASSUMME SPHERES WITH NEIGHBORS DEFINED BY OVERLAP, "
      //i.e REACTION NeighborhoodDistanceSphere???
    }
  else if ( inputFormat.compare ( "ply" ) ==0 )
    {
      if ( verboseFlag )
        {
	  std::cerr << "Reading initial configuration from file " << initFile << " assuming ply format."
		    << std::endl;
        }
      //PLY_file pf ( initFile.c_str() );
      //PLY_reader P;
      //P.read ( pf,O );
      //     std::cerr << "ply read\n ";
      //Sort all cellWalls and cellVertices to comply with area calculations and plotting
      //T.sortCellWallAndCellVertex();
      //T.checkConnectivity(verboseFlag);
      //readInitPly(*O, initFile.c_str(),verboseFlag); ...yet to be defined...
    }
  else
    {
      std::cerr << "Input format " << inputFormat << " not recognized. Use '-help' for allowed formats."
		<< std::endl;
      exit ( EXIT_FAILURE );
    }

 // Print in specified output format
    std::string outputFormat;
    outputFormat = myConfig::getValue ( "output_format",0 );
    if ( outputFormat.empty() || outputFormat.compare ( "organism" ) ==0 )
    {
      if ( verboseFlag )
        {
	  std::cerr << "Printing output using organism format." << std::endl;
        }
      O.printInit ( std::cout );
      O.print;
    }
    else if ( outputFormat.compare ( "ply" ) ==0 )
      {
        if ( verboseFlag )
	  {
            std::cerr << "Printing output using ply graph format." << std::endl;
	  }
        //PLY_ostream ply_os ( std::cout );
        //ply_os.bare_geometry_output() = false;
        //ply_os.center_triangulation_output() = false;
        //ply_os << O;
        //printInitPly(*O, std::cout); //yet to be defined
    }
    else
    {
        std::cerr << "Warning: main() - Format " << outputFormat << " not recognized. "
                  << "No outputwritten." << std::endl;
    }
    if ( verboseFlag )
    {
        std::cerr << "All done!" << std::endl;
    }
}

// Function reading a initial configuration in ply graph format and adds it tpo an Organism
// object
//void readInitPly(Organism *O, std::istream is) {
//}

// Function writing an Organism object into a ply graph format
//void printInitPly(Organism *O, std::ostream os) {
//}




