//
// Filename     : myGL.h
// Description  : Wrapper for PlotToolGL
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:
//
#ifndef MYGL_H
#define MYGL_H
#include <iostream>
#include "plotToolGL.h"

///
/// @brief Wrapper for PlotToolGL
///
/// OpenGL requires function pointers for glutDisplayFunc, glutIdleFunc,
/// glutReshapeFunc, glutKeyboardFunc and glutSpecialFunc. This namespace
/// contains wrapper functions that calls the corresponding member functions
/// in PlotToolGL.
///
namespace myGL {	
	///
	/// @brief Sets a global PlotToolGL object.
	///
	void setGL(PlotToolGL *gl);

	///
	/// @brief Wrapper for PlotToolGL::renderScene()
	///
	/// @see PlotToolGL::renderScene()
	///
	void renderScene(void);
	
	///
	/// @brief Wrapper for PlotToolGL::changeSize()
	///
	/// @see PlotToolGL::changeSize()
	///
	void changeSize(int w, int h);
	
	///
	/// @brief Wrapper for PlotToolGL::processSpecialKeys()
	///
	/// @see PlotToolGL::processSpecialKeys()
	///
	void processSpecialKeys(int key, int x, int y);

  ///
	/// @brief Wrapper for PlotToolGL::processNormalKeys()
	///
	/// @see PlotToolGL::processNormalKeys()
	///	
	void processNormalKeys(unsigned char key, int x, int y);
}
#endif //MYGL_H
