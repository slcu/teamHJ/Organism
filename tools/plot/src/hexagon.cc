//
// Filename     : hexagon.cc
// Description  : Class describing sphere-shaped cell colonies
// Author(s)    : Patrik Sahlin (sahlin@thep.lu.se)
// Revision     : $Id$
//
#include <cstdlib>
#include "hexagon.h"
#include <iomanip>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>

using myVector3::Vector3;

Hexagon::Hexagon(Data *data, size_t dim,std::string min, std::string max)
	: BaseObject(data,dim, min,max)
{
	numSpatialVar_ = 1;
	xList_.resize(numSpatialVar_);
	yList_.resize(numSpatialVar_);
	rCol_.resize(numSpatialVar_);
	
	if (dimension() == 2) {
		varIndex_ = 3;
		rCol_[0] = 2;
		xList_[0] = 0;
		yList_[0] = 1;
	} else {
		std::cerr << "Hexagon::Hexagon(): " 
			  << dimension() << "D objects are not supported.\n";
		std::exit(EXIT_FAILURE);
	}

	findMaxMin();
	normalizeVariables();
	setSpaceScale();
	findMaxMin();
	max_[rCol_[0]] *= 1.2;
}


void Hexagon::plotPS(std::ofstream &OUT, size_t t, size_t cellIndex, 
		    float r, float g, float b, int move, double scaleFact)
{
 	Vector3 min(minX(), minY(), minZ()), pos;
	double X, Y, R;

	pos = (position(t, cellIndex, 0) - min);
	pos += 1.2 * maxR();
	pos *= scaleFact;
	X = move + pos.x();
	Y = move + pos.y();
	R = radius(t, cellIndex) * scaleFact;

	OUT << X << " " << Y << " " << R << " " << r << " " << g << " " << b << " hexagon" << std::endl;
}






