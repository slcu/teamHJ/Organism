#include "myGL.h"
#include "plotToolGL.h"
namespace myGL {
	PlotToolGL *GL;
}	

void myGL::setGL(PlotToolGL *gl)
{
	if(gl==0)
		std::cerr << "Warning\n";
	GL=gl;
	
} 

void myGL::changeSize(int w, int h) {
	if(GL==0) {
		std::cerr << "GL is not initialized in myGL\n";
		exit(-1);
	}
	GL->changeSize(w,h);
}

void myGL::processSpecialKeys(int key, int x, int y) 
{
   if(GL==0) {
		std::cerr << "GL is not initialized in myGL\n";
		exit(-1);
	}
	GL->processSpecialKeys(key,x,y);
}

void myGL::processNormalKeys(unsigned char key, int x, int y) 
{
	if(GL==0) {
		std::cerr << "GL is not initialized in myGL\n";
		exit(-1);
	}
	GL->processNormalKeys(key,x,y);
}

void myGL::renderScene(void) 
{
	if(GL==0) {
		std::cerr << "GL is not initialized in myGL\n";
		exit(-1);
	}
	GL->renderScene();
}

