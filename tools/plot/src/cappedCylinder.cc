//
// Filename     : cappedCylinder.cc
// Description  : Class describing capped cylinder shaped cell colonies
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:$
//

#include "cappedCylinder.h"
#include <cstdlib>
using myVector3::Vector3;

CappedCylinder::CappedCylinder(Data *data,size_t dim, std::string min, std::string max, double b) 
	:BaseObject(data,dim,min,max), b_(b)
{
	numSpatialVar_ = 2;
	xList_.resize(numSpatialVar_);
	yList_.resize(numSpatialVar_);
	rCol_.resize(1);
	if(dimension()==3)
		zList_.resize(numSpatialVar_);
	
	if (dimension()==2) {
		varIndex_=6;
		rCol_[0]= 5;
		xList_[0] = 0;
		xList_[1] = 2;
		yList_[0] = 1;
		yList_[1] = 3;	
	}
	else if(dimension()==3) {
		varIndex_= 8;
		rCol_[0]= 7;
		xList_[0] = 0;
		xList_[1] = 3;
		yList_[0] = 1;
		yList_[1] = 4;
		zList_[0] = 2;
		zList_[1] = 5;
	}
	else {
		std::cerr << "CappedCylinder::CappedCylinder(): " 
		<<dimension() <<"D objects are not supported\n";
		std::exit(EXIT_FAILURE);
	}
	truncateData();
	findMaxMin();
	normalizeVariables();
	setSpaceScale();
	findMaxMin();
}

/*void CappedCylinder::setNeighbor(double rFactor)
{ 	
  double d,r;
	Vector3 posi,posj;
	neighbor_.resize( TMax() );
  for( size_t t=0 ; t<TMax() ; ++t ) {
		neighbor_[t].resize(numCell(t) );
		for( size_t i=0 ; i<numCell(t) ; i++ )
      for( size_t j=i+1 ; j<numCell(t); j++ ) {
				
				r = std::sqrt( dot(position(t,i,0)-position(t,i,1),
													 position(t,i,0)-position(t,i,1))) +
					std::sqrt( dot(position(t,j,0)-position(t,j,1),
												 position(t,j,0)-position(t,j,1)));
				
				posi = 0.5*(position(t,i,0)+position(t,i,1));
				posj = 0.5*(position(t,j,0)+position(t,j,1));
				d = std::sqrt(dot(posi-posj,posi-posj));
				if( d<= r*rFactor ) {
					//Add neighbor
					neighbor_[t][i].push_back(j);
					neighbor_[t][j].push_back(i);
				}
      }
	}	
}*/

void CappedCylinder::plotGL(size_t cellIndex,size_t timeIndex)
{
	//find the two coordinates
	Vector3 position1 = position(timeIndex,cellIndex,0);
	Vector3 position2 = position(timeIndex,cellIndex,1);
	double factor = 2/(scale());
	
	//double bCell= radius(timeIndex, cellIndex);
	double bCell=data_->getFrame(timeIndex).getMatrix()[cellIndex][rCol_[0]];
	//double bCell = 0.5;
	//std::cerr << bCell << " " << rCol_[0] << " " << data_->getFrame(timeIndex).getMatrix()[cellIndex].size() << "\n";
//rescale vectors
	position1 -= COM();
	position2 -= COM();
	position1 *=factor;
	position2 *=factor;
	bCell *=factor;
	Vector3 diff= position2-position1;
	//draw capped cylinder
	glTranslatef(GLfloat(position1(0)),GLfloat(position1(1)),GLfloat(position1(2)));
	gluSphere(quad_,bCell,32,32);
	glPushMatrix();
	glTranslatef(GLfloat(diff(0)),GLfloat(diff(1)),GLfloat(diff(2)));
	gluSphere(quad_,bCell,32,32);
	glPopMatrix();
	glRotatef(diff.phi()*180/Pi,0.0f,0.0f,1.0f);
	glRotatef(diff.theta()*180/Pi,0.0f,1.0f,0.0f);
	gluCylinder(quad_,bCell,bCell,GLfloat(std::sqrt(dot(diff,diff))),32,32);
}


void CappedCylinder::plotPS(std::ofstream &OUT,size_t t,size_t cellIndex, float r,
	float g, float b, int move, double scaleFact)
{
	Vector3 min(minX(),minY(),minZ()); 
	Vector3 position1=position(t,cellIndex,0)- min;
	Vector3 position2=position(t,cellIndex,1)- min;
	position1 += 1.2*maxR();
	position1 *= scaleFact;
	position2 += 1.2*maxR();
	position2 *= scaleFact;
	
	double X1,Y1,X2,Y2,R;
	X1 = move + position1.x();
	Y1 = move + position1.y();
	R = b_*scaleFact;
	//std::cerr << scaleFact << "\n";
	OUT << r << " " << g << " " << b << " " << "setrgbcolor\n\n";
	OUT << X1 << " " 
	<< Y1 << " "
	<< R << " " 
	<< "0 360 arc\n";
	OUT << "gsave\n";
	OUT << "fill\n";
	OUT << "grestore\n";
	OUT << "0 0 0 setrgbcolor\n";
	OUT << "stroke\n\n";
	
	X2 = move + position2.x();
	Y2 = move + position2.y();
	
	OUT << r << " " << g << " " << b << " " << "setrgbcolor\n\n";
	
	OUT << X2 << " " 
	<< Y2 << " "
	<< R << " " 
	<< "0 360 arc\n";
	OUT << "gsave\n";
	OUT << "fill\n";
	OUT << "grestore\n";
	OUT << "0 0 0 setrgbcolor\n";
	OUT << "stroke\n\n";
	
	Vector3 u= position2-position1;
	Vector3 v(-u.y(),u.x(),0);
	v*=1/std::sqrt((dot(v,v)));
	
	OUT << r << " " << g << " " << b << " " << "setrgbcolor\n\n";
	
	OUT << "newpath\n"
	<< "[\n"
	<< "\t[" <<X1-R*v.x() << " " << Y1-R*v.y() << "]\n"
	<< "\t[" <<X2-R*v.x() << " " << Y2-R*v.y() << "]\n"
	<< "\t[" <<X2+R*v.x() << " " << Y2+R*v.y() << "]\n"
	<< "\t[" <<X1+R*v.x() << " " << Y1+R*v.y() << "]\n"
	<< "]\n";		
	OUT << "makepolygon\n"
	<< "closepath\n"
	<< "fill\n\n";
	OUT << "0 0 0 setrgbcolor\n";
	OUT << "newpath\n";
	OUT << X1-R*v.x() << " " << Y1-R*v.y() << " moveto\n";
	OUT << X2-R*v.x() << " " << Y2-R*v.y() << " lineto\n";
	OUT << "stroke\n\n";
	OUT << "newpath\n";
	OUT << X1+R*v.x() << " " << Y1+R*v.y() << " moveto\n";
	OUT << X2+R*v.x() << " " << Y2+R*v.y() << " lineto\n";
	OUT << "stroke\n";
}



