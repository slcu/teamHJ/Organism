//
// Filename     : buddingYeast.h
// Description  : Class describing budding yeast cell colonies
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:$
//
#ifndef BUDDINGYEAST_H
#define BUDDINGYEAST_H

#include "baseObject.h"
///
///@brief A class describing cell colonies of budding yeast
///
/// Budding yeast is described by two spatial varibles defining the centers
/// of two spheres. In the data object the coordinates variables are
/// located in columns 0,1(2), x,y, (z). The column following the
/// last spatial variable contains the radius of the sphere.
///d
class BuddingYeast : public BaseObject {
 public:
	///
	/// @brief Main constructor
	///
	/// Main constructor, sets which indices that describes x,y,z
	/// coordinates and calls contructor of base class.
	///
	/// @see BaseObject::BaseObject(Data *data, size_t dimension);
	///
	BuddingYeast(Data *data, size_t dimension, std::string min , std::string max);
	
	///
	/// @brief destructor
	///
	~BuddingYeast(){}
	
	///
	/// @brief Sets the neighbors of each cell at each timepoint.
  ///
	/// @see BaseObject::setNeighbor(double rFactor=1)
	///
	//void setNeighbor(double rFactor=1.0);

  ///
	/// @brief Does the actual plotting
	///
	void plotGL(size_t cellIndex, size_t timeIndex);
 
	///
	/// @brief Does the actual PS plotting
	///
	void plotPS(std::ofstream &OUT,size_t t,size_t cellIndex, 
							float r, float g, float b, int move, double scaleFact);


 private:
	BuddingYeast(const BuddingYeast &);
};
#endif // BUDDINGYEAST_H
