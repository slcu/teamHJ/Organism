//
// Filename     : root.h
// Description  : Class describing cylinder shaped cells
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:$
//
#ifndef CYLINDER_H
#define CYLINDER_H

#include "baseObject.h"
///
///@brief A class describing cylinder-shaped cells
///
/// This class should be used with some caution since it is rather specific. It is, as by now, 
/// only meant to be used for a simple 1D root model. 
class Cylinder : public BaseObject {
 public:
	///
	/// @brief Main constructor
	///
	/// Main constructor, sets which indices that describes x,y,z
	/// coordinates and calls contructor of base class.
	///
	/// @see BaseObject::BaseObject(Data *data, size_t dimension);
	///
	Cylinder(Data *data, size_t dimension, std::string min, std::string max);
	
	///
	/// @brief destructor
	///
	~Cylinder(){}
	
  ///
	/// @brief Does the actual plotting
	///
	/// @see PlotToolGL::plotObject()
	///
	void plotGL(size_t cellIndex, size_t timeIndex);

  ///
	/// @brief Does the actual PS plotting
	///
	void plotPS(std::ofstream &OUT,size_t t,size_t cellIndex, 
							float r, float g, float b, int move, double scaleFact);

 private:
	Cylinder(const Cylinder &);
};
#endif // CYLINDER_H
