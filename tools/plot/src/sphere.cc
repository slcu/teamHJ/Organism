//
// Filename     : sphere.cc
// Description  : Class describing sphere-shaped cell colonies
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:$
//
#include <cstdlib>
#include "sphere.h"
#include <iomanip>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
using myVector3::Vector3;

Sphere::Sphere(Data *data, size_t dim, std::string min, std::string max)
  :BaseObject(data,dim,min,max)
{
  numSpatialVar_=1;
  xList_.resize(numSpatialVar_);
  yList_.resize(numSpatialVar_);
  rCol_.resize(numSpatialVar_);
  if(dimension()==3)
    zList_.resize(numSpatialVar_);
  if (dimension()==2) {
    varIndex_= 3;
    rCol_[0]= 2;
    xList_[0] = 0;
    yList_[0] = 1;
  }
  else if(dimension()==3) {
    varIndex_=4;
    rCol_[0]= 3;
    xList_[0] = 0;
    yList_[0] = 1;
    zList_[0] = 2;
  }
  else {
    std::cerr << "Sphere::Sphere(): " 
	      <<dimension() <<"D objects are not supported\n";
    std::exit(EXIT_FAILURE);
  }
  truncateData();
  findMaxMin();
  normalizeVariables();
  setSpaceScale();
  findMaxMin();
}

void Sphere::setNeighbor(double rFactor)
{ 	
  double d,r;
  Vector3 posi,posj;
  neighbor_.resize(TMax());
  for( size_t t=0 ; t<TMax() ; ++t ) {
    neighbor_[t].resize(numCell(t) );
    for( size_t i=0 ; i<numCell(t) ; i++ )
      for( size_t j=i+1 ; j<numCell(t); j++ ) {
	r = radius(t,i) + radius(t,j);
	posi = position(t,i,0);
	posj = position(t,j,0);
	d = std::sqrt(dot(posi-posj,posi-posj));
	if( d<= r*rFactor ) {
	  //Add neighbor
	  neighbor_[t][i].push_back(j);
	  neighbor_[t][j].push_back(i);
	}
      }
  }
}

void Sphere::plotGL(size_t cellIndex, size_t timeIndex)
{
  Vector3 pos = position(timeIndex,cellIndex,0);
  //if (pos(0)<=0.0) { //to make a cut...
    double rad = radius(timeIndex, cellIndex);
    double factor = 2/scale();
    pos -= COM();
    pos *= factor;
    rad *= factor;
    
    glTranslatef(pos(0),pos(1),pos(2));
    gluSphere(quad_,rad,16,16);
    //}
}

void Sphere::plotPS(std::ofstream &OUT,size_t t,size_t cellIndex, 
		    float r, float g, float b, int move, double scaleFact)
{
  static bool firstTime = true;
  if (firstTime) {
    setNeighbor();
    firstTime=false;
  }
  Vector3 min(minX(),minY(),minZ()), pos;
  double X,Y,R;
  OUT << "mark\n";
  for( size_t n=0 ; n<numNeigh(t,cellIndex); ++n ) {
    size_t neigh = neighbor(t,cellIndex,n);
    pos = (position(t,neigh,0)- min);
    pos += 1.2*maxR();
    pos *= scaleFact;
    X = move + pos.x();
    Y = move + pos.y();
    R = radius(t,neigh)*scaleFact;
    OUT << X << " " 
	<< Y << " "
	<< R << "\n"; 
  }
  pos = (position(t,cellIndex,0)- min);
  pos += 1.2*maxR();
  pos *= scaleFact;
  X = move + pos.x();
  Y = move + pos.y();
  R = radius(t,cellIndex)*scaleFact;
  OUT << X << " " 
      << Y << " "
      << R << " "
      << r << " " << g << " " << b << "\n"
      << "blob\n\n";      
}






