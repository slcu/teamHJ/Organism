#ifndef LANGEVIN_H
#define LANGEVIN_H
//
// Filename     : langevin.h
// Description  : Simple Langevin stochastic numerical solvers
// Author(s)    : Aram (ag979)
// Created      : Aug 2018
// Revision     : $Id:$
//

#include "baseSolver.h"

///
/// @brief A simple Langevin first order Euler-Maruaya step solver class
///
class LangevinEM : public BaseSolver {

private:
  double h_;

public:
  ///
  /// @brief Main constructor
  ///
  LangevinEM(Organism *O, std::ifstream &IN);

  ///
  /// @brief Reads the parameters used by the Langevin solver
  ///
  /// This function is responsible for reading parameters used by the
  /// Langevin-Euler-Maruayama algorithm. The parameter file sent to the
  /// simulator binary looks like:
  ///
  /// <pre>
  /// LangevinEM
  /// T_start T_end
  /// printFlag printNum
  /// h
  /// </pre>
  ///
  /// where Langevin is the identity string used by BaseSolver::getSolver
  /// to identify that the algorithm should be used. T_start
  /// (T_end) is the start (end) time for the simulation, printFlag is an
  /// integer which sets the output format (read by BaseSolver::print()),
  /// printNum is the number of equally spread time points to be printed. h
  /// is the step size for each Langevin step.
  ///
  /// Comments can be included in the parameter file by starting the line with
  /// an #. Caveat: No check on the validity of the read data is applied.
  ///
  /// @see BaseSolver::getSolver()
  /// @see BaseSolver::print()
  ///
  void readParameterFile(std::ifstream &IN);

  ///
  /// @brief Runs a simulation of an organism model
  ///
  void simulate(void);

  ///
  /// @brief A LangevinEM step
  ///
  void langevinEM(std::vector<std::vector<double> > &sdydt);
};

#endif /* LANGEVIN_H */
