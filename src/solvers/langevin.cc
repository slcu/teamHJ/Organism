//
// Filename     : langevin.cc
// Description  : Simple Langevin(-Euler-Maruyama) numerical solver
// Author(s)    : Aram (ag979@cam.ac.uk)
// Created      : Aug 2018
// Revision     : $Id:$
//
#include <cmath>
#include "langevin.h"
#include "../common/myRandom.h"

LangevinEM::LangevinEM(Organism *O, std::ifstream &IN) : BaseSolver(O, IN) {
  readParameterFile(IN);
}

void LangevinEM::readParameterFile(std::ifstream &IN) {
  // Read in the needed parameters
  IN >> startTime_;
  t_ = startTime_;
  IN >> endTime_;

  IN >> printFlag_;
  IN >> numPrint_;

  IN >> h_;
  //	std::cerr << "LangevinEM constructor executed \n";
}

void LangevinEM::simulate() {
  // Check that h and endTime-startTime are > 0
  //
  if (!(h_ > 0. && (endTime_ - startTime_) > 0.)) {
    std::cerr << "LangevinEM::simulate() Wrong time borders or time step for "
              << "simulation. No simulation performed.\n";
    return;
  }
  std::cerr << "Simulating using a simple Langevin-Euler-Maruyama solver\n";

  // Initiate reactions for those where it is applicable
  //
  O_->reactionInitiate(startTime_, y_);
  std::cerr << "LangevinEM reactions initiated \n";

  // Introduce the neighborhood
  //
  if (O_->numNeighborhood())
    O_->neighborhoodCreate(y_, t_);
  if (y_.size() && y_.size() != dydt_.size()) {
    dydt_.resize(y_.size(), y_[0]);
  }
  assert(y_.size() == O_->numCompartment() && y_.size() == dydt_.size());
  std::cerr << "LangevinEM neighborhood initiated \n";

  // Create the vector that will be needed by this solver
  //
  std::vector<std::vector<double> > sdydt(N());
  // Resize each vector
  for (size_t i = 0; i < N(); i++)
    sdydt[i].resize(M());

  assert(y_.size() == O_->numCompartment() && y_.size() == sdydt.size());

  // Initiate print times
  //
  double tiny = 1e-10;
  double printTime = endTime_ + tiny;
  double printDeltaTime = endTime_ + 2. * tiny;
  if (numPrint_ <= 0) // No printing
    printFlag_ = 0;
  else if (numPrint_ == 1) {   // Print last point (default)
  } else if (numPrint_ == 2) { // Print first/last point
    printTime = startTime_ - tiny;
  } else { // Print first/last points and spread the rest uniformly
    printTime = startTime_ - tiny;
    printDeltaTime = (endTime_ - startTime_) / double(numPrint_ - 1);
  }

  // ----------------------------------------
  // Go
  // ----------------------------------------
  t_ = startTime_;
  numOk_ = numBad_ = 0;
  while (t_ < endTime_) {
    if (debugFlag()) {
      yCopy_[debugCount()] = y_;
    }
    // Update the derivatives (for printing)
    O_->derivs(y_, dydt_);

    // Print if applicable
    if (printFlag_ && t_ >= printTime) {
      printTime += printDeltaTime;
      print(*OS_, *neighOS_);
    }

    // Update
    //    std::cerr << "LangevinEM running solver \n";
    langevinEM(sdydt);
    numOk_++;
    //    std::cerr << "LangevinEM completed the run \n";

    // Make updates of rections that requires it
    O_->reactionUpdate(h_, t_, y_);
    //    std::cerr << "LangevinEM updated the reactions \n";

    // Check for cell divisions, compartmental removal etc
    O_->compartmentChange(y_, dydt_, t_);
    //    std::cerr << "LangevinEM changed the compartment \n";

    // Rescale all temporary vectors as well
    if (y_.size() != sdydt.size())
      sdydt.resize(N(), sdydt[0]);

    // Update neighborhood
    if (O_->numNeighborhood())
      O_->neighborhoodUpdate(y_, t_);
    //     std::cerr << "LangevinEM updated the neighborhood\n";

    // Update time variable
    if ((t_ + h_) == t_) {
      std::cerr << "LangevinEM::simulate() Step size too small.";
      exit(-1);
    }
    t_ += h_;
  }
  if (printFlag_) {
    // Update the derivatives
    O_->derivs(y_, dydt_);
    print(*OS_, *neighOS_);
  }
  std::cerr << "LangevinEM Simulation done." << std::endl;
  return;
}

void LangevinEM::langevinEM(std::vector<std::vector<double> > &sdydt) {
  size_t n = N();
  size_t m = M();
  size_t volumeIndex = O_->numTopologyVariable() - 1;
  bool ifnan = false;
  int num_of_barrier_corrections = 0;

  //  std::cerr <<"Array size "<<n<<" x "<<m<<std::endl;
  //  std::cerr << "LangevinEM is about to call derivsL \n";
  O_->derivsL(y_, dydt_, sdydt);
  //  std::cerr << "LangevinEM called derivsL \n";

  for (size_t i = 0; i < n; ++i) {
    for (size_t j = 0; j < m; ++j) {
      //    std::cerr << "Solver runs, i="<<i<<"  j="<<j<<"    ";
      //    std::cerr << dydt_[i][j] << "," << sdydt[i][j] << std::endl;
      y_[i][j] = y_[i][j] + h_ * dydt_[i][j] + sqrt(h_) * sdydt[i][j];

      if (j >= volumeIndex &&
          y_[i][j] <
              0.0) // Setting an absortive barrier at 0 for concentrations
      {
        y_[i][j] = 0.0;
        //	num_of_barrier_corrections++;
      }

      // Check if derivative are singular
      //    if(std::isnan(dydt_[i][j]))
      //	{
      //		break;
      //		std::cerr << "dydt is nan"<<std::endl;
      //		ifnan = true;
      //	}
      //      if (std::isnan(dydt_[i][j]) || std::isinf(dydt_[i][j]))
      //        dydt_[i][j] = 0.0;
      //      if (std::isnan(sdydt[i][j]) || std::isinf(sdydt[i][j]))
      //        sdydt[i][j] = 0.0;
      //    if( ifnan )	break;
    }
  }

  //    std::cerr << "Absolute barrier had been called
  //    "<<num_of_barrier_corrections<<" times \n";

  //  for(size_t i=0 ; i<N() ; ++i ) {
  //    for(size_t j=0 ; j<M() ; ++j ) {
  //      std::cerr << dydt_[i][j] << "," << sdydt[i][j] << " ";
  //    }
  //  std::cerr << std::endl;
  //  }
}
