//
// Filename     : initiation.cc
// Description  : Classes describing initiation rules for a tissue simulation
// Author(s)    : Henrik Jonsson (henrik.jonsson@slcu.cam.ac.uk)
// Created      : August 2019
// Revision     : $Id:$
//
#include "initiation.h"
#include "../organism.h"
#include "baseReaction.h"
#include <cstdlib>

namespace Initiation {

SingleValue::SingleValue(std::vector<double> &paraValue,
                             std::vector<std::vector<size_t> > &indValue) {
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 1 && paraValue.size() != 2 && paraValue.size() != 3) {
    std::cerr
        << "Initiation::SingleValue::"
        << "SingleValue() "
        << "Takes at least one parameter, setting the value which cells should "
        << "be assigned. "
        << std::endl
        << "A second parameter can be given to give a flag for whether the procedure"
        << "should be repeated. If such is the case, a third parameter can be given to "
        << "set a deltaT for repeat of the procedure. If not given, this happens on every iteration." 
        << std::endl;
    exit(EXIT_FAILURE);
  }
  if (paraValue[0] < 0.0) {
    std::cerr << "Initiation::SingleValue::"
              << "SingleValue() "
              << "First parameter is a concentration, which must be positive."
              << std::endl;
    exit(EXIT_FAILURE);
  }

  if (indValue.size() < 1) {
    std::cerr << "Initiation::SingleValue::"
              << "SingleValue() "
              << "Either index connected to variable (o) or indices for cell "
                 "variable(s) to be initiated given."
              << std::endl
              << std::endl
              << "ERROR: index list is of size: "
              << indValue.size()
              << std::endl;
    exit(EXIT_FAILURE);
  }

  // Set the variable values
  //
  setId("Initiation::SingleValue");
  setParameter(paraValue);
  setVariableIndex(indValue);

  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp[0] = "value";
  if (numParameter() == 2)
    tmp[1] = "update_flag";
  if (numParameter() == 3)
    tmp[2] = "DeltaT";

  setParameterId(tmp);

  // Set the next time if deltaT given (assumes starting at t=0)
  localTime_ = 0.0;
  if (numParameter() == 3)
    nextTime_ = nextTime_ + parameter(1);
}

void SingleValue::derivs(Compartment &compartment, size_t species,
                           DataMatrix &y, DataMatrix &dydt) {
  // nothing
}

void SingleValue::initiate(double t, std::vector<std::vector<double> > &y) {

  float value = parameter(0);
  size_t n_cells = y.size();

  for (size_t ii = 0; ii < numVariableIndex(0); ii++) {
    size_t v_idx = variableIndex(ii, 0);
    for (size_t c_idx = 0; c_idx < n_cells; c_idx++) {
        y[c_idx][v_idx] = value;
    }
  }
}

void SingleValue::update(double h, double t,
    std::vector<std::vector<double> > &y) {
  localTime_ += h;

  if (numParameter() > 2 && localTime_ > nextTime_) {
    float value = parameter(0);
    size_t n_cells = y.size();
    for (size_t ii = 0; ii < numVariableIndex(0); ii++) {
      size_t v_idx = variableIndex(ii, 0);
      for (size_t c_idx = 0; c_idx < n_cells; c_idx++) {
          y[c_idx][v_idx] = value;
      }
    }
    if (numParameter() == 3){
      nextTime_ += parameter(2);
    }
  }
}

RandomBoolean::RandomBoolean(std::vector<double> &paraValue,
                             std::vector<std::vector<size_t> > &indValue) {
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 2 && paraValue.size() != 3) {
    std::cerr
        << "Initiation::RandomBoolean::"
        << "RandomBoolean() "
        << "Uses first parameter p (probability to set cell variable to one)."
        << std::endl
        << "The second parameter sets the seed of the random generator."
        << std::endl
        << "A third parameter can be given to set a deltaT for repeat of the "
           "procedure, "
        << std::endl
        << "but where only off cells can possibly be set to one." << std::endl;
    exit(EXIT_FAILURE);
  }
  if (paraValue[0] < 0.0 || paraValue[0] > 1.0) {
    std::cerr << "Initiation::RandomBoolean::"
              << "RandomBoolean() "
              << "First parameter a probability and have to be in [0:1]."
              << std::endl;
    exit(EXIT_FAILURE);
  }

  if (indValue.size() != 0 || indValue.size() != 1) {
    std::cerr << "Initiation::RandomBoolean::"
              << "RandomBoolean() "
              << "Either index connected to variable (o) or indices for cell "
                 "variable(s) to be initiated given."
              << std::endl;
    exit(EXIT_FAILURE);
  }
  // Set the variable values
  //
  setId("Initiation::RandomBoolean");
  setParameter(paraValue);
  setVariableIndex(indValue);

  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp[0] = "p";
  tmp[1] = "seed";
  if (numParameter() == 3)
    tmp[2] = "DeltaT";

  setParameterId(tmp);

  // Set the next time if deltaT given (assumes starting at t=0)
  localTime_ = 0.0;
  if (numParameter() == 3 && parameter(2) > 0.0)
    nextTime_ = nextTime_ + parameter(2);
}

void RandomBoolean::derivs(Compartment &compartment, size_t species,
                           DataMatrix &y, DataMatrix &dydt) {
  // nothing
}

void RandomBoolean::initiate(double t, std::vector<std::vector<double> > &y) {
  // Initiate with given seed
  long int idum = long(parameter(1));
  myRandom::sran3(idum);

  // Do the initiation for each cell
  size_t numCells = y.size();

  size_t cIndex = variableIndex(0, 0);
  double prob = parameter(0);
  // For each cell
  for (size_t cellI = 0; cellI < numCells; ++cellI) {
    y[cellI][cIndex] = 0.0;
    if (myRandom::ran3() < prob)
      y[cellI][cIndex] = 1.0;
  }
}

void RandomBoolean::update(double h, double t,
                           std::vector<std::vector<double> > &y) {
  localTime_ += h;
  if (numParameter() == 3 && parameter(2) > 0.0 && localTime_ > nextTime_) {

    // Do the check for each cell
    size_t numCells = y.size();

    size_t cIndex = variableIndex(0, 0);
    double prob = parameter(0);
    // For each cell
    for (size_t cellI = 0; cellI < numCells; ++cellI) {
      if (y[cellI][cIndex] < 0.5 &&
          myRandom::ran3() < prob) { // only off cells checked
        y[cellI][cIndex] = 1.0;
      }
    }
    nextTime_ += parameter(2);
  }
}

RandomBooleanBiased::RandomBooleanBiased(
    std::vector<double> &paraValue,
    std::vector<std::vector<size_t> > &indValue) {
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 3 && paraValue.size() != 4) {
    std::cerr
        << "Initiation::RandomBooleanBiased::"
        << "RandomBooleanBiased() "
        << "Uses first parameter p (probability to set cell variable to one)."
        << std::endl
        << "The second parameter set the bias ." << std::endl
        << "The third parameter sets the seed of the random generator."
        << std::endl
        << "A fourth parameter can be given to set a deltaT for repeat of the "
           "procedure, "
        << std::endl
        << "but where only off cells can possibly be set to one." << std::endl;
    exit(EXIT_FAILURE);
  }
  if (paraValue[0] < 0.0 || paraValue[0] > 1.0) {
    std::cerr << "Initiation::RandomBooleanBiased::"
              << "RandomBooleanBiased() "
              << "First parameter a probability and have to be in [0:1]."
              << std::endl;
    exit(EXIT_FAILURE);
  }

  if (indValue.size() != 1 || indValue[0].size() != 1) {
    std::cerr << "Initiation::RandomBooleanBiased::"
              << "RandomBooleanBiased() "
              << "Index for cell variable to be initiated given." << std::endl;
    exit(EXIT_FAILURE);
  }
  // Set the variable values
  //
  setId("Initiation::RandomBooleanBiased");
  setParameter(paraValue);
  setVariableIndex(indValue);

  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp[0] = "p";
  tmp[1] = "N_t";
  tmp[2] = "seed";
  if (numParameter() == 4)
    tmp[3] = "DeltaT";

  setParameterId(tmp);

  // Set the next time if deltaT given (assumes starting at t=0)
  localTime_ = 0.0;
  if (numParameter() == 4 && parameter(3) > 0.0)
    nextTime_ = nextTime_ + parameter(3);
}

void RandomBooleanBiased::derivs(Compartment &compartment, size_t species,
                                 DataMatrix &y, DataMatrix &dydt) {
  // nothing
}

void RandomBooleanBiased::initiate(double t,
                                   std::vector<std::vector<double> > &y) {
  // Initiate with given seed
  long int idum = long(parameter(2));
  myRandom::sran3(idum);

  // Do the initiation for each cell
  size_t numCells = y.size();

  size_t cIndex = variableIndex(0, 0);
  double prob = parameter(0);
  // For each cell
  for (size_t cellI = 0; cellI < numCells; ++cellI) {
    // Count number of neighs on
    size_t numOn = 0;
    for (size_t k = 0; k < organism()->compartment(cellI).numNeighbor(); k++) {
      size_t neighI = organism()->compartment(cellI).neighbor(k);
      if (y[neighI][cIndex] > 0.5)
        numOn++;
    }
    y[cellI][cIndex] = 0.0;
    if (numOn <= parameter(1) && myRandom::ran3() < prob)
      y[cellI][cIndex] = 1.0;
  }
}

void RandomBooleanBiased::update(double h, double t,
                                 std::vector<std::vector<double> > &y) {
  localTime_ += h;
  if (numParameter() == 4 && parameter(3) > 0.0 && localTime_ > nextTime_) {

    // Do the check for each cell
    size_t numCells = y.size();

    size_t cIndex = variableIndex(0, 0);
    double prob = parameter(0);
    // For each cell
    for (size_t cellI = 0; cellI < numCells; ++cellI) {
      if (y[cellI][cIndex] < 0.5) {
        // Count number of neighs on
        size_t numOn = 0;
        for (size_t k = 0; k < organism()->compartment(cellI).numNeighbor();
             k++) {
          size_t neighI = organism()->compartment(cellI).neighbor(k);
          if (y[neighI][cIndex] > 0.5)
            numOn++;
        }
        if (numOn <= parameter(1) &&
            myRandom::ran3() < prob) { // only off cells checked
          y[cellI][cIndex] = 1.0;
        }
      }
    }
    nextTime_ += parameter(3);
  }
}

Gradient::Gradient(std::vector<double> &paraValue,
                   std::vector<std::vector<size_t> > &indValue) {
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 2) {
    std::cerr << "Initiation::Gradient::"
              << "Gradient() "
              << "Uses first parameter p (factor added to cells neighbouring "
                 "on (1.0) cells)."
              << std::endl
              << "The second parameter set a flag if p is the multiplied with "
                 "sum of neighs on."
              << std::endl;
    exit(EXIT_FAILURE);
  }
  if (paraValue[1] != 0.0 && paraValue[1] != 1.0) { // Flag should be 0 or 1
    std::cerr << "Initiation::Gradient::"
              << "Gradient() "
              << "The second parameter set a flag if the value should be "
                 "summed over neighbours and needs to be 0 or 1."
              << std::endl;
    exit(EXIT_FAILURE);
  }
  if (indValue.size() != 1 || indValue[0].size() != 1) {
    std::cerr << "Initiation::Gradient::"
              << "Gradient() "
              << "Index for cell variable to be initiated given." << std::endl;
    exit(EXIT_FAILURE);
  }
  // Set the variable values
  //
  setId("Initiation::Gradient");
  setParameter(paraValue);
  setVariableIndex(indValue);

  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp[0] = "p";
  tmp[1] = "sumFlag";
  setParameterId(tmp);
}

void Gradient::derivs(Compartment &compartment, size_t species, DataMatrix &y,
                      DataMatrix &dydt) {
  // nothing
}

void Gradient::initiate(double t, std::vector<std::vector<double> > &y) {
  // Do the initiation for each cell
  size_t numCells = y.size();
  // Temporary vector to do the update synchronously
  std::vector<double> yiTmp(numCells);

  size_t cIndex = variableIndex(0, 0);
  double value = parameter(0);
  // For each cell
  for (size_t cellI = 0; cellI < numCells; ++cellI) {
    if (y[cellI][cIndex] < 1.0) { // do nothing to cells that are on (i.e. 1.0)
      // Count number of neighs on
      size_t numNeigh = organism()->compartment(cellI).numNeighbor();
      size_t numOn = 0;
      for (size_t k = 0; k < numNeigh; k++) {
        size_t neighI = organism()->compartment(cellI).neighbor(k);
        if (y[neighI][cIndex] >= 1.0) // better to use >0.5?
          numOn++;
      }
      if (numOn) {
        if (parameter(1) == 1.0)
          yiTmp[cellI] = double(numOn) * value;
        else
          yiTmp[cellI] = value;
      }
    }
  }
  for (size_t cellI = 0; cellI < numCells; ++cellI) {
    if (y[cellI][cIndex] < 1.0) { // do nothing to cells that are on
      y[cellI][cIndex] = yiTmp[cellI];
    }
  }
}

void Gradient::update(double h, double t,
                      std::vector<std::vector<double> > &y) {
  // Do nothing
}

  RandomFactor::RandomFactor(std::vector<double> &paraValue,
                   std::vector<std::vector<size_t> > &indValue) {
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 2) {
    std::cerr << "Initiation::RandomFactor::"
              << "RandomFactor() "
              << "Uses first parameter p (factor multiplied to current value "
                 "at initiation)."
              << std::endl
              << "The second parameter is an integer setting the seed for the "
                 "random number generator."
              << std::endl;
    exit(EXIT_FAILURE);
  }
  if (paraValue[0] < 0.0) { // Factor should be positive
    std::cerr << "Initiation::RandomFactor::"
              << "RandomFactor() "
              << "The first parameter (factor) should be "
                 "positive."
              << std::endl;
    exit(EXIT_FAILURE);
  }
  if (indValue.size() != 1 || indValue[0].size() != 1) {
    std::cerr << "Initiation::RandomFactor::"
              << "RandomFactor() "
              << "Index for cell variable to be initiated given." << std::endl;
    exit(EXIT_FAILURE);
  }
  // Set the variable values
  //
  setId("Initiation::RandomFactor");
  setParameter(paraValue);
  setVariableIndex(indValue);

  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp[0] = "randomFactorBound";
  tmp[1] = "seed";
  setParameterId(tmp);
}

void RandomFactor::derivs(Compartment &compartment, size_t species, DataMatrix &y,
                      DataMatrix &dydt) {
  // Do nothing
}

void RandomFactor::initiate(double t, std::vector<std::vector<double> > &y) {

  // Initiate with given seed
  long int idum = long(parameter(1));
  myRandom::sran3(idum);

  size_t cIndex = variableIndex(0, 0);
  double bound = parameter(0);
  double divBound = 1.0/bound;
  // Do the initiation for each cell
  size_t numCells = y.size();
  for (size_t cellI = 0; cellI < numCells; ++cellI) {
    y[cellI][cIndex] *= divBound+myRandom::ran3()*(bound-divBound);
  }
}

void RandomFactor::update(double h, double t,
                      std::vector<std::vector<double> > &y) {
  // Do nothing
}

} // end namespace Initiation
