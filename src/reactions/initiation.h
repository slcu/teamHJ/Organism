//
// Filename     : initiation.h
// Description  : Classes describing initiation rules for an organism simulation
// Author(s)    : Henrik Jonsson (henrik.jonsson@slcu.cam.ac.uk)
// Created      : October 2019
// Revision     : $Id:$
//
#ifndef INITIATION_H
#define INITIATION_H

#include <cmath>

#include "../common/myRandom.h"
#include "baseReaction.h"

///
/// @brief Collection of reactions describing tissue initiation rules (e.g.
/// setting concentration variables)
///
/// @details Rules for setting initial values of variables are included in this
/// namespace. These reactions do not create the geometry, but rather only set
/// (cell) variable values.
///
namespace Initiation {
///
/// @brief This rule sets a cell variable to a given value.
/// 
///
/// @details For each cell the given cell index variable is set to the value of
/// the first parameter. A @$\Delta T@$ parameter can
/// be given to set the frequency at which the operation should repeat. By
/// default, this happens every time step.
///
/// In the model file the reaction is defined by
/// @verbatim
/// Initiation::SingleValue 1[2,3] 1 1
/// value [\update_flag] [\DeltaT]
/// var_index
/// @endverbatim
/// the index given is the variable to be updated (tissue style)
///
class SingleValue : public BaseReaction {

public:
  double localTime_ = 0.0; // variable to update time using h in update function
  double nextTime_ = 0.0;  // variable updated to hold the next time it will
                           // launch the procedure
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  SingleValue(std::vector<double> &paraValue,
                std::vector<std::vector<size_t> > &indValue);

  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment, size_t species, DataMatrix &y,
              DataMatrix &dydt);

  ///
  /// @see BaseReaction::update()
  ///
  void update(double h, double t, std::vector<std::vector<double> > &y);

  ///
  /// @see BaseReaction::update()
  ///
  void initiate(double t, std::vector<std::vector<double> > &y);
};


///
/// @brief This rule sets a cell variable to 1 with probability @f$p_{0}@f$, 0
/// otherwise
///
/// @details For each cell the given cell index variable is set to
/// restricting variable given by
///
/// @f[ y_{ij} = 1 if R<p_0 y_{ij} = 0 if R>p_0 @f]
///
/// where p_0 is the probability for each cell i to get value 1 for molecule j.
/// seed is an integer to seed the random number. A @$\Delta T@$ parameter can
/// be given and then the random initiation is redone with a period (but where
/// only ones can be given to off cells). If set to 0, it will not repeat. In
/// the model file the reaction is defined by
/// @verbatim
/// Initiation::RandomBoolean 2[3] 1 1
/// p seed [\DeltaT]
/// var_index
/// @endverbatim
/// the index given is the variable to be updated (tissue style)
///
class RandomBoolean : public BaseReaction {

public:
  double localTime_ = 0.0; // variable to update time using h in update function
  double nextTime_ = 0.0;  // variable updated to hold the next time it will
                           // launch the procedure
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  RandomBoolean(std::vector<double> &paraValue,
                std::vector<std::vector<size_t> > &indValue);

  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment, size_t species, DataMatrix &y,
              DataMatrix &dydt);

  ///
  /// @see BaseReaction::update()
  ///
  void update(double h, double t, std::vector<std::vector<double> > &y);

  ///
  /// @see BaseReaction::update()
  ///
  void initiate(double t, std::vector<std::vector<double> > &y);
};

///
/// @brief This rule sets a cell variable to 1 with probability @f$p_{0}@f$, 0
/// otherwise with a bias disallowing setting cells where more than @$N_t@$
/// neighbour cells are on
///
/// @details For each cell the given cell index variable is set to
/// restricting variable given by
///
/// @f[ y_{ij} = 1 if R<p_0 and N_{on}<p_1, else y_{ij} = 0 @f]
///
/// where p_0 is the probability for each cell i to get value 1 for molecule j.
/// probability will be 0 if the number of neighbours that are 'on' ar more than
/// @$N_{t}@$ or more. seed is an integer to seed the random number. A @$\Delta
/// T@$ parameter can be given and then the random initiation is redone with a
/// period (but where only ones can be given to off cells). If set to 0, it will
/// not repeat. In the model file the reaction is defined by
/// @verbatim
/// Initiation::RandomBoolean 2[3] 1 1
/// p N_t seed [\DeltaT]
/// var_index
/// @endverbatim
/// the index given is the variable to be updated (tissue style)
///
class RandomBooleanBiased : public BaseReaction {

public:
  double localTime_ = 0.0; // variable to update time using h in update function
  double nextTime_ = 0.0;  // variable updated to hold the next time it will
                           // launch the procedure
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  RandomBooleanBiased(std::vector<double> &paraValue,
                      std::vector<std::vector<size_t> > &indValue);

  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment, size_t species, DataMatrix &y,
              DataMatrix &dydt);

  ///
  /// @see BaseReaction::update()
  ///
  void update(double h, double t, std::vector<std::vector<double> > &y);

  ///
  /// @see BaseReaction::initiate()
  ///
  void initiate(double t, std::vector<std::vector<double> > &y);
};

///
/// @brief This rule sets a cell variable to a value given if neighbouring
/// a cell that is on (=1)
///
/// @details For each cell neighbouring an on cell (conc=1.0), the concentration is
/// initiated to the value given as p0. p1 is a flag and if it is set, the value in
/// p0 is set to p0*(number on neighbours). The reaction is used to generate
/// gradients around boolean regions.
///
/// In the model file the reaction is defined by
/// @verbatim
/// Initiation::Gradient 1[2] 1 1
/// value [sumNeighFlag]
/// index
/// @endverbatim
/// where the index gives the molecul to initiate (Tissue style)
///
class Gradient : public BaseReaction {

public:
  ///
  /// @brief Main constructor
  ///
  /// @details This is the main constructor which sets the parameters and
  /// variable indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  Gradient(std::vector<double> &paraValue,
           std::vector<std::vector<size_t> > &indValue);

  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment, size_t species, DataMatrix &y,
              DataMatrix &dydt);

  ///
  /// @see BaseReaction::update()
  ///
  void update(double h, double t, std::vector<std::vector<double> > &y);

  ///
  /// @see BaseReaction::update()
  ///
  void initiate(double t, std::vector<std::vector<double> > &y);
};

///
/// @brief This rule multiplies a concentration value with a random factor
/// at initiation
///
/// @details For each cell, the value given in the init file is multiplied with
/// a random factor which bound is given as parameter. For a parameter
/// p0 each cell value will be multiplied by a (uniform) random number
/// between (1/p0) and p0. p0 would typically be close to 1.0 to generate small
/// random fluctuations. The multiplication is only applied once.
/// A second integer parameter p1 is to seed the random number generator.
///
/// In the model file the reaction is defined by
/// @verbatim
/// Initiation::RandomFactor 2 1 1
/// p0      # random factor
/// p1      # seed (integer) for random number generator
/// index   # index of molecule to be initiated
/// @endverbatim
/// where the index gives the molecul to initiate (Tissue style)
/// @note This can be used after Initiation::Value to generate a homogeneous
/// initiation with random fluctuations.
class RandomFactor : public BaseReaction {

public:
  ///
  /// @brief Main constructor
  ///
  /// @details This is the main constructor which sets the parameters and
  /// variable indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  RandomFactor(std::vector<double> &paraValue,
           std::vector<std::vector<size_t> > &indValue);

  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment, size_t species, DataMatrix &y,
              DataMatrix &dydt);

  ///
  /// @see BaseReaction::update()
  ///
  void update(double h, double t, std::vector<std::vector<double> > &y);

  ///
  /// @see BaseReaction::update()
  ///
  void initiate(double t, std::vector<std::vector<double> > &y);
};

} // end of namespace Initiation
#endif
