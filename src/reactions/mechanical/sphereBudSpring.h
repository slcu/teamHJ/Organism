///
/// Filename     : sphereBudSpring.h
/// Description  : Classes describing springs between spherical cells
/// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
/// Created      : March 2010
/// Revision     : $Id:$
///
#ifndef SPHEREBUDSPRING_H
#define SPHEREBUDSPRING_H

#include "../baseReaction.h"
#include <cmath>

///
/// @brief Namespace including reactions acting on budding cells.
///
/// Budding cells are described by two spheres where the bud/daughter sits on
/// the surface of the mother. These reactions assume a data structure of
/// X_mother, X_bud, r_mother, r_bud where X is the p[osition vector in 1, 2, or
/// 3D. More description can be found in the paper Jonsson and Levchenko (2005)
/// "An explicit spatial model of yeast microcolony growth"
/// Multiscale Modeling & Simulation
namespace SphereBud {

///
/// @brief SpringAsymmetricSphereBud uses an asymmetric(adh,rep) spring between
/// two budding cells
///
/// @details Reaction describing mechaniocal interactions between two budding
/// compartments It implements an asymmetric spring (stronger when under
/// compression) with parameters p0=r_factor, setting equilibrium distance as
/// factor of radii (1.0=on surface), p1=k_spring, spring constant
/// p2=k_adhesion, factor multiplied when under tension (adhesion)
/// p3=f_trunc, distance outside of which no force applied as function of radii
///
/// @see SpringAsymmetricSphere
///
class SpringAsymmetric : public BaseReaction {

public:
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  SpringAsymmetric(std::vector<double> &paraValue,
                   std::vector<std::vector<size_t> > &indValue);

  void derivs(Compartment &compartment, size_t varIndex,
              std::vector<std::vector<double> > &y,
              std::vector<std::vector<double> > &dydt);
};
} // end namespace SphereBud

#endif // SPHEREBUDSPRING_H
