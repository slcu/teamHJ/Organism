//
// Filename     : adhocReaction.cc
// Description  : Classes describing some adhoc reactions
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : October 2008
// Revision     : $Id:$
//

#include "adhocReaction.h"
#include "../organism.h"
#include "../common/myRandom.h"
#include "baseReaction.h"

namespace AdHoc {

SumCompartmentVar::SumCompartmentVar(
    std::vector<double> &paraValue,
    std::vector<std::vector<size_t> > &indValue) {
  //
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 1) {
    std::cerr << "SumCompartmentVar::SumCompartmentVar() one parameter used "
              << "(unit conversion factor)." << std::endl;
    exit(0);
  }
  int fail = 0;
  size_t i = 1;
  while (i < indValue.size()) {
    if (indValue[i].size() != 2) {
      fail = 1;
    }
    ++i;
  }
  if (indValue.size() < 2 || indValue[0].size() != 2 || fail) {
    std::cerr << "SumCompartmentVar::SumCompartmentVar() "
              << "First level of indices is (concentrationIndex sumvarIndex)"
              << std::endl
              << "For each additional level compartmentStart and compartmentEnd"
              << " should be given." << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("sumCompartmentVar");
  setParameter(paraValue);
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  tmp[0] = "conversionFactor";
  setParameterId(tmp);
}

void SumCompartmentVar::derivs(Compartment &compartment, size_t species,
                               DataMatrix &y, DataMatrix &dydt) {
  // Do nothing (everything is done in the update).
}

void SumCompartmentVar::update(double h, double t, DataMatrix &y) {
  for (size_t k = 1; k < numVariableIndexLevel(); ++k) {
    double sum = 0.0;
    for (size_t c = variableIndex(k, 0); c <= variableIndex(k, 1); ++c)
      sum += y[c][variableIndex(0, 0)];
    double val = parameter(0) * sum;
    for (size_t c = variableIndex(k, 0); c <= variableIndex(k, 1); ++c)
      y[c][variableIndex(0, 1)] = val;
  }
}

SumCompartmentMass::SumCompartmentMass(
    std::vector<double> &paraValue,
    std::vector<std::vector<size_t> > &indValue) {
  //
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 1) {
    std::cerr << "SumCompartmentMass::SumCompartmentMass() one parameter used "
              << "(converion factor)." << std::endl;
    exit(0);
  }
  int fail = 0;
  size_t i = 1;
  while (i < indValue.size()) {
    if (indValue[i].size() != 2) {
      fail = 1;
    }
    ++i;
  }
  if (indValue.size() < 2 || indValue[0].size() != 3 || fail) {
    std::cerr
        << "SumCompartmentMass::SumCompartmentMass() "
        << "First level of indices is (Volume concentration massSummed)"
        << std::endl
        << "Then for each additional level compartmentStart and compartmentEnd"
        << " should be given." << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("sumCompartmentMass");
  setParameter(paraValue);
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  tmp[0] = "conversionFactor";
  setParameterId(tmp);
}

void SumCompartmentMass::derivs(Compartment &compartment, size_t species,
                                DataMatrix &y, DataMatrix &dydt) {
  // Do nothing (everything is done in the update).
}

void SumCompartmentMass::update(double h, double t, DataMatrix &y) {
  for (size_t k = 1; k < numVariableIndexLevel(); ++k) {
    double sum = 0.0;
    for (size_t c = variableIndex(k, 0); c <= variableIndex(k, 1); ++c)
      sum += y[c][variableIndex(0, 0)] * y[c][variableIndex(0, 1)];
    double val = parameter(0) * sum;
    for (size_t c = variableIndex(k, 0); c <= variableIndex(k, 1); ++c)
      y[c][variableIndex(0, 2)] = val;
  }
}

DiffusionDivided::DiffusionDivided(
    std::vector<double> &paraValue,
    std::vector<std::vector<size_t> > &indValue) {
  //
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 2) {
    std::cerr << "DiffusionDivided::DiffusionDivided() two parameters used "
              << "diffusion rate and leakage neighbors" << std::endl;
    exit(0);
  }
  if (indValue.size()) {
    std::cerr << "DiffusionDivided::DiffusionDivided() "
              << "No indices used in this reaction." << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("diffusionDivided");
  setParameter(paraValue);
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  tmp[0] = "D";
  tmp[1] = "leakageNumber";
  setParameterId(tmp);
}

void DiffusionDivided::derivs(Compartment &compartment, size_t species,
                              DataMatrix &y, DataMatrix &dydt) {
  size_t i = compartment.index();
  size_t N = compartment.numNeighbor();
  double rate = parameter(0) / (N + parameter(1));
  double diff = rate * y[i][species];
  // cell-cell diffusion part
  for (size_t n = 0; n < N; n++) {
    size_t j = compartment.neighbor(n);
    dydt[i][species] -= diff;
    dydt[j][species] += diff;
  }
  // cell-background diffusion part
  dydt[i][species] -= parameter(1) * diff;
}

double DiffusionDivided::propensity(Compartment &compartment, size_t species,
                                    DataMatrix &y) {
  return parameter(0) * y[compartment.index()][species];
}

void DiffusionDivided::discreteUpdate(Compartment &compartment, size_t species,
                                      DataMatrix &y) {
  size_t neigh = static_cast<size_t>(
      myRandom::ran3() * (compartment.numNeighbor() + parameter(1)));
  y[compartment.index()][species] -= 1.0;
  if (neigh < compartment.numNeighbor()) {
    y[compartment.neighbor(neigh)][species] += 1.0;
  }
}

Add::Add(std::vector<double> &paraValue,
         std::vector<std::vector<size_t> > &indValue) {
  //
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 1) {
    std::cerr << "Add::Add() one parameters used "
              << "Constant to add." << std::endl;
    exit(0);
  }
  if (indValue.size() != 2 || indValue[0].size() != 1) {
    std::cerr << "Add::Add() "
              << "Two levels of variable indices are used, "
              << "one where the addition is stored (single index)"
              << ", and one for variables to add is used." << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("add");
  setParameter(paraValue);
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  tmp[0] = "const";
  setParameterId(tmp);
}

void Add::derivs(Compartment &compartment, size_t species, DataMatrix &y,
                 DataMatrix &dydt) {
  // Nothing to be done for the derivative function.
}

void Add::update(double h, double t, DataMatrix &y) {
  size_t N = y.size();
  for (size_t i = 0; i < N; i++) {
    double sum = parameter(0);
    for (size_t k = 0; k < numVariableIndex(1); ++k)
      sum += y[i][variableIndex(1, k)];
    y[i][variableIndex(0, 0)] = sum;
  }
}

SetRestricted::SetRestricted(std::vector<double> &paraValue,
                             std::vector<std::vector<size_t> > &indValue) {
  //
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 1) {
    std::cerr << "SetRestricted::SetRestricted() one parameters used "
              << "Constant value to set the variable value to." << std::endl;
    exit(0);
  }
  if (indValue.size() != 2 || indValue[0].size() != 1) {
    std::cerr << "SetRestricted::SetRestricted() "
              << "Two levels of variable indices are used, "
              << "one where the value is stored"
              << ", and one for the variable that has to be on for setting the "
                 "variable."
              << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("set");
  setParameter(paraValue);
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  tmp[0] = "value";
  setParameterId(tmp);
}

void SetRestricted::derivs(Compartment &compartment, size_t species,
                           DataMatrix &y, DataMatrix &dydt) {
  // Nothing to be done for the derivative function.
}

void SetRestricted::update(double h, double t, DataMatrix &y) {
  size_t N = y.size();
  for (size_t i = 0; i < N; i++) {
    if (y[i][variableIndex(1, 0)]) {
      y[i][variableIndex(0, 0)] = parameter(0);
    }
  }
}

SwitchCompartmentThreshold::SwitchCompartmentThreshold(
    std::vector<double> &paraValue,
    std::vector<std::vector<size_t> > &indValue) {
  //
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 2) {
    std::cerr << "SwitchCompartmentThreshold::SwitchCompartmentThreshold() two "
                 "parameters used "
              << "Threshold and marker for >threshold (1) or <threshold (-1)."
              << std::endl;
    exit(0);
  }
  if (indValue.size() != 2 || indValue[0].size() != 1 ||
      indValue[1].size() != 2) {
    std::cerr << "SwitchCompartmentThreshold::SwitchCompartmentThreshold() "
              << "Two levels of variable indices are used, "
              << "one for the threshold variable (single index)"
              << ", and one for variables to change (first set to zero second "
                 "to one is used."
              << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("add");
  setParameter(paraValue);
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  tmp[0] = "const";
  setParameterId(tmp);
}

void SwitchCompartmentThreshold::derivs(Compartment &compartment,
                                        size_t species, DataMatrix &y,
                                        DataMatrix &dydt) {
  // Nothing to be done for the derivative function.
}

void SwitchCompartmentThreshold::update(double h, double t, DataMatrix &y) {
  for (size_t i = 0; i < y.size(); ++i)
    if ((parameter(1) >= 0 && y[i][variableIndex(0, 0)] > parameter(0)) ||
        (parameter(1) < 0 && y[i][variableIndex(0, 0)] < parameter(0))) {
      y[i][variableIndex(1, 0)] = 0;
      y[i][variableIndex(1, 1)] = 1;
    }
}

//

ThresholdSwitch::ThresholdSwitch(std::vector<double> &paraValue,
                                 std::vector<std::vector<size_t> > &indValue) {
  //
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 2) {
    std::cerr << "ThresholdSwitch::ThresholdSwitch()  parameter used "
              << "Threshold" << std::endl;
    exit(0);
  }
  if (indValue.size() != 2 || indValue[0].size() != 1 ||
      indValue[1].size() != 1) {
    std::cerr << "ThresholdSwitch::ThresholdSwitch() "
              << "Two levels of variable indices are used, "
              << "one for the threshold variable (single index)"
              << ", and one for a list of variables to change" << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("add");
  setParameter(paraValue);
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  tmp[0] = "const";
  tmp[1] = "switchtype";
  setParameterId(tmp);
}

void ThresholdSwitch::derivs(Compartment &compartment, size_t species,
                             DataMatrix &y, DataMatrix &dydt) {
  // Nothing to be done for the derivative function.
}

void ThresholdSwitch::derivsWithAbs(Compartment &compartment, size_t species,
                                    DataMatrix &y, DataMatrix &dydt,
                                    DataMatrix &sdydt) {
  // Nothing to be done for the derivative function.
}

void ThresholdSwitch::update(double h, double t, DataMatrix &y) {
  for (size_t i = 0; i < y.size(); ++i)
    if (y[i][variableIndex(0, 0)] >= parameter(0)) {
      y[i][variableIndex(1, 0)] = 1;
    } else if (y[i][variableIndex(0, 0)] < parameter(0) && parameter(1) == 0) {
      y[i][variableIndex(1, 0)] = 0;
    }
}

SpecialTimedThresholdSwitch::SpecialTimedThresholdSwitch(
    std::vector<double> &paraValue,
    std::vector<std::vector<size_t> > &indValue) {
  //
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 0) {
    std::cerr << "SpecialTimedThresholdSwitch::SpecialTimedThresholdSwitch()  "
                 "0 parameters used "
              << std::endl;
    exit(0);
  }
  if (indValue.size() != 2 || indValue[0].size() != 3 ||
      indValue[1].size() != 1) {
    std::cerr << "SpecialTimedThresholdSwitch::SpecialTimedThresholdSwitch() "
              << "Two levels of variable indices are used, "
              << "three for the switch input indices the threshold variable "
                 "(single index)"
              << ", and one for the downstream variable to change."
              << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("add");
  setParameter(paraValue);
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  // tmp[0] = "const";
  // tmp[1] = "switchtype";
  setParameterId(tmp);
}

void SpecialTimedThresholdSwitch::derivs(Compartment &compartment,
                                         size_t species, DataMatrix &y,
                                         DataMatrix &dydt) {
  // Nothing to be done for the derivative function.
}

void SpecialTimedThresholdSwitch::derivsWithAbs(Compartment &compartment,
                                                size_t species, DataMatrix &y,
                                                DataMatrix &dydt,
                                                DataMatrix &sdydt) {
  // Nothing to be done for the derivative function.
}

void SpecialTimedThresholdSwitch::update(double h, double t, DataMatrix &y) {
  for (size_t i = 0; i < y.size(); ++i) {

    if (y[i][variableIndex(0, 1)] > y[i][variableIndex(0, 0)] &&
        y[i][variableIndex(0, 2)] == 0.0) {
      y[i][variableIndex(1, 0)] = t;
    }
  }
}

SpecialTimedReversedThresholdSwitch::SpecialTimedReversedThresholdSwitch(
    std::vector<double> &paraValue,
    std::vector<std::vector<size_t> > &indValue) {
  //
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 0) {
    std::cerr << "SpecialTimedReversedThresholdSwitch::"
                 "SpecialTimedReversedThresholdSwitch()  0 parameters used "
              << std::endl;
    exit(0);
  }
  if (indValue.size() != 2 || indValue[0].size() != 3 ||
      indValue[1].size() != 1) {
    std::cerr << "SpecialTimedReversedThresholdSwitch::"
                 "SpecialTimedReversedThresholdSwitch() "
              << "Two levels of variable indices are used, "
              << "three for the switch input indices the threshold variable "
                 "(single index)"
              << ", and one for the downstream variable to change."
              << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("add");
  setParameter(paraValue);
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  // tmp[0] = "const";
  // tmp[1] = "switchtype";
  setParameterId(tmp);
}

void SpecialTimedReversedThresholdSwitch::derivs(Compartment &compartment,
                                                 size_t species, DataMatrix &y,
                                                 DataMatrix &dydt) {
  // Nothing to be done for the derivative function.
}

void SpecialTimedReversedThresholdSwitch::derivsWithAbs(
    Compartment &compartment, size_t species, DataMatrix &y, DataMatrix &dydt,
    DataMatrix &sdydt) {
  // Nothing to be done for the derivative function.
}

void SpecialTimedReversedThresholdSwitch::update(double h, double t,
                                                 DataMatrix &y) {
  for (size_t i = 0; i < y.size(); ++i) {

    if (y[i][variableIndex(0, 1)] <= y[i][variableIndex(0, 0)] &&
        y[i][variableIndex(0, 2)] == 0.0) {
      y[i][variableIndex(1, 0)] = t;
    }
  }
}
FactorOnDerivsRestricted::FactorOnDerivsRestricted(
    std::vector<double> &paraValue,
    std::vector<std::vector<size_t> > &indValue) {
  //
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 1) {
    std::cerr << "FactorOnDerivsRestricted::FactorOnDerivsRestricted() one "
                 "parameter used: "
              << "Factor for multiplication" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (indValue.size() != 1 || indValue[0].size() != 1) {
    std::cerr << "FactorOnDerivsRestricted::FactorOnDerivsRestricted() one "
                 "variable index "
              << "used for flag variable (marks whether multiplication should "
                 "be done."
              << std::endl;
    exit(EXIT_FAILURE);
  }
  //
  // Set the variable values
  //
  setId("factorOnDerivsRestricted");
  setParameter(paraValue);
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  tmp[0] = "factor";
  setParameterId(tmp);
}

void FactorOnDerivsRestricted::derivs(Compartment &compartment, size_t species,
                                      DataMatrix &y, DataMatrix &dydt) {
  if (y[compartment.index()][variableIndex(0, 0)]) {
    dydt[compartment.index()][species] *= parameter(0);
  }
}

void FactorOnDerivsRestricted::derivsWithAbs(Compartment &compartment,
                                             size_t species, DataMatrix &y,
                                             DataMatrix &dydt,
                                             DataMatrix &sdydt) {
  if (y[compartment.index()][variableIndex(0, 0)]) {
    dydt[compartment.index()][species] *= parameter(0);
    sdydt[compartment.index()][species] *= parameter(0);
  }
}

FactorOnDerivsMultiple::FactorOnDerivsMultiple(
    std::vector<double> &paraValue,
    std::vector<std::vector<size_t> > &indValue) {
  //
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size() != 1) {
    std::cerr << "FactorOnDerivsMultiple::FactorOnDerivsMultiple() one "
                 "parameter used: "
              << "Factor for multiplication" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (indValue.size() != 1 || indValue[0].size() != 1) {
    std::cerr << "FactorOnDerivsMultiple::FactorOnDerivsMultiple() one "
                 "variable index "
              << "used for flag variable (marks whether multiplication should "
                 "be done."
              << std::endl;
    exit(EXIT_FAILURE);
  }
  //
  // Set the variable values
  //
  setId("factorOnDerivsMultiple");
  setParameter(paraValue);
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  tmp[0] = "threshold";
  setParameterId(tmp);
}

void FactorOnDerivsMultiple::derivs(Compartment &compartment, size_t species,
                                    DataMatrix &y, DataMatrix &dydt) {
  if (y[compartment.index()][variableIndex(0, 0)] > parameter(0)) {
    // int factor = std::pow(2, (int)
    // y[compartment.index()][variableIndex(0,0)]/parameter(0) );
    int factor =
        std::pow(2, (int)log2(pow(y[compartment.index()][variableIndex(0, 0)] /
                                      parameter(0),
                                  3.0)));

    dydt[compartment.index()][species] *= factor;
  }
}

void FactorOnDerivsMultiple::derivsWithAbs(Compartment &compartment,
                                           size_t species, DataMatrix &y,
                                           DataMatrix &dydt,
                                           DataMatrix &sdydt) {
  if (y[compartment.index()][variableIndex(0, 0)] > parameter(0)) {
    // int factor = std::pow(2, (int)
    // y[compartment.index()][variableIndex(0,0)]/parameter(0) );
    int factor =
        std::pow(2, (int)log2(pow(y[compartment.index()][variableIndex(0, 0)] /
                                      parameter(0),
                                  3.0)));
    dydt[compartment.index()][species] *= factor;
    sdydt[compartment.index()][species] *= factor;
  }
}

NeighbourTopology::
NeighbourTopology(std::vector<double> &paraValue, 
		  std::vector< std::vector<size_t> > &indValue ) {
  //
  // Do some checks on the parameters and variable indeces
  //
  if (paraValue.size()>2) {
    std::cerr << "AdHoc::NeighbourTopology::NeighbourTopology() " << std::endl
	      << "0, 1 or 2 parameters required."
	      << "All neighbours are taken into account always." << std::endl
	      << "If p_0 given and equal to 1, L1-distribution is calculated,"
	      << std::endl
	      << "If p_1 given and equal to 1 L1-L2 distribution is calculated."
	      << std::endl;
    exit(EXIT_FAILURE);
  }
  if (paraValue.size() && (indValue.size()!=1 || indValue[0].size()!=1) ) {
    std::cerr << "AdHoc::NeighbourTopology::NeighbourTopology() " << std::endl
	      << "If p_0 given, L1marker_index needs to be provided."
	      << std::endl;
    exit(EXIT_FAILURE);
  }
  //
  // Set the variable values
  //
  setId("AdHoc::neighbourTopology");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  if (numParameter())
    tmp[0] = "L1flag";
  if (numParameter()>1)
    tmp[1] = "L1L2flag";
  setParameterId( tmp );
}
    
void NeighbourTopology::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt ) {
}

void NeighbourTopology::    
update(double h, double t, std::vector< std::vector<double> > &y) {
  initiate(t,y);
}

void NeighbourTopology::
initiate(double t, std::vector< std::vector<double> > &y) {

  // Check what to do in terms of regional collection
  size_t flagL1=0;
  if (numParameter() && parameter(0)==1)
    flagL1 = 1;
  size_t flagL1L2=0;
  if (numParameter()>1 && parameter(1)==1)
    flagL1L2 = 1;
  // Set up storage variables
  std::vector<size_t> numNeigh(1);
  std::vector<size_t> numNeighL1(1);
  std::vector<size_t> numNeighL1L2(1);
  size_t numNeighTot = 0;
  size_t numNeighTotL1 = 0;
  size_t numNeighTotL1L2 = 0;

  // Go through all cells and collect number of neighbours
  size_t numCell = y.size();
  for (size_t cellI=0; cellI<numCell; ++cellI) {
    size_t nNeigh = organism()->compartment(cellI).numNeighbor();
    if (nNeigh>numNeigh.size())
      numNeigh.resize(nNeigh+1);
    numNeigh[nNeigh]++;
    numNeighTot++;
    if (flagL1 && y[cellI][variableIndex(0,0)] == 1) {
      size_t nNeighL1 = 0;
      // Find L1L1 connections
      for (size_t neighK=0; neighK<nNeigh; neighK++) {
	if (y[organism()->compartment(cellI).neighborIndex(neighK)][variableIndex(0,0)] == 1) {
	  nNeighL1++;
	  numNeighTotL1++;
	}
      }
      if (nNeigh>numNeigh.size())
	numNeigh.resize(nNeigh+1);
      if (flagL1L2) {
	size_t nNeighL1L2 = nNeigh-nNeighL1;
	if (nNeighL1L2>numNeighL1L2.size())
	  numNeighL1L2.resize(nNeigh+1);
	numNeighL1L2[nNeigh]++;
	numNeighTotL1L2++;
	
      }
    }
  }
  // Print the result (ad hoc to do it here but fine for now)
  std::cout << "# Neighbourhood topology distribution for all cells" << std::endl
	    << "# NumNeigh NumCells" << std::endl;
  for (size_t n=0; n<numNeigh.size(); n++)
    std::cout << n << "\t" << numNeigh[n] << std::endl;
  std::cout << std::endl;

  if (flagL1) {
    std::cout << "# Neighbourhood topology distribution for cells within (L1) region"
	      << std::endl
	      << "# NumNeigh NumCells" << std::endl;
    for (size_t n=0; n<numNeighL1.size(); n++)
      std::cout << n << "\t" << numNeighL1[n] << std::endl;
    std::cout << std::endl;
  }

  if (flagL1) {
    std::cout << "# Neighbourhood topology distribution for cells within (L1) region"
	      << std::endl
	      << "# NumNeigh NumCells" << std::endl;
    for (size_t n=0; n<numNeighL1.size(); n++)
      std::cout << n << "\t" << numNeighL1[n] << std::endl;
    std::cout << std::endl;
  }
}


} // end namespace AdHoc

