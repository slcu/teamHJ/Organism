#ifndef MYSIGNAL_H
#define MYSIGNAL_H

#include "../optimizers/baseOptimizer.h"
#include "../solvers/baseSolver.h"
#include <signal.h>
#include <vector>

namespace mySignal {
void myExit();
void addSolver(BaseSolver *S);
void addOptimizer(BaseOptimizer *Opt);
void solverSignalHandler(int signal);
void optimizerSignalHandler(int signal);
} // namespace mySignal

#endif /* MYSIGNAL_H */
