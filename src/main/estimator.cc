#include "../common/myConfig.h"
#include "../common/myRandom.h"
#include "../common/mySignal.h"
#include "../optimizers/baseOptimizer.h"
#include "../solvers/baseSolver.h"

int main(int argc, char *argv[]) {

  myConfig::registerOption("verbose", 1);
  myConfig::registerOption("debug_output", 1);
  myConfig::registerOption("parameter_input", 1);

  std::string configFile(getenv("HOME"));
  configFile.append("/.organism");
  myConfig::initConfig(argc, argv, configFile);

  if ((argc < 4) && (argc != 2)) {
    std::cerr << "possible usages: 1:" << argv[0] << " modelFile "
              << "simulatorParaFile analyzerFile\n";
    std::cerr << "                 2:" << argv[0]
              << " (model + simulator + analyser)File  [i.e. everything "
                 "consecutively in a big file]\n";
    exit(-1);
  }
  if (argc > 4) {
    std::cerr << argv[0] << "Is normally used with 3 file names. Ignoring the "
              << 4 - argc << " other files\n";
  }

  if (argc == 4 || argc == 6) {

    char *modelFile = argv[1];
    std::string simPara = argv[2];
    std::string analyzerFile = argv[3];

    myRandom::Randomize();
    myTimes::getTime();

    //
    // Define the organism (model), simulator and the optimizer
    //
    Organism O(modelFile);
    std::cerr << "Organism created from model " << modelFile << "."
              << std::endl;

    O.initiateParameter();

    // Overrride parameter values in model file if applicable
    // Caveat: currently it assumes correct number of parameters and
    // do not allow for comments in the file.
    std::string parameterValueFile;
    parameterValueFile = myConfig::getValue("parameter_input", 0);
    if (!parameterValueFile.empty()) {
      std::istream *IN = myFiles::openFile(parameterValueFile);
      if (!IN) {
        std::cerr << "Warning: main() -"
                  << "Cannot open file for parameter reading ("
                  << parameterValueFile << ")." << std::endl;
      } else {
        std::cerr << "Overriding model parameters with values from file "
                  << parameterValueFile << "." << std::endl;
        double tmpDouble;
        for (size_t i = 0; i < O.numParameter(); ++i) {
          *IN >> tmpDouble;
          O.setParameter(i, tmpDouble);
        }
        delete IN;
      }
    }

    BaseSolver *S = BaseSolver::getSolver(&O, simPara);
    std::cerr << "Solver created from file " << simPara << "." << std::endl;

    BaseEstimator *Estimator = new BaseEstimator(S, analyzerFile);
    std::cerr << "Estimator created from file " << analyzerFile << "."
              << std::endl;

    // Add optimizer to signal handler.
    // mySignal::addOptimizer(Optimizer);

    // Start optimizing
    std::cerr << "cost = " << Estimator->getCost() << "\n";

    // Delete simulator and optimizer
    delete S;
    delete Estimator;
  }
  if (argc == 2) {
    char *totalFile = argv[1];

    myRandom::Randomize();
    myTimes::getTime();

    std::istream *IN = myFiles::openFile(totalFile);
    if (!IN) {
      std::cerr << "./estimator - main : "
                << "Cannot open (model-sim-est)file " << totalFile << std::endl;
      exit(-1);
    }

    Organism O((std::ifstream &)*IN);
    std::cerr << "Organism created from a first part of file " << totalFile
              << "." << std::endl;

    BaseSolver *S = BaseSolver::getSolver(&O, (std::ifstream &)*IN);
    std::cerr << "Solver created from a second part of file " << totalFile
              << "." << std::endl;

    O.initiateParameter();

    BaseEstimator *Estimator = new BaseEstimator(S, (std::ifstream &)*IN);
    std::cerr << "Estimator created from a third and last part of file "
              << totalFile << "." << std::endl;
    delete IN;

    std::cerr << "cost = " << Estimator->getCost() << "\n";

    // Delete simulator and optimizer
    delete S;
    delete Estimator;
  }
  return 0;
}
