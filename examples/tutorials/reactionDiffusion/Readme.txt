This directory holds some files with examples on running reaction-diffusion models using organism.

More information is found on the organism wiki tutorial page

https://gitlab.com/slcu/teamHJ/organism/wikis/Tutorials

Creator/author/contact: henrik.jonsson@slcu.cam.ac.uk
